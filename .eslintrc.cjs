/* eslint-env node */
require('@rushstack/eslint-patch/modern-module-resolution')

module.exports = {
  root: true,
  extends: [
    'plugin:vue/vue3-essential',
    'eslint:recommended',
    '@vue/eslint-config-typescript',
    '@vue/eslint-config-prettier/skip-formatting',
    'plugin:storybook/recommended'
  ],
  rules: {
    /**
     * Allow unused vars starting with `_` character
     * useful for destructuring eg.
     */
    "no-unused-vars": "off",
    "@typescript-eslint/no-unused-vars": [
      "warn", // or "error"
      {
        "argsIgnorePattern": "^_",
        "varsIgnorePattern": "^_",
        "caughtErrorsIgnorePattern": "^_"
      }
    ]
  },
  parserOptions: {
    ecmaVersion: 'latest'
  },
  overrides: [
    {
      files: [
        'src/pages/**/*.vue'
      ],
      rules: {
        'vue/multi-word-component-names': 'off'
      }
    }
  ],
}
