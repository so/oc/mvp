import '@/assets/main.css';

import type { Preview } from '@storybook/vue3'
import { setup } from '@storybook/vue3'
import PrimeVue from 'primevue/config'
import { createPinia } from 'pinia'
import i18n from '../src/i18n'
import primeVueConfig from '../src/primevue.config'
import ConfirmationService from 'primevue/confirmationservice'
import {
  createRouter,
  createWebHistory
} from 'vue-router'
import { routes } from 'vue-router/auto-routes'
import { abilitiesPlugin } from '@casl/vue'
import ability from '../src/ability'

const preview: Preview = {
  tags: ['autodocs'],
  parameters: {
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i
      }
    }
  }
}

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      redirect() {
        const language = navigator.language.split('-')[0] as typeof i18n.global.locale.value
        if (i18n.global.availableLocales.includes(language)) {
          return { name: 'index', params: { lang: language } }
        } else {
          return { name: 'index', params: { lang: i18n.global.fallbackLocale } }
        }
      }
    },
    ...routes
  ]
})

/**
 * Configure i18n from the first slug of current route.
 */
router.beforeResolve((to) => {
  const language = (
    Array.isArray(to.params.lang) ? to.params.lang[0] : to.params.lang
  ) as typeof i18n.global.locale.value

  if (i18n.global.availableLocales.includes(language)) {
    i18n.global.locale.value = language
  } else {
    return {
      name: `catchall`,
      params: {
        lang: i18n.global.locale.value,
        path: 'error'
      },
      query: {
        code: 404,
        from: to.path
      }
    }
  }
})

setup((app) => {
  app.use(createPinia())
    .use(i18n)
    .use(router)
    .use(PrimeVue, primeVueConfig)
    .use(ConfirmationService)
    .use(abilitiesPlugin, ability)
});

export default preview
