# Projet SO DRIIHM

Ce projet est une itération pour le MVP de l'équipe SO-DRIIHM.

L'objectif de ce projet est d'être un front-hub 
sur la p/f [SparQL Virtuoso](https://virtuoso.openlinksw.com/).

Plusieurs cas d'usage sont prévus, 
se référer au projet gitlab : https://gitlab.irit.fr/so/oc/mvp.

## Installation du projet

```sh
nvm use # version 20 actuellement
npm ci  # permet de se baser sur le package-lock.json

cp .env.dist .env # permet de copier les variables d'environnement
```

### Démarrage du projet

```sh
npm run dev
```

### Compilation du projet avec vérification du typage

```sh
npm run build
```

### Démarrage du storybook

```sh
npm run storybook
```

### Tests unitaires avec [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Lint avec [ESLint](https://eslint.org/)

```sh
npm run lint
```

## Review du projet

Afin de structurer les développements, l'équipe de développement a établi plusieurs règles
de fonctionnement.

Chaque développement fait l'objet d'un ticket particulier, 
créé sur la forge actuelle : https://gitlab.irit.fr/so/oc/mvp/

Les tickets sont gérés en suivant le board actuel : 
https://gitlab.irit.fr/so/oc/mvp/-/boards

Ils passent d'un statut à un autre, dans l'ordre suivant :
* En attente de spéc
* To do
* Doing
* À relire
* À tester
* Closed

L'étape "Doing" implique :
* la création d'une branche dédiée à ce ticket, préfixée du n° du ticket et d'un titre évoquant le ticket (ex: 16-ci-cd)
* l'assignation d'une personne qui prend en charge ce ticket

L'étape "À relire" permet de :
* créer une merge request liée à l'issue correspondante, avec sa branche corrélée
* assigner une personne responsable de la relecture
* ce relecteur pourra commenter / approuver / demander des changements sur la MR
* chaque "thread" ouvert par le relecteur devra être résolu par ce dernier, pour valider les réponses / modifications du responsable de la MR

Une fois la MR approuvée par le relecteur, 
le responsable de la MR pourra la merger.
Attention, le ticket **ne doit pas être clôturé**
mais donné à tester à une autre personne de l'équipe,
en positionnant le ticket dans la bonne colonne.

Seule une personne ayant validée la fonctionnalité,
hors équipe de dév,
pourra fermer le ticket.

Si le ticket est un ticket "opérationnel" (CI/CD par ex.),
il peut être fermé par l'équipe de dév.