import { AbilityBuilder, createMongoAbility, type ExtractSubjectType, type MongoAbility } from '@casl/ability'
import type { OcCommunity, OcMembership } from './declarations'
import { getCommunityGraph, getCommunityPublicGraph } from './helpers/resourceVisibility'

/**
 * Our ACLs only depend on graphs.
 *
 * For a specific subject (OcResource), depending on the graph to which it belongs, can the user:
 * - `read`: read it?
 * - `update`: update it?
 * - `insertNode`: insert a new object into it?
 *
 * Also, we need to know if the user can write in a specific graph : `write`
 */
type Actions = 'read' | 'update' | 'insertNode' | 'write'
type Subjects = Object
type AppAbility = MongoAbility<[Actions, Subjects]>;

const { build } = new AbilityBuilder<AppAbility>(createMongoAbility)
export default build({
  // A little hack to get all subject detected as 'object'
  detectSubjectType: _object => ('object' as ExtractSubjectType<Subjects>)
})

export const roleVisitor = 'https://www.irit.fr/opencommon/terms/Visitor'
export const roleCommunityMember = 'https://www.irit.fr/opencommon/terms/communityMember'
/** @deprecated use roleCommunityMember instead */
// TODO Remove this legacy role once the graphs have been cleaned. (#66)
export const roleSpaceMember = 'https://www.irit.fr/opencommon/terms/SpaceMember'
export const roleCommunityManager = 'https://www.irit.fr/opencommon/terms/communityManager'
/** @deprecated use roleCommunityManager instead */
// TODO Remove this legacy role once the graphs have been cleaned. (#66)
export const roleSpaceManager = 'https://www.irit.fr/opencommon/terms/SpaceManager'
export const rolePlatformManager = 'https://www.irit.fr/opencommon/terms/PlatformManager'
export const roleCatalogManager = 'https://www.irit.fr/opencommon/terms/CatalogManager'

export function defineAbilityFor(memberships: OcMembership[], communities: OcCommunity[], userPrivateGraph: string | undefined) {
  const { can, rules } = new AbilityBuilder(createMongoAbility)

  if (userPrivateGraph) {
    can(['read', 'update', 'insertNode'], 'object', { 'graph': userPrivateGraph })
    can('write', userPrivateGraph)
  }

  memberships.forEach((membership: OcMembership) => {
    const community = communities.find(community => community['@id'] === membership.organization)
    const publicGraph = getCommunityPublicGraph(community)
    const privateGraph = getCommunityGraph(community)

    // TODO Remove management of legacy graphs once the graphs have been cleaned. (#66)
    const publicGraphLegacy = getCommunityPublicGraph(community, true)
    const privateGraphLegacy = getCommunityGraph(community, true)

    if ([
      roleCommunityMember,
      roleSpaceMember,
      roleCommunityManager,
      roleSpaceManager
    ].includes(membership.role)) {
      can(['read', 'insertNode'], 'object', { '@id': membership.organization })

      if (community) {
        can(['insertNode'], 'object', { 'graph': publicGraph })
        can(['insertNode'], 'object', { 'graph': publicGraphLegacy })
        can(['read', 'update', 'insertNode'], 'object', { 'graph': privateGraph })
        /** TODO delete  */
        can(['read', 'update', 'insertNode'], 'object', { 'graph': privateGraphLegacy })

        can('write', privateGraph)
        can('write', privateGraphLegacy)
      }
    }

    if ([
      roleCommunityManager,
      roleSpaceManager
    ].includes(membership.role)) {
      if (community) {
        can(['update'], 'object', { 'graph': publicGraph })
        can(['update'], 'object', { 'graph': publicGraphLegacy })
        can(['write'], 'object', { 'graph': publicGraph })
        can(['write'], 'object', { 'graph': publicGraphLegacy })
      }
    }

    // TODO not sure about how to deal with protected catalog ?
    if (roleCatalogManager === membership.role) {
      can(['insertNode'], 'object', { '@id': membership.organization })
    }
  })

  return rules
}