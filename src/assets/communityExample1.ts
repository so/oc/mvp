import type { OcCommunity } from "@/declarations";

export const communityExample1: OcCommunity = {
  "@id": "https://www.irit.fr/opencommon/communities/189088ec-baa9-4397-8c6f-eefde9a3790c",
  "@type": [
    "http://www.w3.org/ns/dcat#Catalog",
    "http://www.w3.org/2002/07/owl#NamedIndividual",
    "https://www.irit.fr/opencommon/system/Space",
    "https://www.irit.fr/opencommon/terms/Space",
    "https://www.irit.fr/opencommon/terms/Community",
    "http://www.w3.org/1999/02/22-rdf-syntax-ns#Bag"
  ],
  "abstract": {
    "en": "Laureate of the Laboratory for Excellence project (LabEx) in the program « Investment in the future », the DRIIHM LabEx, Device for Interdisciplinary Research on human-environments Interactions, aggregate 13 human-environments observatories (OHM in french), tools for observing socio-ecosystems impacted by anthropic events. Created by CNRS-INEE in 2007, they are located in metropolitan France, overseas France and abroad.",
    "fr": "Lauréat de la deuxième vague de l'appel à projet Laboratoire d'Excellence (LabEx) dans le cadre du programme « Investissements d'avenir », le LabEx DRIIHM, Dispositif de Recherche Interdisciplinaire sur les Interactions Hommes-Milieux, regroupe à ce jour 13 Observatoires Hommes-Milieux, outils d'observation de socio-écosystèmes impactés par un événement d'origine anthropique. Créés par le CNRS-INEE en 2007, ils sont répartis en France métropolitaine, en outre-mer et à l’étranger."
  }, "description": {
    "en": "InterDisciplinary Research Facility aggregating 13 human-environments observatories (OHM).",
    "fr": "Dispositif de Recherche Interdisciplinaire avec ses 13 Observatoires Hommes-Milieux (OHM)."
  },
  "identifier": "189088ec-baa9-4397-8c6f-eefde9a3790c",
  "title": {
    "fr": "Labex DRIIHM",
    "en": "DRIIHM"
  },
  "catalog": [
    "https://www.irit.fr/opencommon/resourceTree/MyResources",
    "https://opencommon.irit.fr/catalogs/6B8E175A-8C94-11EF-A39A-D6E96336453A",
    "https://opencommon.irit.fr/catalogs/AF1A5FAE-8C91-11EF-A39A-D6E96336453A"
  ],
  "logo": "https://www.driihm.fr/images/images/logos_png/logo_DRIIHM_r%C3%A9duit.png",
  "name": "driihm",
  "isSpaceOf": "https://www.irit.fr/opencommon/agents/organization/9a20f121-c64e-4049-93a7-4bedbe819fd6",
  "color": "olivine",
}