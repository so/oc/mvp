import type { OcDataset } from "@/declarations";

export const datasetExample1: OcDataset = {
  "@id": "urn:testdataset:nakala:6d79f9ed-e10e-4eea-8bfe-93c0bdf370dc",
  "@type": ["http://www.w3.org/ns/dcat#Dataset"],
  "identifier": "test-6d79f9ed-e10e-4eea-8bfe-93c0bdf370dc",
  "creator@string": "Coché, Lorraine; Arnaud, Elie; Bouveret, Laurent; David, Romain; Foulquier, Eric; Gandilhon, Nadège; Jeannesson, Etienne; Le Bras, Yvan; Lerigoleur, Emilie; Lopez, Pascal Jean; Madon, Bénédicte; Sananikone, Julien; Sèbe, Maxime; Le Berre, Iwan; Jung, Jean-Luc",
  "accrualPeriodicity": "http://publications.europa.eu/resource/authority/frequency/UNKNOWN",
  "bibliographicCitation": "Coché, L., Arnaud, E., Laurent, B., David, R., Foulquier, E., Gandilhon, N., Jeannesson, E., Le Bras, Y., Lerigoleur, E., Jean Lopez, P., Madon, B., Sananikone, J., Sèbe, M., Le Berre, I., & Jung, J.-L. (2021). Kakila database of marine mammal observation data around the French archipelago of Guadeloupe in the AGOA sanctuary - French Antilles [dataset]. PNDB Metacat PRODUCTION Node. https://doi.org/10.48502/8BB5-PK85",
  "creator": [
    {
      "@id": "urn:testdataset:nakala:benedictemadon",
      "familyName": "Madon",
      "givenName": "Bénédicte",
      "mbox": "mailto:benedicte.madon@gmail.com",
      "name": "Madon, Bénédicte"
    },
    {
      "@id": "urn:testdataset:nakala:eliearnaud",
      "familyName": "Arnaud",
      "givenName": "Elie",
      "mbox": "mailto:elie.arnaud@mnhn.fr",
      "name": "Arnaud, Elie"
    },
    {
      "@id": "urn:testdataset:nakala:emilielerigoleur",
      "familyName": "Lerigoleur",
      "givenName": "Emilie",
      "mbox": "mailto:emilie.lerigoleur@univ-tlse2.fr",
      "name": "Lerigoleur, Emilie"
    },
    {
      "@id": "urn:testdataset:nakala:ericfoulquier",
      "familyName": "Foulquier",
      "givenName": "Eric",
      "mbox": "mailto:eric.foulquier@univ-brest.fr",
      "name": "Foulquier, Eric"
    },
    {
      "@id": "urn:testdataset:nakala:etiennejeannesson",
      "familyName": "Jeannesson",
      "givenName": "Etienne",
      "mbox": "mailto:etienne.jeannesson@ofb.gouv.fr",
      "name": "Jeannesson, Etienne"
    },
    {
      "@id": "urn:testdataset:nakala:iwanleberre",
      "familyName": "Le Berre",
      "givenName": "Iwan",
      "mbox": "mailto:iwan.leberre@univ-brest.fr",
      "name": "Le Berre, Iwan"
    },
    {
      "@id": "urn:testdataset:nakala:jeanlucjung",
      "familyName": "Jung",
      "givenName": "Jean-Luc",
      "mbox": "mailto:jean-luc.jung@mnhn.fr",
      "name": "Jung, Jean-Luc"
    },
    {
      "@id": "urn:testdataset:nakala:juliensananikone",
      "familyName": "Sananikone",
      "givenName": "Julien",
      "mbox": "mailto:julien.sananikone@mnhn.fr",
      "name": "Sananikone, Julien"
    },
    {
      "@id": "urn:testdataset:nakala:laurentbouveret",
      "familyName": "Bouveret",
      "givenName": "Laurent",
      "mbox": "mailto:laurent.bouveret@gmail.com",
      "name": "Bouveret, Laurent"
    },
    {
      "@id": "urn:testdataset:nakala:lorrainecoche",
      "familyName": "Coché",
      "givenName": "Lorraine",
      "mbox": "mailto:lorraine.coche@gmail.com",
      "name": "Coché, Lorraine"
    },
    {
      "@id": "urn:testdataset:nakala:maximesebe",
      "familyName": "Sèbe",
      "givenName": "Maxime",
      "mbox": "mailto:maxime.sebe@gmail.com",
      "name": "Sèbe, Maxime"
    },
    {
      "@id": "urn:testdataset:nakala:nadegegandilhon",
      "familyName": "Gandilhon",
      "givenName": "Nadège",
      "mbox": "mailto:ngandilhon75@gmail.com",
      "name": "Gandilhon, Nadège"
    },
    {
      "@id": "urn:testdataset:nakala:pascaljeanlopez",
      "familyName": "Lopez",
      "givenName": "Pascal Jean",
      "mbox": "mailto:pjlopez@mnhn.fr",
      "name": "Lopez, Pascal Jean"
    },
    {
      "@id": "urn:testdataset:nakala:romaindavid",
      "familyName": "David",
      "givenName": "Romain",
      "mbox": "mailto:david.romain@gmail.com",
      "name": "David, Romain"
    },
    {
      "@id": "urn:testdataset:nakala:yvanlebras",
      "familyName": "Le Bras",
      "givenName": "Yvan",
      "mbox": "mailto:yvan.le-bras@mnhn.fr",
      "name": "Le Bras, Yvan"
    }
  ],
  "description": {
    "fr": "Base de données collectée dans le cadre du stage de master 2 Lorraine Coché \"Inventaire et structuration des données d'observation des mammifères marins autour de la Guadeloupe\" en 2020 (Master Écosystèmes marins tropicaux de l'Université des Antilles). Cette base de données a été constituée dans un esprit de science participative. Elle centralise et harmonise les données d'observation collectées par l'équipe du Sanctuaire Agoa (Aire Marine Protégée), l'OMMAG (Observatoire des Mammifères Marins de l'Archipel Guadeloupéen), BREACH Antilles, et les sociétés de whale-watching Cétacés Caraïbes, Guadeloupe Evasion Découverte et Aventures Marines.",
    "en": "Database collected as part of Lorraine Coché's Master 2 course entitled \"Inventory and structuring of marine mammal observation data around Guadeloupe\" in 2020 (Master's degree in Tropical Marine Ecosystems at the University of the West Indies). This database has been set up in the spirit of participatory science. It centralises and harmonises the observation data collected by the Agoa Sanctuary team (Marine Protected Area), OMMAG (Observatoire des Mammifères Marins de l'Archipel Guadeloupéen), BREACH Antilles, and the whale-watching companies Cétacés Caraïbes, Guadeloupe Evasion Découverte and Aventures Marines."
  },
  "otherIdentifier": [
    {
      "@id": "_:vb112236",
      "@type": "http://www.w3.org/ns/adms#Identifier",
      "creator": "https://doi.org/",
      "issued": new Date(Date.parse("2021-05-20")),
      "notation": "10.48502/8bb5-pk85",
      "schemeAgency": "DOI"
    },
    {
      "@id": "_:vb112249",
      "@type": "http://www.w3.org/ns/adms#Identifier",
      "creator": "https://doi.org/",
      "issued": new Date(Date.parse("2021-05-20")),
      "notation": "10.48502/8bb5-pk85",
      "schemeAgency": "DOI"
    },
    {
      "@id": "_:vb112262",
      "@type": "http://www.w3.org/ns/adms#Identifier",
      "creator": "https://doi.org/",
      "issued": new Date(Date.parse("2021-05-20")),
      "notation": "10.48502/8bb5-pk85",
      "schemeAgency": "DOI"
    }
  ],
  "isPartOf": "https://data.driihm.fr/catalogs/geo-ohm-lc",
  "isReferencedBy": "https://doi.org/10.3897/BDJ.9.e69022",
  "issued": new Date(Date.parse("2021-01-01")),
  "language": "http://publications.europa.eu/resource/authority/language/FRE",
  "modified": new Date(Date.parse("2023-12-04")),
  "provenance": [
    {
      "@id": "_:vb112239",
      "@type": "http://purl.org/dc/terms/ProvenanceStatement",
      "label": {
        "fr": "Kakila (« qui est là ? ») est une base de données d’observations de cétacés construite à partir de données hétérogènes, produite par divers acteurs impliqués dans l'observation des mammifères marins dans les Antilles françaises : l'équipe du Sanctuaire Agoa (Aire Marine Protégée), l'OMMAG (Observatoire des Mammifères Marins de l'Archipel Guadeloupéen), BREACH Antilles, et les sociétés de whale-watching Cétacés Caraïbes, Guadeloupe Evasion Découverte et Aventures Marines.\nPlus de 4700 observations, réalisées entre 2000 et 2019, y sont référencées. Vingt-et-une espèces différentes ont été identifiées, mysticètes comme odontocètes.\nKakila a été construite selon un protocole FAIR, visant à rendre les données faciles à retrouver, accessibles, interopérables et réutilisables. Kakila est intégralement accessible dans sa version originale dans le Pôle National de données de Biodiversité (PNDB) (doi:10.48502/8bb5-pk85), ainsi que dans le GBIF sous un format structuré standardisé selon le Darwin Core (https://doi.org/10.15468/qrhurc). Elle a également donné lieu à la publication d'un data paper dans Biodiversity Data Journal (https://doi.org/10.3897/BDJ.9.e69022)."
      }
    },
    {
      "@id": "_:vb112252",
      "@type": "http://purl.org/dc/terms/ProvenanceStatement",
      "label": {
        "fr": "Kakila (« qui est là ? ») est une base de données d’observations de cétacés construite à partir de données hétérogènes, produite par divers acteurs impliqués dans l'observation des mammifères marins dans les Antilles françaises : l'équipe du Sanctuaire Agoa (Aire Marine Protégée), l'OMMAG (Observatoire des Mammifères Marins de l'Archipel Guadeloupéen), BREACH Antilles, et les sociétés de whale-watching Cétacés Caraïbes, Guadeloupe Evasion Découverte et Aventures Marines.\nPlus de 4700 observations, réalisées entre 2000 et 2019, y sont référencées. Vingt-et-une espèces différentes ont été identifiées, mysticètes comme odontocètes.\nKakila a été construite selon un protocole FAIR, visant à rendre les données faciles à retrouver, accessibles, interopérables et réutilisables. Kakila est intégralement accessible dans sa version originale dans le Pôle National de données de Biodiversité (PNDB) (doi:10.48502/8bb5-pk85), ainsi que dans le GBIF sous un format structuré standardisé selon le Darwin Core (https://doi.org/10.15468/qrhurc). Elle a également donné lieu à la publication d'un data paper dans Biodiversity Data Journal (https://doi.org/10.3897/BDJ.9.e69022)."
      }
    },
    {
      "@id": "_:vb112265",
      "@type": "http://purl.org/dc/terms/ProvenanceStatement",
      "label": {
        "fr": "Kakila (« qui est là ? ») est une base de données d’observations de cétacés construite à partir de données hétérogènes, produite par divers acteurs impliqués dans l'observation des mammifères marins dans les Antilles françaises : l'équipe du Sanctuaire Agoa (Aire Marine Protégée), l'OMMAG (Observatoire des Mammifères Marins de l'Archipel Guadeloupéen), BREACH Antilles, et les sociétés de whale-watching Cétacés Caraïbes, Guadeloupe Evasion Découverte et Aventures Marines.\nPlus de 4700 observations, réalisées entre 2000 et 2019, y sont référencées. Vingt-et-une espèces différentes ont été identifiées, mysticètes comme odontocètes.\nKakila a été construite selon un protocole FAIR, visant à rendre les données faciles à retrouver, accessibles, interopérables et réutilisables. Kakila est intégralement accessible dans sa version originale dans le Pôle National de données de Biodiversité (PNDB) (doi:10.48502/8bb5-pk85), ainsi que dans le GBIF sous un format structuré standardisé selon le Darwin Core (https://doi.org/10.15468/qrhurc). Elle a également donné lieu à la publication d'un data paper dans Biodiversity Data Journal (https://doi.org/10.3897/BDJ.9.e69022)."
      }
    }
  ],
  "publisher": {
    "@id": "https://data.driihm.fr/agents/driihm",
    "mbox": "mailto:driihm-admin@services.cnrs.fr",
    "name": {
      "en": "InterDisciplinary Research Facility on Human-Environment Interactions",
      "fr": "Laboratoire d'Excellence Dispositif de Recherche Interdisciplinaire sur les Interactions Hommes-Milieux"
    }
  },
  "spatial": [
    {
      "@id": "_:vb112238",
      "@type": "http://purl.org/dc/terms/Location",
      "geometry": [
        {
          "@type": "http://www.opengis.net/ont/geosparql#gmlLiteral",
          "@value": "<gml:Envelope srsName=\"http://www.opengis.net/def/crs/OGC/1.3/CRS84\"><gml:lowerCorner>-61.8098386177696 15.832018691052</gml:lowerCorner><gml:upperCorner>-61.0019590353041 16.5144923539361</gml:upperCorner></gml:Envelope>"
        },
        {
          "@type": "http://www.openlinksw.com/schemas/virtrdf#Geometry",
          "@value": "POLYGON((-61.80983861777 16.514492353936,-61.001959035304 16.514492353936,-61.001959035304 15.832018691052,-61.80983861777 15.832018691052,-61.80983861777 16.514492353936))"
        }
      ]
    },
    {
      "@id": "_:vb112251",
      "@type": "http://purl.org/dc/terms/Location",
      "geometry": [
        {
          "@type": "http://www.opengis.net/ont/geosparql#gmlLiteral",
          "@value": "<gml:Envelope srsName=\"http://www.opengis.net/def/crs/OGC/1.3/CRS84\"><gml:lowerCorner>-61.8098386177696 15.832018691052</gml:lowerCorner><gml:upperCorner>-61.0019590353041 16.5144923539361</gml:upperCorner></gml:Envelope>"
        },
        {
          "@type": "http://www.openlinksw.com/schemas/virtrdf#Geometry",
          "@value": "POLYGON((-61.80983861777 16.514492353936,-61.001959035304 16.514492353936,-61.001959035304 15.832018691052,-61.80983861777 15.832018691052,-61.80983861777 16.514492353936))"
        }
      ]
    },
    {
      "@id": "_:vb112264",
      "@type": "http://purl.org/dc/terms/Location",
      "geometry": [
        {
          "@type": "http://www.opengis.net/ont/geosparql#gmlLiteral",
          "@value": "<gml:Envelope srsName=\"http://www.opengis.net/def/crs/OGC/1.3/CRS84\"><gml:lowerCorner>-61.8098386177696 15.832018691052</gml:lowerCorner><gml:upperCorner>-61.0019590353041 16.5144923539361</gml:upperCorner></gml:Envelope>"
        },
        {
          "@type": "http://www.openlinksw.com/schemas/virtrdf#Geometry",
          "@value": "POLYGON((-61.80983861777 16.514492353936,-61.001959035304 16.514492353936,-61.001959035304 15.832018691052,-61.80983861777 15.832018691052,-61.80983861777 16.514492353936))"
        }
      ]
    }
  ],
  "temporal": [
    [
      new Date(Date.parse('2019-12-31')),
      new Date(Date.parse('2000-01-01'))
    ],
    [
      new Date(Date.parse('2019-12-31')),
      new Date(Date.parse('2000-01-01'))
    ],
    [
      new Date(Date.parse('2019-12-31')),
      new Date(Date.parse('2000-01-01'))
    ]
  ],
  "title": {
    "fr": "KAKILA, Base de données d'observation des mammifères marins autour de l'archipel de la Guadeloupe dans le sanctuaire AGOA - Antilles françaises",
    "en": "KAKILA, Marine mammal observation database around the Guadeloupe archipelago in the AGOA sanctuary - French West Indies"
  },
  "sameAs": "https://doi.org/10.48502/8bb5-pk85",
  "status": {
    "@id": "http://publications.europa.eu/resource/authority/dataset-status/COMPLETED",
    "prefLabel": {
      "en": "completed",
      "fi": "valmis",
      "hr": "potpun",
      "it": "completato",
      "el": "πλήρη δεδομένα",
      "fr": "complété",
      "nl": "voltooid",
      "sk": "dokončený",
      "sl": "dokončano",
      "hu": "kész",
      "ga": "lánchríochnaithe",
      "lv": "pabeigts",
      "de": "vollständig",
      "et": "täielik",
      "lt": "baigtas",
      "no": "ferdigstilt",
      "nb": "ferdigstilt",
      "nn": "ferdigstilt",
      "sv": "fullständig",
      "da": "færdiggjort",
      "pt": "completado",
      "ro": "finalizat",
      "bg": "пълен",
      "cs": "dokončen",
      "mt": "komplut",
      "pl": "ukończony",
      "es": "completo"
    }
  },
  "contactPoint": [
    {
      "@id": "urn:testdataset:nakala:iwanleberre",
      "familyName": "Le Berre",
      "givenName": "Iwan",
      "mbox": "mailto:iwan.leberre@univ-brest.fr",
      "name": "Le Berre, Iwan"
    },
    {
      "@id": "urn:testdataset:nakala:jeanlucjung",
      "familyName": "Jung",
      "givenName": "Jean-Luc",
      "mbox": "mailto:jean-luc.jung@mnhn.fr",
      "name": "Jung, Jean-Luc"
    },
    {
      "@id": "urn:testdataset:nakala:lorrainecoche",
      "familyName": "Coché",
      "givenName": "Lorraine",
      "mbox": "mailto:lorraine.coche@gmail.com",
      "name": "Coché, Lorraine"
    }
  ],
  "distribution": [
    {
      "@id": "urn:testdistribution:nakala:kakila-database",
      "@type": "http://www.w3.org/ns/dcat#Distribution",
      "accessRights": "http://data.jrc.ec.europa.eu/access-rights/no-limitations",
      "description": {
        "fr": "Base de données collectée dans le cadre du stage de master 2 Lorraine Coché \"Inventaire et structuration des données d'observation des mammifères marins autour de la Guadeloupe\" en 2020 (Master Ecosystèmes marins tropicaux de l'Université des Antilles). Cette base de données centralise et harmonise les données collectées par l'équipe du Sanctuaire Agoa (Aire Marine Protégée), l'OMMAG (Observatoire des Mammifères Marins de l'Archipel Guadeloupéen), l'ASBL BREACH, et les sociétés de whale-watching Cétacés Caraïbes, Guadeloupe Evasion Découverte et Aventures Marines.",
        "en": "Database collected as part of the Lorraine Coché master's 2 internship \"Inventory and structuring of marine mammal observation data around Guadeloupe\" in 2020 (Master Tropical marine ecosystems at the University of the Antilles). This database centralizes and harmonizes the data collected by the team of the Agoa Sanctuary (Aire Marine Protégée), the OMMAG (Observatory of Marine Mammals of the Guadeloupe Archipelago), the NPO BREACH, and whale-watching companies Cétacés Caraïbes, Guadeloupe Evasion Découverte et Aventures Marines. "
      },
      "format": "http://publications.europa.eu/resource/authority/file-type/ZIP",
      "identifier": "test-kakila-database.zip",
      "license": "https://spdx.org/licenses/CC-BY-4.0.html#licenseText",
      "title": {
        "fr": "Base de données Kakila des données d'observation des mammifères marins autour de l'archipel français de la Guadeloupe dans le sanctuaire AGOA - Antilles françaises",
        "en": "Kakila database of marine mammal observation data around the French archipelago of Guadeloupe in the AGOA sanctuary - French Antilles"
      },
      "type": "http://purl.org/coar/resource_type/c_ddb1",
      "accessURL": "https://data.pndb.fr/view/doi:10.48502/8bb5-pk85"
    }
  ],
  "keyword": {
    "fr": [
      "France",
      "zone marine protégée",
      "OHM Littoral Caraïbe",
      "cétacé",
      "mammifère marin",
      "Caraïbes (les)",
      "espace naturel",
      "Guadeloupe"
    ],
    "en": "Caribbean Sea"
  },
  "landingPage": "https://opencommon.irit.fr/driihm/datasets/test-6d79f9ed-e10e-4eea-8bfe-93c0bdf370dc/descriptor",
  "theme": [
    {
      "@id": "http://www.eionet.europa.eu/gemet/concept/2463",
      "prefLabel": {
        "sv": "ekologisk inventering",
        "da": "økologisk opgørelse",
        "lv": "ekoloģiskā mijiedarbības un situācijas novērtēšana",
        "fi": "ekologinen inventointi",
        "en": "ecological stocktaking",
        "en-us": "ecological stocktaking",
        "ga": "stocáireamh éiceolaíoch",
        "ar": "الجرد الإيكولوجي",
        "es": "inventario ecológico",
        "nl": "ecologische inventarisatie",
        "uk": "екологічна інвентаризація",
        "lt": "ekologinė inventorizacija",
        "eu": "inbentario ekologiko",
        "hr": "ekološka revizija",
        "bg": "Екологичен преглед на постигнатото",
        "it": "inventario ecologico",
        "el": "οικολογική απογραφή",
        "sk": "ekologická inventarizácia",
        "ro": "inventariere ecologică",
        "mt": "inventarju ekoloġiku",
        "no": "økologisk bestandsanalyse",
        "de": "Ökologische Bestandsaufnahme",
        "ca": "inventari ecològic",
        "sl": "okoljski pregled (v podjetju)",
        "ru": "экологическая инвентаризация",
        "hy": "էկոլոգիական հաշվառում",
        "fr": "inventaire écologique",
        "hu": "ökológiai készletezés",
        "cs": "inventarizace ekologická",
        "tr": "ekolojik envanter",
        "is": "vistfræðileg úttekt",
        "zh-cn": "生态盘查",
        "az": "ekoloji inventarizasiya",
        "ka": "ეკოლოგიური ინვენტარიზაცია",
        "pt": "inventariação ecológica",
        "et": "ökoloogiline loendus",
        "pl": "inwentaryzacja ekologiczna"
      }
    },
    {
      "@id": "http://www.eionet.europa.eu/gemet/concept/2519",
      "prefLabel": {
        "ar": "النظام الإيكولوجي",
        "no": "økosystem",
        "da": "økosystem",
        "zh-cn": "生态系统",
        "fi": "ekosysteemi",
        "et": "ökosüsteem",
        "pt": "ecossistemas",
        "hr": "ekosustav",
        "cs": "ekosystém",
        "sk": "ekosystém",
        "hy": "էկոհամակարգ",
        "ca": "ecosistema",
        "it": "ecosistema",
        "en": "ecosystem",
        "en-us": "ecosystem",
        "pl": "ekosystem",
        "sv": "ekosystem",
        "ru": "экосистема",
        "es": "ecosistemas",
        "nl": "ecosysteem",
        "is": "vistkerfi",
        "mt": "ekosistema",
        "lt": "ekosistema",
        "eu": "ekosistema",
        "bg": "Екосистема",
        "fr": "écosystème",
        "el": "οικοσύστημα",
        "ro": "ecosistem",
        "de": "Ökosystem",
        "lv": "ekosistēma",
        "ga": "éiceachóras",
        "tr": "ekosistem",
        "sl": "ekosistem",
        "az": "ekosistem",
        "hu": "ökoszisztéma",
        "ka": "ეკოსისტემა",
        "uk": "екосистема"
      }
    },
    {
      "@id": "http://www.eionet.europa.eu/gemet/concept/420",
      "prefLabel": {
        "lv": "dzīvnieku ekoloģija",
        "ro": "ecologie animală",
        "ca": "ecologia animal",
        "pt": "ecologia animal",
        "pl": "ekologia zwierząt",
        "mt": "ekoloġija tal-annimali",
        "eu": "animalien ekologia",
        "de": "Tierökologie",
        "tr": "hayvan ekolojisi",
        "hy": "կենդանիների էկոլոգիա",
        "zh-cn": "动物生态学",
        "en": "animal ecology",
        "en-us": "animal ecology",
        "bg": "Животинска екология",
        "ga": "éiceolaíocht ainmhithe",
        "cs": "ekologie živočichů",
        "ka": "ცხოველთა ეკოლოგია",
        "no": "dyreøkologi",
        "da": "dyreøkologi",
        "fi": "eläinekologia",
        "uk": "екологія тварин",
        "fr": "écologie animale",
        "es": "ecología animal",
        "az": "heyvanların ekologiyası",
        "hr": "ekologija životinja",
        "et": "loomaökoloogia",
        "it": "ecologia animale",
        "nl": "dierenecologie",
        "el": "ζωοοικολογία",
        "ru": "экология животных",
        "hu": "állatökológia",
        "sk": "ekológia živočíchov",
        "sl": "ekologija živali",
        "ar": "إيكولوجيا الحيوان",
        "is": "dýravistfræði",
        "lt": "gyvūnų ekologija",
        "sv": "djurekologi"
      }
    },
    {
      "@id": "http://www.eionet.europa.eu/gemet/concept/4648",
      "prefLabel": {
        "sv": "landskap",
        "no": "landskap",
        "eu": "paisaia",
        "de": "Landschaft",
        "hu": "táj",
        "hy": "լանդշաֆտ",
        "pt": "paisagem",
        "ca": "paisatge",
        "uk": "ландшафт",
        "bg": "ландшафт",
        "ru": "ландшафт",
        "ro": "peisaj",
        "ar": "منظر طبيعي",
        "cs": "krajina",
        "sk": "krajina",
        "pl": "krajobraz",
        "hr": "krajobraz",
        "tr": "peyzaj",
        "sl": "pokrajina, krajina",
        "et": "maastik",
        "is": "landslag",
        "mt": "pajsaġġ",
        "lt": "kraštovaizdis",
        "fr": "paysage",
        "en-us": "landscape",
        "en": "landscape",
        "nl": "landschap",
        "ka": "ლანდშაფტი",
        "az": "landşaft",
        "lv": "ainava",
        "ga": "tírdhreach",
        "es": "paisaje",
        "it": "paesaggio",
        "fi": "maisema",
        "el": "τοπίο",
        "da": "landskab",
        "zh-cn": "景观"
      }
    },
    {
      "@id": "http://www.eionet.europa.eu/gemet/concept/5494",
      "prefLabel": {
        "ru": "природная зона",
        "cs": "oblast přírodní",
        "lt": "natūrali gamtinė zona",
        "pl": "obszar przyrody niezakłóconej przez człowieka",
        "mt": "żona naturali",
        "pt": "áreas naturais",
        "sv": "naturområde",
        "no": "naturområde",
        "da": "naturområde",
        "ro": "zonă naturală",
        "ar": "منطقة طبيعية",
        "en-us": "natural area",
        "en": "natural area",
        "hu": "természeti terület",
        "es": "areas naturales",
        "it": "area naturale",
        "hy": "բնական գոտի",
        "sl": "naravno območje",
        "az": "təbiət zonası",
        "bg": "Природна област",
        "is": "náttúrusvæði",
        "ga": "limistéar nádúrtha",
        "ka": "ბუნებრივი ზონა",
        "fr": "espace naturel",
        "zh-cn": "自然区",
        "sk": "prírodná oblasť",
        "de": "Naturgebiet",
        "eu": "natur gune",
        "et": "loodusmaistu",
        "lv": "dabiska (mazskarta) teritorija",
        "fi": "luonnonvarainen alue",
        "nl": "natuurgebied",
        "el": "φυσικό καταφύγιο",
        "tr": "doğal alan",
        "hr": "prirodno područje",
        "uk": "природна зона",
        "ca": "àrea natural"
      }
    }
  ]
}