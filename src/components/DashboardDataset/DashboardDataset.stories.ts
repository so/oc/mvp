import type { Meta, StoryObj } from '@storybook/vue3';

import DashboardDataset from './DashboardDataset.vue';
import { datasetSummaries, distributionSummaries } from './exampleSummaries'
import type { OcDatasetSummary } from '@/declarations';
import { communityExample1 } from '@/assets/communityExample1';

const meta: Meta<typeof DashboardDataset> = {
  component: DashboardDataset,
};

export default meta;
type Story = StoryObj<typeof DashboardDataset>;

export const Default: Story = {
  render: (args) => ({
    components: { DashboardDataset },
    setup() {
      return { args };
    },
    template: '<DashboardDataset v-bind="args" />',
  }),
  args: {
    community: communityExample1,
    datasets: datasetSummaries as OcDatasetSummary[],
    distributionsCallback: (_: string[]) => new Promise((resolve) => {
      setTimeout(() => {
        resolve(distributionSummaries);
      }, 300)
    }),
  },
};
