import type { OcDatasetSummary, OcDistributionSummary } from "@/declarations";

export const distributionSummaries: OcDistributionSummary[] = [
  {
    "@id": "https://www.irit.fr/distributions/7f452387-c7ef-4928-b776-b5c3f9080769",
    "@type": ["http://www.w3.org/ns/dcat#Distribution"],
    "description": { "@none": "distribution 1 desc" },
    "identifier": "7f452387-c7ef-4928-b776-b5c3f9080769",
    "license": "http://publications.europa.eu/resource/authority/licence/APACHE_2_0",
    issued: new Date(),
    "title": { "@none": "distribution 1" },
    "graph": ["https://www.irit.fr/opencommon/members/41932a13-a79f-4ad5-b5a1-d2106d46c626/private"]
  },
  {
    "@id": "https://www.irit.fr/distributions/7f452387-c7ef-4928-b776-b5c3f9080769",
    "@type": ["http://www.w3.org/ns/dcat#Distribution"],
    "description": { "@none": "distribution 2 desc" },
    "identifier": "7f452387-c7ef-4928-b776-b5c3f9080769",
    "license": "http://publications.europa.eu/resource/authority/licence/APACHE_2_0",
    issued: new Date(),
    "title": { "@none": "distribution 2" },
    "graph": ["https://www.irit.fr/opencommon/members/41932a13-a79f-4ad5-b5a1-d2106d46c626/private"]
  },
]

export const datasetSummaries: OcDatasetSummary[] = [
  {
    "@id": "https://www.irit.fr/datasets/7e32d839-8884-4939-a992-7ad318a79451",
    "@type": ["http://www.w3.org/ns/dcat#Dataset"],
    "description": { "fr": "description fr", "en": "description en" },
    "identifier": "7e32d839-8884-4939-a992-7ad318a79451",
    "issued": new Date(),
    "license": "http://publications.europa.eu/resource/authority/licence/APSL_2_0",
    "modified": new Date(),
    "title": { "en": "title en", "fr": "titre fr 2" },
    "graph": ["https://www.irit.fr/opencommon/members/41932a13-a79f-4ad5-b5a1-d2106d46c626/private"]
  },
  {
    "@id": "https://www.irit.fr/datasets/96cdccec-e3c8-4a86-a187-1fdd75055f27",
    "@type": ["http://www.w3.org/ns/dcat#Dataset", "http://www.w3.org/ns/dcat#Distribution"],
    "description": {
      "fr": "un jeu de test avec distributions Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum",
      "en": "test dataset with ditributions Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum"
    },
    "identifier": "96cdccec-e3c8-4a86-a187-1fdd75055f27",
    "issued": new Date(),
    "license": "http://publications.europa.eu/resource/authority/licence/AGPL_3_0",
    "modified": new Date(),
    "title": {
      "en": "test dataset with ditributions ",
      "fr": "jeu de test avec distributions"
    },
    "status": "http://publications.europa.eu/resource/authority/dataset-status/DEPRECATED",
    "distribution": ["https://www.irit.fr/distributions/96cdccec-e3c8-4a86-a187-1fdd75055f27"],
    "version": "0.0.1",
    "graph": ["https://www.irit.fr/opencommon/members/41932a13-a79f-4ad5-b5a1-d2106d46c626/private"]
  },
  {
    "@id": "https://www.irit.fr/datasets/47fda623-c4f0-41aa-9dd9-8440467d82ca",
    "@type": ["http://www.w3.org/ns/dcat#Dataset"],
    "description": { "fr": "description fr", "en": "description en" },
    "identifier": "47fda623-c4f0-41aa-9dd9-8440467d82ca",
    "issued": new Date(),
    "license": "http://publications.europa.eu/resource/authority/licence/APSL_2_0",
    "modified": new Date(),
    "title": { "fr": "titre fr", "en": "title en" },
    "version": "0.0.1",
    "graph": ["https://www.irit.fr/opencommon/members/41932a13-a79f-4ad5-b5a1-d2106d46c626/private"]
  },
  {
    "@id": "https://www.irit.fr/datasets/7f452387-c7ef-4928-b776-b5c3f9080769",
    "@type": ["http://www.w3.org/ns/dcat#Dataset"],
    "description": {
      "fr": "un autre jeu de test avec distributions desc",
      "en": "test dataset with ditributions desc"
    },
    "identifier": "7f452387-c7ef-4928-b776-b5c3f9080769",
    "issued": new Date(),
    "modified": new Date(),
    "title": {
      "fr": "un autre jeu de test avec distributions",
      "en": "another test dataset with ditributions"
    },
    "status": "http://publications.europa.eu/resource/authority/dataset-status/DEPRECATED",
    "distribution": ["https://www.irit.fr/distributions/7f452387-c7ef-4928-b776-b5c3f9080769"],
    "version": "0.0.1",
    "graph": [
      "https://www.irit.fr/opencommon/members/41932a13-a79f-4ad5-b5a1-d2106d46c626/private"
    ],
  }
]
