import type { Meta, StoryObj } from '@storybook/vue3';

import OcDatasetFormStep1 from './OcDatasetFormStep1.vue';

const meta: Meta<typeof OcDatasetFormStep1> = {
  component: OcDatasetFormStep1,
};

export default meta;
type Story = StoryObj<typeof OcDatasetFormStep1>;

export const Default: Story = {
  render: (args) => ({
    components: { OcDatasetFormStep1 },
    setup() {
      return { args };
    },
    template: '<OcDatasetFormStep1 v-bind="args" />',
  }),
  args: {
    modelValue: {}
  },
};
