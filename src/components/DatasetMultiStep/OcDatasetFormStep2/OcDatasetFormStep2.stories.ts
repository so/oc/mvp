import type { Meta, StoryObj } from '@storybook/vue3';

import OcDatasetFormStep2 from './OcDatasetFormStep2.vue';
import { communityExample1 } from '@/assets/communityExample1';

const meta: Meta<typeof OcDatasetFormStep2> = {
  component: OcDatasetFormStep2,
};

export default meta;
type Story = StoryObj<typeof OcDatasetFormStep2>;

export const Default: Story = {
  render: (args) => ({
    components: { OcDatasetFormStep2 },
    setup() {
      return { args };
    },
    template: '<OcDatasetFormStep2 v-bind="args" />',
  }),
  args: {
    modelValue: {},
    community: communityExample1,
    userPrivateGraph: 'private-graph',
    auth: {
      email: import.meta.env.VITE_VISITOR_USER,
      password: import.meta.env.VITE_VISITOR_PWD,
    },
  },
};
