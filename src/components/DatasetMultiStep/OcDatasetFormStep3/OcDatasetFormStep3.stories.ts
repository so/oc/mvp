import type { Meta, StoryObj } from '@storybook/vue3';

import OcDatasetFormStep3 from './OcDatasetFormStep3.vue';

const meta: Meta<typeof OcDatasetFormStep3> = {
  component: OcDatasetFormStep3,
};

export default meta;
type Story = StoryObj<typeof OcDatasetFormStep3>;

export const Default: Story = {
  render: (args) => ({
    components: { OcDatasetFormStep3 },
    setup() {
      return { args };
    },
    template: '<OcDatasetFormStep3 v-bind="args" />',
  }),
  args: {
    modelValue: {}
  },
};
