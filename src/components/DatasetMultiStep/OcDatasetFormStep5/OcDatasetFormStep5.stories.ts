import type { Meta, StoryObj } from '@storybook/vue3';

import OcDatasetFormStep5 from './OcDatasetFormStep5.vue';

const meta: Meta<typeof OcDatasetFormStep5> = {
  component: OcDatasetFormStep5,
};

export default meta;
type Story = StoryObj<typeof OcDatasetFormStep5>;

export const Default: Story = {
  render: (args) => ({
    components: { OcDatasetFormStep5 },
    setup() {
      return { args };
    },
    template: '<OcDatasetFormStep5 v-bind="args" />',
  }),
  args: {
    modelValue: {
      "@id": "",
      "@type": "",
      "title": "My title",
      "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. \nDuis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
      "catalog": { "@id": "https://www.irit.fr/opencommon/resourceTree/MyData", "title": "My data" },
      "creator": [
        {
          "@id": "https://www.irit.fr/opencommon/agents/organization/9e33853c-a19c-424e-9f39-1f8eee395a32",
          "@type": ["http://xmlns.com/foaf/0.1/Agent", "http://xmlns.com/foaf/0.1/Organization"],
          "name": { "@none": "Université de Toulouse" }
        },
        {
          "@id": "https://www.irit.fr/opencommon/agents/organization/9e33853c-a19c-424e-9f39-1f8eee395a32",
          "@type": ["http://xmlns.com/foaf/0.1/Agent", "http://xmlns.com/foaf/0.1/Person"],
          "firstName": "Jean",
          "familyName": "Durand",
        },
        {
          "@id": "https://www.irit.fr/opencommon/agents/organization/9e33853c-a19c-424e-9f39-1f8eee395a32",
          "@type": ["http://xmlns.com/foaf/0.1/Agent", "http://xmlns.com/foaf/0.1/Person"],
          "firstName": "Julie",
          "familyName": "Dupuis",
        },
        {
          "@id": "https://www.irit.fr/opencommon/agents/organization/9e33853c-a19c-424e-9f39-1f8eee395a32",
          "@type": ["http://xmlns.com/foaf/0.1/Agent", "http://xmlns.com/foaf/0.1/Person"],
          "firstName": "Ismael",
          "familyName": "van der Hoog",
        },
        {
          "@id": "https://www.irit.fr/opencommon/agents/organization/9e33853c-a19c-424e-9f39-1f8eee395a32",
          "@type": ["http://xmlns.com/foaf/0.1/Agent", "http://xmlns.com/foaf/0.1/Person"],
          "firstName": "Bob",
          "familyName": "Kavinsky",
        },
      ],
      "publisher": {
        "@id": "https://www.irit.fr/opencommon/agents/organization/9e33853c-a19c-424e-9f39-1f8eee395a32",
        "@type": ["http://xmlns.com/foaf/0.1/Agent", "http://xmlns.com/foaf/0.1/Organization"],
        "name": { "@none": "Université de Toulouse" }
      },
      "contactPoint": {
        "@id": "https://www.irit.fr/opencommon/agents/organization/9e33853c-a19c-424e-9f39-1f8eee395a32",
        "@type": ["http://xmlns.com/foaf/0.1/Agent", "http://xmlns.com/foaf/0.1/Organization"],
        "name": { "@none": "Université de Toulouse" }
      },
      "type": {
        "@id": "http://publications.europa.eu/resource/authority/dataset-type/NAL",
        "prefLabel": "Liste d’autorité de noms"
      },
      "theme": [
        { "@id": "http://publications.europa.eu/resource/authority/data-theme/SOCI", "prefLabel": "Population et société" },
        { "@id": "http://publications.europa.eu/resource/authority/data-theme/SOCI", "prefLabel": "International issues" },
        { "@id": "http://publications.europa.eu/resource/authority/data-theme/SOCI", "prefLabel": "Environment" },
      ],
      "accrualPeriodicity": {
        "@id": "http://publications.europa.eu/resource/authority/frequency/BIENNIAL",
        "prefLabel": { "fr": "biennal", "en": "biennial" }
      },
      "issued": new Date('2024-10-11T00:00:00.000'),
      "language": [
        { "@id": "http://publications.europa.eu/resource/authority/language/CYB", "prefLabel": { "fr": "cayubaba" } },
        { "@id": "http://publications.europa.eu/resource/authority/language/EPO", "prefLabel": { "fr": "espéranto" } }
      ],
      "keyword": ["Population", "ADN microsatellites", "ADN", "microsatellites"],
      "status": { "@id": "http://publications.europa.eu/resource/authority/dataset-status/DEPRECATED", "prefLabel": { "fr": "déconseillé", "en": "deprecated" } },
      "modified": new Date('2024-10-11T00:00:00.000'),
      "temporal": [
        new Date('2024-10-01T00:00:00.000'),
        new Date('2024-10-27T00:00:00.000')
      ],
      "spatial": { "@id": "http://publications.europa.eu/resource/authority/continent/ANTARCTICA", "prefLabel": "Antarctique" },
      "landingPage": "https://duckduckgo.fr",
      "license": { "@id": "http://publications.europa.eu/resource/authority/licence/CC_BY_1_0", "prefLabel": { "en": "Creative Commons Attribution 1.0 Generic", "fr": "Creative Commons Attribution 1.0 Générique", } },
      "conformsTo": { "@id": "http://docs.geoserver.org/latest/en/user/rest", "prefLabel": { "en": "GeoServer REST configuration API" } },
      "version": "0.0.1",
      "distribution": [
        {
          "accessService": {
            "@id": "https://data.driihm.fr/services/87a56f90-aff8-4502-a137-39c0a6b41f58",
            "@type": "http://www.w3.org/ns/dcat#DataService",
            "title": { "@none": "Test service avec enpointIRI" },
          },
          "accessRights": { "@id": "http://data.jrc.ec.europa.eu/access-rights/authorisation-required", "prefLabel": { "en": "Authorisation required" } },
          "accessURL": "https://duckduckgo.fr",
          "conformsTo": { "@id": "http://docs.geoserver.org/latest/en/user/rest", "prefLabel": { "en": "GeoServer REST configuration API" } },
          "format": { "@id": "http://publications.europa.eu/resource/authority/file-type/AAB", "prefLabel": { "en": "AAB", "et": "AAB" } },
          "issued": new Date('2024-10-01T00:00:00.000'),
          "type": { "@id": "http://purl.org/coar/resource_type/H41Y-FW7B", "prefLabel": "carnet de laboratoire" },
          "license": { "@id": "http://publications.europa.eu/resource/authority/licence/AGPL_3_0", "prefLabel": { "en": "GNU Affero General Public License version 3" } },
          "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
          "title": "My distribution"
        },
        {
          "accessService": {
            "@id": "https://data.driihm.fr/services/87a56f90-aff8-4502-a137-39c0a6b41f58",
            "@type": "http://www.w3.org/ns/dcat#DataService",
            "title": { "@none": "Test service avec enpointIRI" },
          },
          "accessRights": { "@id": "http://data.jrc.ec.europa.eu/access-rights/authorisation-required", "prefLabel": { "en": "Authorisation required" } },
          "accessURL": "https://duckduckgo.fr",
          "conformsTo": { "@id": "http://docs.geoserver.org/latest/en/user/rest", "prefLabel": { "en": "GeoServer REST configuration API" } },
          "format": { "@id": "http://publications.europa.eu/resource/authority/file-type/AAB", "prefLabel": { "en": "AAB", "et": "AAB" } },
          "issued": new Date('2024-10-01T00:00:00.000'),
          "type": { "@id": "http://purl.org/coar/resource_type/H41Y-FW7B", "prefLabel": "carnet de laboratoire" },
          "license": { "@id": "http://publications.europa.eu/resource/authority/licence/AGPL_3_0", "prefLabel": { "en": "GNU Affero General Public License version 3" } },
          "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
          "title": "My distribution 2"
        }
      ]
    },
  },
};
