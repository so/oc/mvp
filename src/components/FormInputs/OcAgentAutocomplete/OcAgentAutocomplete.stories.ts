import type { Meta, StoryObj } from '@storybook/vue3';

import OcAgentAutocomplete from './OcAgentAutocomplete.vue';

const meta: Meta<typeof OcAgentAutocomplete> = {
  component: OcAgentAutocomplete,
};

export default meta;
type Story = StoryObj<typeof OcAgentAutocomplete>;

export const OrganizationAndPerson: Story = {
  render: (args) => ({
    components: { OcAgentAutocomplete },
    setup() {
      return { args };
    },
    template: '<OcAgentAutocomplete v-bind="args" />',
  }),
  args: {},
};

export const OrganizationOnly: Story = {
  render: (args) => ({
    components: { OcAgentAutocomplete },
    setup() {
      return { args };
    },
    template: '<OcAgentAutocomplete v-bind="args" />',
  }),
  args: {
    targets: "organization"
  }
};

export const PersonOnly: Story = {
  render: (args) => ({
    components: { OcAgentAutocomplete },
    setup() {
      return { args };
    },
    template: '<OcAgentAutocomplete v-bind="args" />',
  }),
  args: {
    targets: "person"
  }
};