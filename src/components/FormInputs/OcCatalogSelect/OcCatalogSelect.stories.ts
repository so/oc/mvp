import type { Meta, StoryObj } from '@storybook/vue3';

import OcCatalogSelect from './OcCatalogSelect.vue';
import { communityExample1 } from '@/assets/communityExample1';

const meta: Meta<typeof OcCatalogSelect> = {
  component: OcCatalogSelect,
};

export default meta;
type Story = StoryObj<typeof OcCatalogSelect>;

export const Default: Story = {
  render: (args) => ({
    components: { OcCatalogSelect },
    setup() {
      return { args };
    },
    template: '<OcCatalogSelect v-bind="args" />',
  }),
  args: {
    community: communityExample1,
    auth: {
      email: import.meta.env.VITE_VISITOR_USER,
      password: import.meta.env.VITE_VISITOR_PWD,
    }
  },
};
