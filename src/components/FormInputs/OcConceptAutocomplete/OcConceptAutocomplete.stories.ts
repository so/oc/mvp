import type { Meta, StoryObj } from '@storybook/vue3';

import OcConceptAutocomplete from './OcConceptAutocomplete.vue';

const meta: Meta<typeof OcConceptAutocomplete> = {
  component: OcConceptAutocomplete,
};

export default meta;
type Story = StoryObj<typeof OcConceptAutocomplete>;

export const Default: Story = {
  render: (args) => ({
    components: { OcConceptAutocomplete },
    setup() {
      return { args };
    },
    template: '<OcConceptAutocomplete v-bind="args" />',
  }),
  args: {
    vocabulariesUriList: ['https://data.archives-ouvertes.fr/subject', 'http://publications.europa.eu/resource/authority/country'],
  },
};

export const NoneMultiple: Story = {
  render: (args) => ({
    components: { OcConceptAutocomplete },
    setup() {
      return { args };
    },
    template: '<OcConceptAutocomplete v-bind="args" />',
  }),
  args: {
    vocabulariesUriList: ['https://data.archives-ouvertes.fr/subject', 'http://publications.europa.eu/resource/authority/country'],
    multiple: false
  },
};

export const WithBrowser: Story = {
  render: (args) => ({
    components: { OcConceptAutocomplete },
    setup() {
      return { args };
    },
    template: '<OcConceptAutocomplete v-bind="args" />',
  }),
  args: {
    vocabulariesUriList: ['https://data.archives-ouvertes.fr/subject', 'http://publications.europa.eu/resource/authority/country'],
    withBrowser: true
  },
};

export const WithBrowserNotMutliple: Story = {
  render: (args) => ({
    components: { OcConceptAutocomplete },
    setup() {
      return { args };
    },
    template: '<OcConceptAutocomplete v-bind="args" />',
  }),
  args: {
    vocabulariesUriList: ['https://data.archives-ouvertes.fr/subject', 'http://publications.europa.eu/resource/authority/country'],
    withBrowser: true,
    multiple: false
  },
};