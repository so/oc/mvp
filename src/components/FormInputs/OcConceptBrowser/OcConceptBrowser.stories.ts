import type { Meta, StoryObj } from '@storybook/vue3'

import OcConceptBrowser from './OcConceptBrowser.vue'

const meta: Meta<typeof OcConceptBrowser> = {
  component: OcConceptBrowser
}

export default meta
type Story = StoryObj<typeof OcConceptBrowser>

export const Default: Story = {
  render: (args) => ({
    components: { OcConceptBrowser },
    setup() {
      return { args }
    },
    template: '<OcConceptBrowser v-bind="args" />'
  }),
  args: {
    vocabulariesUriList: ['https://data.archives-ouvertes.fr/subject', 'http://publications.europa.eu/resource/authority/country'],
  }
}