import type { Meta, StoryObj } from '@storybook/vue3';

import OcConceptSelect from './OcConceptSelect.vue';

const meta: Meta<typeof OcConceptSelect> = {
  component: OcConceptSelect,
};

export default meta;
type Story = StoryObj<typeof OcConceptSelect>;

export const Default: Story = {
  render: (args) => ({
    components: { OcConceptSelect },
    setup() {
      return { args };
    },
    template: '<OcConceptSelect v-bind="args" />',
  }),
  args: {
    vocabulariesUriList: ["http://publications.europa.eu/resource/authority/licence"],
  },
};
