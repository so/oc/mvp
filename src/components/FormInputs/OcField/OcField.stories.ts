import type { Meta, StoryObj } from '@storybook/vue3';

import OcField from './OcField.vue';
import InputText from 'primevue/inputtext';

const meta: Meta<typeof OcField> = {
  component: OcField,
};

export default meta;
type Story = StoryObj<typeof OcField>;

export const Default: Story = {
  render: (args) => ({
    components: { OcField, InputText },
    setup() {
      return { args };
    },
    template: '<OcField v-bind="args"><InputText fluid/></OcField>',
  }),
  args: {
    metadata: {
      label: { en: 'My field', fr: 'Mon champ' },
      required: true,
      propertyUri: 'https://duckduckgo.com',
      dereferencement: 'https://duckduckgo.com',
      desc: { en: 'My field should be filled with a cool value', fr: 'Mon champ doit être rempli avec une valeur cool' },
      fair: ['f', 'a', 'r'],
      comment: { en: 'My field could help user somehow', fr: 'D\'une certaine manière, Mon champ pourrait bien aider quelqu\'un' },
      vocabularies: undefined
    },
  },
};

export const WithFairBadges: Story = {
  render: (args) => ({
    components: { OcField, InputText },
    setup() {
      return { args };
    },
    template: '<OcField v-bind="args"><InputText fluid/></OcField>',
  }),
  args: {
    showFairBadges: true,
    metadata: {
      label: { en: 'My field', fr: 'Mon champ' },
      required: true,
      propertyUri: 'https://duckduckgo.com',
      dereferencement: 'https://duckduckgo.com',
      desc: { en: 'My field should be filled with a cool value', fr: 'Mon champ doit être rempli avec une valeur cool' },
      fair: ['f', 'a', 'r'],
      comment: { en: 'My field could help user somehow', fr: 'D\'une certaine manière, Mon champ pourrait bien aider quelqu\'un' },
      vocabularies: undefined
    },
  },
};
