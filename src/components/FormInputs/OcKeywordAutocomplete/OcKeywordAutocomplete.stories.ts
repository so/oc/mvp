import type { Meta, StoryObj } from '@storybook/vue3';

import OcKeywordAutocomplete from './OcKeywordAutocomplete.vue';

const meta: Meta<typeof OcKeywordAutocomplete> = {
  component: OcKeywordAutocomplete,
};

export default meta;
type Story = StoryObj<typeof OcKeywordAutocomplete>;

export const Default: Story = {
  render: (args) => ({
    components: { OcKeywordAutocomplete },
    setup() {
      return { args };
    },
    template: '<OcKeywordAutocomplete v-bind="args" />',
  }),
  args: {},
};
