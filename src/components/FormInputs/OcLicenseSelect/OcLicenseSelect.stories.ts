import type { Meta, StoryObj } from '@storybook/vue3';

import OcLicenseSelect from './OcLicenseSelect.vue';

const meta: Meta<typeof OcLicenseSelect> = {
  component: OcLicenseSelect,
};

export default meta;
type Story = StoryObj<typeof OcLicenseSelect>;

export const Default: Story = {
  render: (args) => ({
    components: { OcLicenseSelect },
    setup() {
      return { args };
    },
    template: '<OcLicenseSelect v-bind="args" />',
  }),
};
