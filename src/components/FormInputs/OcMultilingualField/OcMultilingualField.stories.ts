import type { Meta, StoryObj } from '@storybook/vue3';

import OcMultilingualField from './OcMultilingualField.vue';
import InputText from 'primevue/inputtext';
import Textarea from 'primevue/textarea';
import { ref } from 'vue';

const meta: Meta<typeof OcMultilingualField> = {
  component: OcMultilingualField,
};

export default meta;
type Story = StoryObj<typeof OcMultilingualField>;

export const Default: Story = {
  render: (args) => ({
    components: { OcMultilingualField, InputText },
    setup() {
      const value = ref({
        fr: 'test 1',
        en: 'test 2',
      })
      return {
        args,
        value
      };
    },
    template: `
    <OcMultilingualField v-bind="args" :initialValue="value" @updateValue="value = $event"/>
    <ul class="mt-4">
      <li v-for="(content, locale) of value" :key="locale">
        {{ locale }} : {{ content }}
      </li>
    </ul>
    `,
  }),
  args: {
    id: 'test-default',
    inputComponent: InputText,
    inputProps: { fluid: true },
  },
};

export const WithoutInitialValue: Story = {
  render: (args) => ({
    components: { OcMultilingualField, InputText },
    setup() {
      return {
        args,
      };
    },
    template: `
    <OcMultilingualField v-bind="args"/>
    `,
  }),
  args: {
    id: 'test-without-initial-value',
    inputComponent: InputText,
    inputProps: { fluid: true },
  },
};


export const TextArea: Story = {
  render: (args) => ({
    components: { OcMultilingualField, InputText },
    setup() {
      return {
        args,
      };
    },
    template: `
    <OcMultilingualField v-bind="args"/>
    `,
  }),
  args: {
    initialValue: {
      fr: 'test 1',
      en: 'test 2',
    },
    id: 'test-textarea',
    inputComponent: Textarea,
    inputProps: { fluid: true },
  },
};

