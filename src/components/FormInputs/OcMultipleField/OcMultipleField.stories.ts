import type { Meta, StoryObj } from '@storybook/vue3';

import OcMultipleField from './OcMultipleField.vue';
import InputText from 'primevue/inputtext';
import DatePicker from 'primevue/datepicker';
import Textarea from 'primevue/textarea';
import { ref } from 'vue';

const meta: Meta<typeof OcMultipleField> = {
  component: OcMultipleField,
};

export default meta;
type Story = StoryObj<typeof OcMultipleField>;

export const Default: Story = {
  render: (args) => ({
    components: { OcMultipleField, InputText },
    setup() {
      const value = ref([
        'test 1',
        'test 2',
      ])
      return {
        args,
        value
      };
    },
    template: `
    <OcMultipleField v-bind="args" :initialValue="value" @updateValue="value = $event"/>
    <ul class="mt-4">
      <li v-for="(content, key) of value" :key="key">
        {{ key }} : {{ content }}
      </li>
    </ul>
    `,
  }),
  args: {
    id: 'test-default',
    inputComponent: InputText,
    inputProps: { fluid: true },
  },
};

export const DateRange: Story = {
  render: (args) => ({
    components: { OcMultipleField, DatePicker },
    setup() {
      return {
        args,
      };
    },
    template: `
    <OcMultipleField v-bind="args"/>
    `,
  }),
  args: {
    initialValue: [
      [new Date("2024-10-02T00:00:00.000"), new Date("2024-10-11T00:00:00.000")]
    ],
    id: 'test-daterange',
    inputComponent: DatePicker,
    inputProps: {
      selectionMode: "range",
      fluid: true,
      class: 'w-full'
    },
  },
};

export const TextArea: Story = {
  render: (args) => ({
    components: { OcMultipleField, DatePicker },
    setup() {
      return {
        args,
      };
    },
    template: `
    <OcMultipleField v-bind="args"/>
    `,
  }),
  args: {
    initialValue: [
      'some text about something 1',
      'some text about something 2',
      'some text about something 3',
    ],
    id: 'test-textarea',
    inputComponent: Textarea,
    inputProps: {
      fluid: true,
    },
  },
};

