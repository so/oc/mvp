import type { Meta, StoryObj } from '@storybook/vue3';

import OcRorOrganismSelect from './OcRorOrganismSelect.vue';

const meta: Meta<typeof OcRorOrganismSelect> = {
  component: OcRorOrganismSelect,
};

export default meta;
type Story = StoryObj<typeof OcRorOrganismSelect>;

export const Default: Story = {
  render: (args) => ({
    components: { OcRorOrganismSelect },
    setup() {
      return { args };
    },
    template: '<OcRorOrganismSelect v-bind="args" />',
  }),
  args: {},
};

export const WithInitialValue: Story = {
  render: (args) => ({
    components: { OcRorOrganismSelect },
    setup() {
      return { args };
    },
    template: '<OcRorOrganismSelect v-bind="args" />',
  }),
  args: {
    modelValue: "https://ror.org/01rx4qw44",
  }
};