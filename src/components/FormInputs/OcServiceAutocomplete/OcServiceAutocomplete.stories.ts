import type { Meta, StoryObj } from '@storybook/vue3';

import OcServiceAutocomplete from './OcServiceAutocomplete.vue';

const meta: Meta<typeof OcServiceAutocomplete> = {
  component: OcServiceAutocomplete,
};

export default meta;
type Story = StoryObj<typeof OcServiceAutocomplete>;

export const OrganizationAndPerson: Story = {
  render: (args) => ({
    components: { OcServiceAutocomplete },
    setup() {
      return { args };
    },
    template: '<OcServiceAutocomplete v-bind="args" />',
  }),
};
