import type { Meta, StoryObj } from '@storybook/vue3';

import OcAgentTag from './OcAgentTag.vue';

const meta: Meta<typeof OcAgentTag> = {
  component: OcAgentTag,
};

export default meta;
type Story = StoryObj<typeof OcAgentTag>;

export const Person: Story = {
  render: (args) => ({
    components: { OcAgentTag },
    setup() {
      return { args };
    },
    template: '<OcAgentTag v-bind="args" />',
  }),
  args: {
    agent: {
      '@id': '1234',
      '@type': ['http://xmlns.com/foaf/0.1/Person'],
      firstName: 'Jean',
      familyName: 'Durand'
    },
  },
};

export const Organization: Story = {
  render: (args) => ({
    components: { OcAgentTag },
    setup() {
      return { args };
    },
    template: '<OcAgentTag v-bind="args" />',
  }),
  args: {
    agent: {
      '@id': '1234',
      '@type': ['http://xmlns.com/foaf/0.1/Organization'],
      name: 'Université de Toulouse',
    },
  },
};
