import type { Meta, StoryObj } from '@storybook/vue3';

import OcBreadcrumb from './OcBreadcrumb.vue';

const meta: Meta<typeof OcBreadcrumb> = {
  component: OcBreadcrumb,
};

export default meta;
type Story = StoryObj<typeof OcBreadcrumb>;

export const Primary: Story = {
  render: (args) => ({
    components: { OcBreadcrumb },
    setup() {
      return { args };
    },
    template: '<OcBreadcrumb v-bind="args" />',
  }),
  args: {
    items: [
      {
        label: "LabEx DRIIHM",
        key: "driihm",
        type: "community",
      },
      {
        label: "Les ressources de la communauté",
        key: "driihm-resources",
        type: "http://www.w3.org/ns/dcat#Catalog",
      },
      {
        label: "Les Données de la communauté",
        key: "driihm-data",
        type: "http://www.w3.org/ns/dcat#Catalog",
      }
    ]
  }
};

export const TruncatedPath: Story = {
  render: (args) => ({
    components: { OcBreadcrumb },
    setup() {
      return { args };
    },
    template: '<OcBreadcrumb v-bind="args" />',
  }),
  args: {
    items: [
      {
        label: "LabEx DRIIHM",
        key: "driihm",
        type: "community",
      },
      {
        label: "Les ressources de la communauté",
        key: "driihm-resources",
        type: "http://www.w3.org/ns/dcat#Catalog",
      },
      {
        label: "Les Données de la communauté",
        key: "driihm-data",
        type: "http://www.w3.org/ns/dcat#Catalog",
      },
      {
        label: "LabEx DRIIHM",
        key: "labex-driihm",
        type: "http://www.w3.org/ns/dcat#Catalog",
      },
      {
        label: "KAKILA, Base de données d'observation des mammifères marins autour de l'archipel de la Guadeloupe dans le sanctuaire AGOA - Antilles françaises",
        key: "kakila",
        type: "http://www.w3.org/ns/dcat#Dataset",
      },
      {
        label: "Base de données d'observation des mammifères marins autour de l'archipel de la Guadeloupe dans le sanctuaire AGOA - Antilles françaises",
        key: "kakila-db",
        type: "http://www.w3.org/ns/dcat#Distribution",
      }
    ]
  }
};