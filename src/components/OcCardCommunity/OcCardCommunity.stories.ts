import type { Meta, StoryObj } from '@storybook/vue3';

import OcCardCommunity from './OcCardCommunity.vue';
import { communityExample1 } from '@/assets/communityExample1';

const meta: Meta<typeof OcCardCommunity> = {
  component: OcCardCommunity,
};

export default meta;
type Story = StoryObj<typeof OcCardCommunity>;

export const Primary: Story = {
  render: (args) => ({
    components: { OcCardCommunity },
    setup() {
      return { args };
    },
    template: '<OcCardCommunity v-bind="args" />',
  }),
  args: {
    community: communityExample1
  },
};