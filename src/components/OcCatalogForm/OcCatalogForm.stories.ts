import type { Meta, StoryObj } from '@storybook/vue3';

import OcCatalogForm from './OcCatalogForm.vue';
import { communityExample1 } from '@/assets/communityExample1';

const meta: Meta<typeof OcCatalogForm> = {
  component: OcCatalogForm,
};

export default meta;
type Story = StoryObj<typeof OcCatalogForm>;

export const Default: Story = {
  render: (args) => ({
    components: { OcCatalogForm },
    setup() {
      return { args };
    },
    template: '<OcCatalogForm v-bind="args" />',
  }),
  args: {
    modelValue: {},
    community: communityExample1,
    userPrivateGraph: 'private-graph',
    auth: {
      email: import.meta.env.VITE_VISITOR_USER,
      password: import.meta.env.VITE_VISITOR_PWD,
    }
  },
};

export const CustomLabels: Story = {
  render: (args) => ({
    components: { OcCatalogForm },
    setup() {
      return { args };
    },
    template: '<OcCatalogForm v-bind="args" />',
  }),
  args: {
    modelValue: {},
    community: communityExample1,
    userPrivateGraph: 'private-graph',
    auth: {
      email: import.meta.env.VITE_VISITOR_USER,
      password: import.meta.env.VITE_VISITOR_PWD,
    },
    saveLabel: 'Next',
    cancelLabel: 'Exit',
  },
};

export const WithoutCancel: Story = {
  render: (args) => ({
    components: { OcCatalogForm },
    setup() {
      return { args };
    },
    template: '<OcCatalogForm v-bind="args" />',
  }),
  args: {
    modelValue: {},
    community: communityExample1,
    userPrivateGraph: 'private-graph',
    auth: {
      email: import.meta.env.VITE_VISITOR_USER,
      password: import.meta.env.VITE_VISITOR_PWD,
    },
    cancelButton: false,
  },
};
