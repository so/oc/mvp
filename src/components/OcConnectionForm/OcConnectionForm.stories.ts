import type { Meta, StoryObj } from '@storybook/vue3';

import OcConnectionForm from './OcConnectionForm.vue';

const meta: Meta<typeof OcConnectionForm> = {
  component: OcConnectionForm,
};

export default meta;
type Story = StoryObj<typeof OcConnectionForm>;

export const Primary: Story = {
  render: (args) => ({
    components: { OcConnectionForm },
    setup() {
      return { args };
    },
    template: '<OcConnectionForm v-bind="args" />',
  }),
  args: {},
};