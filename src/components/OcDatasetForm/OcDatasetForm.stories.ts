import type { Meta, StoryObj } from '@storybook/vue3';

import OcDatasetForm from './OcDatasetForm.vue';

const meta: Meta<typeof OcDatasetForm> = {
  component: OcDatasetForm,
};

export default meta;
type Story = StoryObj<typeof OcDatasetForm>;

export const Default: Story = {
  render: (args) => ({
    components: { OcDatasetForm },
    setup() {
      return { args };
    },
    template: '<OcDatasetForm v-bind="args" />',
  }),
  args: {
    modelValue: {},
  },
};

export const CustomLabels: Story = {
  render: (args) => ({
    components: { OcDatasetForm },
    setup() {
      return { args };
    },
    template: '<OcDatasetForm v-bind="args" />',
  }),
  args: {
    modelValue: {},
    saveLabel: 'Next',
    cancelLabel: 'Exit',
  },
};

export const WithoutCancel: Story = {
  render: (args) => ({
    components: { OcDatasetForm },
    setup() {
      return { args };
    },
    template: '<OcDatasetForm v-bind="args" />',
  }),
  args: {
    modelValue: {},
    cancelButton: false,
  },
};
