import type { Meta, StoryObj } from '@storybook/vue3';

import OcDistributionForm from './OcDistributionForm.vue';

const meta: Meta<typeof OcDistributionForm> = {
  component: OcDistributionForm,
};

export default meta;
type Story = StoryObj<typeof OcDistributionForm>;

export const Default: Story = {
  render: (args) => ({
    components: { OcDistributionForm },
    setup() {
      return { args };
    },
    template: '<OcDistributionForm v-bind="args" />',
  }),
  args: {
    modelValue: {}
  },
};
