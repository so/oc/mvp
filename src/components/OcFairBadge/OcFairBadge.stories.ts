import type { Meta, StoryObj } from '@storybook/vue3';

import OcFairBadge from './OcFairBadge.vue';

const meta: Meta<typeof OcFairBadge> = {
  component: OcFairBadge,
};

export default meta;
type Story = StoryObj<typeof OcFairBadge>;

export const FAIR: Story = {
  render: (args) => ({
    components: { OcFairBadge },
    setup() {
      return { args };
    },
    template: '<OcFairBadge v-bind="args" />',
  }),
  args: {
    badges: ["f", "a", "i", "r"],
  },
};

export const FAI: Story = {
  render: (args) => ({
    components: { OcFairBadge },
    setup() {
      return { args };
    },
    template: '<OcFairBadge v-bind="args" />',
  }),
  args: {
    badges: ["f", "a", "i"],
  },
};

export const AI: Story = {
  render: (args) => ({
    components: { OcFairBadge },
    setup() {
      return { args };
    },
    template: '<OcFairBadge v-bind="args" />',
  }),
  args: {
    badges: ["a", "i"],
  },
};

export const WithoutLetter: Story = {
  render: (args) => ({
    components: { OcFairBadge },
    setup() {
      return { args };
    },
    template: '<OcFairBadge v-bind="args" />',
  }),
  args: {
    badges: ["f", "a", "i"],
    letter: false
  },
};

export const WithPopover: Story = {
  render: (args) => ({
    components: { OcFairBadge },
    setup() {
      return { args };
    },
    template: '<OcFairBadge v-bind="args" />',
  }),
  args: {
    badges: ["f", "a", "i"],
    letter: false,
    popover: true
  },
};

export const WithoutArgs: Story = {
  render: (args) => ({
    components: { OcFairBadge },
    setup() {
      return { args };
    },
    template: '<OcFairBadge v-bind="args" />',
  }),
  args: {

  },
};