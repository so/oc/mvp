import type { Meta, StoryObj } from '@storybook/vue3';

import OcHomeCommunityActionList from './OcHomeCommunityActionList.vue';

const meta: Meta<typeof OcHomeCommunityActionList> = {
  component: OcHomeCommunityActionList,
};

export default meta;
type Story = StoryObj<typeof OcHomeCommunityActionList>;

export const Primary: Story = {
  render: (args) => ({
    components: { OcHomeCommunityActionList },
    setup() {
      return { args };
    },
    template: '<OcHomeCommunityActionList v-bind="args" />',
  }),
  args: {},
};