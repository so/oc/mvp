import type { HomeCommunityAction } from "@/declarations";

export const homeCommunityActionList: HomeCommunityAction[] = [
  {
    "id": "access",
    "title": {
      "en": "Access my community's resources",
      "fr": "Accéder aux ressources de ma communauté"
    },
    "description": {
      "en": "By signing in, you can access not only public resources (ie. catalogues, data, publications, codes) of all communities, but also resources accessible through this research community. You can browse the resources tree on the left, launch a new search or view your previous searches.",
      "fr": "En vous connectant, vous pouvez accéder non seulement aux ressources publiques (catalogues, données, publications, codes) de toutes les communautés, mais aussi aux ressources accessibles par l'intermédiaire de cette communauté de recherche. Vous pouvez parcourir l'arbre des ressources sur la gauche, lancer une nouvelle recherche ou consulter vos recherches précédentes."
    },
    "needAccount": false
  },
  {
    "id": "reference",
    "title": {
      "en": "Reference/catalogue",
      "fr": "Référencer/cataloguer"
    },
    "description": {
      "en": "This interface guides you through the process of cataloguing your resources (data, codes), according to the FAIR principles. This will increase the visibility of your resources in Open Common and beyond. This feature is only available if you are logged in.",
      "fr": "Cette interface vous guide dans le processus de catalogage de vos ressources (données, codes), selon les principes FAIR. Cela augmentera la visibilité de vos ressources dans Open Common et au-delà. Cette fonctionnalité n'est disponible que si vous êtes connecté."
    },
    "fair": ["f", "a", "i", "r"],
    "needAccount": true
  },
  {
    "id": "publish",
    "title": {
      "en": "Publish my resources",
      "fr": "Déposer mes ressources"
    },
    "description": {
      "en": "This interface guides you through the process of depositing your resources (data, codes) in a recommended data repository, according to the FAIR principles. Let us guide you! Note that a DOI will be assigned to your data or codes. This feature is only available if you are logged in.",
      "fr": "Cette interface vous guide dans le processus de dépôt de vos ressources (données, codes) dans un entrepôt de données recommandé, selon les principes FAIR. Laissez-vous guider ! Notez qu'un DOI sera attribué à vos données ou codes. Cette fonctionnalité n'est disponible que si vous êtes connecté."
    },
    "fair": ["f", "a", "i", "r"],
    "needAccount": true
  },
  {
    "id": "manage",
    "title": {
      "en": "Manage my resources",
      "fr": "Gérer mes ressources"
    },
    "description": {
      "en": "Access to your dashboard and see the results of your previous searches and your previous data deposits. Add, modify or continue your research or data cataloguing/publication processes. This feature is only available if you are logged in.",
      "fr": "Accédez à votre tableau de bord et consultez les résultats de vos recherches et de vos dépôts de données antérieurs. Vous avez la ossibilité d'ajouter, de modifier ou de poursuivre vos recherches ou compléter vos processus de catalogage/publication de données. Cette fonctionnalité n'est disponible que si vous êtes connecté."
    },
    "needAccount": true
  },
  {
    "id": "vocabularies",
    "title": {
      "en": "Access & Share thematic vocabularies",
      "fr": "Découvrez & Partagez des vocabulaires thématiques"
    },
    "description": {
      "en": "Discover the thematic vocabularies used or recommended for your disciplinary and thematic community. Share your own thematic vocabularies with commmunity members. This feature is only available if you are logged in.",
      "fr": "Découvrez les vocabulaires thématiques utilisés ou recommandés pour votre communauté disciplinaire et thématique. Partagez vos propres vocabulaires thématiques avec les membres de la communauté. Cette fonctionnalité n'est disponible que si vous êtes connecté."
    },
    "fair": ["f", "a", "i", "r"],
    "needAccount": true
  }
]
