import type { Meta, StoryObj } from '@storybook/vue3';

import OcRegisterForm from './OcRegisterForm.vue';

const meta: Meta<typeof OcRegisterForm> = {
  component: OcRegisterForm,
};

export default meta;
type Story = StoryObj<typeof OcRegisterForm>;

export const Primary: Story = {
  render: (args) => ({
    components: { OcRegisterForm },
    setup() {
      return { args };
    },
    template: '<OcRegisterForm v-bind="args" />',
  }),
  args: {},
};