import type { Meta, StoryObj } from '@storybook/vue3';

import OcTopBar from './OcTopBar.vue';

const meta: Meta<typeof OcTopBar> = {
  component: OcTopBar,
};

export default meta;
type Story = StoryObj<typeof OcTopBar>;

export const Default: Story = {
  render: (args) => ({
    components: { OcTopBar },
    setup() {
      return { args };
    },
    template: '<OcTopBar v-bind="args" />',
  }),
  args: {},
};

export const Authenticated: Story = {
  render: (args) => ({
    components: { OcTopBar },
    setup() {
      return { args };
    },
    template: '<OcTopBar v-bind="args" />',
  }),
  args: {
    isAuthenticated: true
  },
};

export const WithBreadcrumb: Story = {
  render: (args) => ({
    components: { OcTopBar },
    setup() {
      return { args };
    },
    template: '<OcTopBar v-bind="args" />',
  }),
  args: {
    isAuthenticated: false,
    breadcrumbItems: [
      {
        label: "LabEx DRIIHM",
        key: "driihm",
        type: "community",
      },
      {
        label: "Les ressources de la communauté",
        key: "driihm-resources",
        type: "http://www.w3.org/ns/dcat#Catalog",
      },
      {
        label: "Les Données de la communauté",
        key: "driihm-data",
        type: "http://www.w3.org/ns/dcat#Catalog",
      }]
  },
};