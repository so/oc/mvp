import type { Meta, StoryObj } from '@storybook/vue3'
import { fn } from '@storybook/test'

import OcTreeCommunity from './OcTreeCommunity.vue'

const meta: Meta<typeof OcTreeCommunity> = {
  component: OcTreeCommunity
}

export default meta
type Story = StoryObj<typeof OcTreeCommunity>

export const NotAuthenticated: Story = {
  render: (args) => ({
    components: { OcTreeCommunity },
    setup() {
      return { args }
    },
    template: '<OcTreeCommunity v-bind="args" />'
  }),
  args: {
    onNodeExpand: fn(),
    onNodeSelect: fn(),
    onNodeCollapse: fn(),
    onNodeUnselect: fn(),
    community: {
      '@id': 'id',
      description: {
        fr: "Lauréat de la deuxième vague de l'appel à projet Laboratoire d'Excellence (LabEx) dans le cadre du programme « Investissements d'avenir », le LabEx DRIIHM, Dispositif de Recherche Interdisciplinaire sur les Interactions Hommes-Milieux, regroupe à ce jour 13 Observatoires Hommes-Milieux, outils d'observation de socio-écosystèmes impactés par un événement d'origine anthropique. Créés par le CNRS-INEE en 2007, ils sont répartis en France métropolitaine, en outre-mer et à l’étranger.",
        en: 'Laureate of the Laboratory for Excellence project (LabEx) in the program « Investment in the future », the DRIIHM LabEx, Device for Interdisciplinary Research on human-environments Interactions, aggregate 13 human-environments observatories (OHM in french), tools for observing socio-ecosystems impacted by anthropic events. Created by CNRS-INEE in 2007, they are located in metropolitan France, overseas France and abroad.'
      },
      identifier: '189088ec-baa9-4397-8c6f-eefde9a3790c',
      title: {
        fr: 'Communauté du LabEx DRIIHM',
        en: 'DRIIHM Community'
      },
      logo: 'https://www.driihm.fr/images/images/logos_png/logo_DRIIHM_r%C3%A9duit.png',
      name: 'driihm',
      isSpaceOf:
        'https://www.irit.fr/opencommon/agents/organization/9a20f121-c64e-4049-93a7-4bedbe819fd6',
      color: 'olivine'
    },
    treeNodes: [
      {
        '@id': 'https://www.irit.fr/opencommon/resourceTree/Home',
        '@type': [
          'http://www.w3.org/ns/dcat#Catalog',
          'http://www.w3.org/2002/07/owl#NamedIndividual'
        ],
        identifier: {
          '@type': 'http://www.w3.org/2000/01/rdf-schema#Literal',
          '@value': '1cc0d1cd-e00a-4dfe-aa3d-49c7915b15d1'
        },
        title: { en: 'Home', fr: 'Accueil' },
        catalog: [
          {
            '@id': 'https://www.irit.fr/opencommon/resourceTree/PublicResources',
            '@type': [
              'http://www.w3.org/ns/dcat#Catalog',
              'http://www.w3.org/2002/07/owl#NamedIndividual'
            ],
            identifier: {
              '@type': 'http://www.w3.org/2000/01/rdf-schema#Literal',
              '@value': 'bd033e43-7c34-4641-a922-d04f77c3e0a6'
            },
            title: { en: 'Public resources', fr: 'Les ressources publiques' },
            parentGraphs: [
              'https://www.irit.fr/opencommon/communities/189088ec-baa9-4397-8c6f-eefde9a3790c/publicResources'
            ]
          }
        ],
        parentGraphs: [
          'https://www.irit.fr/opencommon/communities/189088ec-baa9-4397-8c6f-eefde9a3790c/publicResources'
        ]
      }
    ],
    isAuthenticated: false
  }
}

export const Authenticated: Story = {
  render: (args) => ({
    components: { OcTreeCommunity },
    setup() {
      return { args }
    },
    template: '<OcTreeCommunity v-bind="args" />'
  }),
  args: {
    onNodeExpand: fn(),
    onNodeSelect: fn(),
    onNodeCollapse: fn(),
    onNodeUnselect: fn(),
    community: {
      '@id': 'id',
      description: {
        fr: "Lauréat de la deuxième vague de l'appel à projet Laboratoire d'Excellence (LabEx) dans le cadre du programme « Investissements d'avenir », le LabEx DRIIHM, Dispositif de Recherche Interdisciplinaire sur les Interactions Hommes-Milieux, regroupe à ce jour 13 Observatoires Hommes-Milieux, outils d'observation de socio-écosystèmes impactés par un événement d'origine anthropique. Créés par le CNRS-INEE en 2007, ils sont répartis en France métropolitaine, en outre-mer et à l’étranger.",
        en: 'Laureate of the Laboratory for Excellence project (LabEx) in the program « Investment in the future », the DRIIHM LabEx, Device for Interdisciplinary Research on human-environments Interactions, aggregate 13 human-environments observatories (OHM in french), tools for observing socio-ecosystems impacted by anthropic events. Created by CNRS-INEE in 2007, they are located in metropolitan France, overseas France and abroad.'
      },
      identifier: '189088ec-baa9-4397-8c6f-eefde9a3790c',
      title: {
        fr: 'Communauté du LabEx DRIIHM',
        en: 'DRIIHM Community'
      },
      logo: 'https://www.driihm.fr/images/images/logos_png/logo_DRIIHM_r%C3%A9duit.png',
      name: 'driihm',
      isSpaceOf:
        'https://www.irit.fr/opencommon/agents/organization/9a20f121-c64e-4049-93a7-4bedbe819fd6',
      color: 'olivine'
    },
    treeNodes: [
      {
        '@id': 'https://www.irit.fr/opencommon/resourceTree/Home',
        '@type': [
          'http://www.w3.org/ns/dcat#Catalog',
          'http://www.w3.org/2002/07/owl#NamedIndividual'
        ],
        identifier: {
          '@type': 'http://www.w3.org/2000/01/rdf-schema#Literal',
          '@value': '1cc0d1cd-e00a-4dfe-aa3d-49c7915b15d1'
        },
        title: { en: 'Home', fr: 'Accueil' },
        parentGraphs: [
          'https://www.irit.fr/opencommon/communities/189088ec-baa9-4397-8c6f-eefde9a3790c/publicResources'
        ],
        catalog: [
          {
            '@id': 'https://www.irit.fr/opencommon/resourceTree/MyResources',
            '@type': [
              'http://www.w3.org/ns/dcat#Catalog',
              'http://www.w3.org/2002/07/owl#NamedIndividual'
            ],
            identifier: {
              '@type': 'http://www.w3.org/2000/01/rdf-schema#Literal',
              '@value': '65ae9254-cf19-41f3-9d1e-cf6ff60b4889'
            },
            title: { en: 'My resources', fr: 'Mes ressources' },
            catalog: [
              'https://www.irit.fr/opencommon/resourceTree/MyCode',
              'https://www.irit.fr/opencommon/resourceTree/MyDMP',
              'https://www.irit.fr/opencommon/resourceTree/MyData',
              'https://www.irit.fr/opencommon/resourceTree/MyDatapapers',
              'https://www.irit.fr/opencommon/resourceTree/MyProjects',
              'https://www.irit.fr/opencommon/resourceTree/MyPublications',
              'https://www.irit.fr/opencommon/resourceTree/MyVocabularies',
              'https://www.irit.fr/opencommon/resourceTree/MyServices'
            ],
            parentGraphs: ['https://www.irit.fr/opencommon/member/123/private']
          },
          {
            '@id': 'https://www.irit.fr/opencommon/resourceTree/CommunityResources',
            '@type': [
              'http://www.w3.org/ns/dcat#Catalog',
              'http://www.w3.org/2002/07/owl#NamedIndividual'
            ],
            identifier: {
              '@type': 'http://www.w3.org/2000/01/rdf-schema#Literal',
              '@value': '3dc83dc4-ef4a-42a8-9dd1-016f0943f72f'
            },
            title: { en: 'Community resources', fr: 'Les ressources de la communauté' },
            catalog: [
              'https://www.irit.fr/opencommon/resourceTree/CommunityActions',
              'https://www.irit.fr/opencommon/resourceTree/CommunityCode',
              'https://www.irit.fr/opencommon/resourceTree/CommunityDMP',
              'https://www.irit.fr/opencommon/resourceTree/CommunityData',
              'https://www.irit.fr/opencommon/resourceTree/CommunityDatapapers',
              'https://www.irit.fr/opencommon/resourceTree/CommunityPublications',
              'https://www.irit.fr/opencommon/resourceTree/CommunityServices',
              'https://www.irit.fr/opencommon/resourceTree/CommunityVocabularies',
              'https://data.driihm.fr/catalogs/59c0f544-a008-48e0-b1dc-ded597025ca1'
            ],
            parentGraphs: [
              'https://www.irit.fr/opencommon/communities/189088ec-baa9-4397-8c6f-eefde9a3790c/communityResources'
            ]
          },
          {
            '@id': 'https://www.irit.fr/opencommon/resourceTree/PublicResources',
            '@type': [
              'http://www.w3.org/ns/dcat#Catalog',
              'http://www.w3.org/2002/07/owl#NamedIndividual'
            ],
            identifier: {
              '@type': 'http://www.w3.org/2000/01/rdf-schema#Literal',
              '@value': 'bd033e43-7c34-4641-a922-d04f77c3e0a6'
            },
            title: { en: 'Public resources', fr: 'Les ressources publiques' },
            catalog: [
              'https://www.irit.fr/opencommon/resourceTree/PublicActions',
              'https://www.irit.fr/opencommon/resourceTree/PublicCode',
              'https://www.irit.fr/opencommon/resourceTree/PublicDMP',
              'https://www.irit.fr/opencommon/resourceTree/PublicData',
              'https://www.irit.fr/opencommon/resourceTree/PublicDatapapers',
              'https://www.irit.fr/opencommon/resourceTree/PublicPublications',
              'https://www.irit.fr/opencommon/resourceTree/PublicServices',
              'https://www.irit.fr/opencommon/resourceTree/PublicVocabularies'
            ],
            parentGraphs: [
              'https://www.irit.fr/opencommon/communities/189088ec-baa9-4397-8c6f-eefde9a3790c/publicResources'
            ]
          }
        ]
      }
    ],
    isAuthenticated: true,
    userPrivateGraph: 'https://www.irit.fr/opencommon/member/123/private'
  }
}

export const ExpandedHome: Story = {
  render: (args) => ({
    components: { OcTreeCommunity },
    setup() {
      return { args }
    },
    template: '<OcTreeCommunity v-bind="args" />'
  }),
  args: {
    onNodeExpand: fn(),
    onNodeSelect: fn(),
    onNodeCollapse: fn(),
    onNodeUnselect: fn(),
    community: {
      '@id': 'id',
      description: {
        fr: "Lauréat de la deuxième vague de l'appel à projet Laboratoire d'Excellence (LabEx) dans le cadre du programme « Investissements d'avenir », le LabEx DRIIHM, Dispositif de Recherche Interdisciplinaire sur les Interactions Hommes-Milieux, regroupe à ce jour 13 Observatoires Hommes-Milieux, outils d'observation de socio-écosystèmes impactés par un événement d'origine anthropique. Créés par le CNRS-INEE en 2007, ils sont répartis en France métropolitaine, en outre-mer et à l’étranger.",
        en: 'Laureate of the Laboratory for Excellence project (LabEx) in the program « Investment in the future », the DRIIHM LabEx, Device for Interdisciplinary Research on human-environments Interactions, aggregate 13 human-environments observatories (OHM in french), tools for observing socio-ecosystems impacted by anthropic events. Created by CNRS-INEE in 2007, they are located in metropolitan France, overseas France and abroad.'
      },
      identifier: '189088ec-baa9-4397-8c6f-eefde9a3790c',
      title: {
        fr: 'Communauté du LabEx DRIIHM',
        en: 'DRIIHM Community'
      },
      logo: 'https://www.driihm.fr/images/images/logos_png/logo_DRIIHM_r%C3%A9duit.png',
      name: 'driihm',
      isSpaceOf:
        'https://www.irit.fr/opencommon/agents/organization/9a20f121-c64e-4049-93a7-4bedbe819fd6',
      color: 'olivine'
    },
    treeNodes: [
      {
        '@id': 'https://www.irit.fr/opencommon/resourceTree/Home',
        '@type': [
          'http://www.w3.org/ns/dcat#Catalog',
          'http://www.w3.org/2002/07/owl#NamedIndividual'
        ],
        identifier: {
          '@type': 'http://www.w3.org/2000/01/rdf-schema#Literal',
          '@value': '1cc0d1cd-e00a-4dfe-aa3d-49c7915b15d1'
        },
        title: { en: 'Home', fr: 'Accueil' },
        parentGraphs: [
          'https://www.irit.fr/opencommon/communities/189088ec-baa9-4397-8c6f-eefde9a3790c/publicResources'
        ],
        catalog: [
          {
            '@id': 'https://www.irit.fr/opencommon/resourceTree/MyResources',
            '@type': [
              'http://www.w3.org/ns/dcat#Catalog',
              'http://www.w3.org/2002/07/owl#NamedIndividual'
            ],
            identifier: {
              '@type': 'http://www.w3.org/2000/01/rdf-schema#Literal',
              '@value': '65ae9254-cf19-41f3-9d1e-cf6ff60b4889'
            },
            title: { en: 'My resources', fr: 'Mes ressources' },
            catalog: [
              'https://www.irit.fr/opencommon/resourceTree/MyCode',
              'https://www.irit.fr/opencommon/resourceTree/MyDMP',
              'https://www.irit.fr/opencommon/resourceTree/MyData',
              'https://www.irit.fr/opencommon/resourceTree/MyDatapapers',
              'https://www.irit.fr/opencommon/resourceTree/MyProjects',
              'https://www.irit.fr/opencommon/resourceTree/MyPublications',
              'https://www.irit.fr/opencommon/resourceTree/MyVocabularies',
              'https://www.irit.fr/opencommon/resourceTree/MyServices'
            ],
            parentGraphs: ['https://www.irit.fr/opencommon/member/123/private']
          },
          {
            '@id': 'https://www.irit.fr/opencommon/resourceTree/CommunityResources',
            '@type': [
              'http://www.w3.org/ns/dcat#Catalog',
              'http://www.w3.org/2002/07/owl#NamedIndividual'
            ],
            identifier: {
              '@type': 'http://www.w3.org/2000/01/rdf-schema#Literal',
              '@value': '3dc83dc4-ef4a-42a8-9dd1-016f0943f72f'
            },
            title: { en: 'Community resources', fr: 'Les ressources de la communauté' },
            catalog: [
              'https://www.irit.fr/opencommon/resourceTree/CommunityActions',
              'https://www.irit.fr/opencommon/resourceTree/CommunityCode',
              'https://www.irit.fr/opencommon/resourceTree/CommunityDMP',
              'https://www.irit.fr/opencommon/resourceTree/CommunityData',
              'https://www.irit.fr/opencommon/resourceTree/CommunityDatapapers',
              'https://www.irit.fr/opencommon/resourceTree/CommunityPublications',
              'https://www.irit.fr/opencommon/resourceTree/CommunityServices',
              'https://www.irit.fr/opencommon/resourceTree/CommunityVocabularies',
              'https://data.driihm.fr/catalogs/59c0f544-a008-48e0-b1dc-ded597025ca1'
            ],
            parentGraphs: [
              'https://www.irit.fr/opencommon/communities/189088ec-baa9-4397-8c6f-eefde9a3790c/communityResources'
            ]
          },
          {
            '@id': 'https://www.irit.fr/opencommon/resourceTree/PublicResources',
            '@type': [
              'http://www.w3.org/ns/dcat#Catalog',
              'http://www.w3.org/2002/07/owl#NamedIndividual'
            ],
            identifier: {
              '@type': 'http://www.w3.org/2000/01/rdf-schema#Literal',
              '@value': 'bd033e43-7c34-4641-a922-d04f77c3e0a6'
            },
            title: { en: 'Public resources', fr: 'Les ressources publiques' },
            catalog: [
              'https://www.irit.fr/opencommon/resourceTree/PublicActions',
              'https://www.irit.fr/opencommon/resourceTree/PublicCode',
              'https://www.irit.fr/opencommon/resourceTree/PublicDMP',
              'https://www.irit.fr/opencommon/resourceTree/PublicData',
              'https://www.irit.fr/opencommon/resourceTree/PublicDatapapers',
              'https://www.irit.fr/opencommon/resourceTree/PublicPublications',
              'https://www.irit.fr/opencommon/resourceTree/PublicServices',
              'https://www.irit.fr/opencommon/resourceTree/PublicVocabularies'
            ],
            parentGraphs: [
              'https://www.irit.fr/opencommon/communities/189088ec-baa9-4397-8c6f-eefde9a3790c/publicResources'
            ]
          }
        ]
      }
    ],
    isAuthenticated: true,
    userPrivateGraph: 'https://www.irit.fr/opencommon/member/123/private',
    expandedKeys: ['1cc0d1cd-e00a-4dfe-aa3d-49c7915b15d1']
  }
}

export const SelectedMyResources: Story = {
  render: (args) => ({
    components: { OcTreeCommunity },
    setup() {
      return { args }
    },
    template: '<OcTreeCommunity v-bind="args" />'
  }),
  args: {
    onNodeExpand: fn(),
    onNodeSelect: fn(),
    onNodeCollapse: fn(),
    onNodeUnselect: fn(),
    community: {
      '@id': 'id',
      description: {
        fr: "Lauréat de la deuxième vague de l'appel à projet Laboratoire d'Excellence (LabEx) dans le cadre du programme « Investissements d'avenir », le LabEx DRIIHM, Dispositif de Recherche Interdisciplinaire sur les Interactions Hommes-Milieux, regroupe à ce jour 13 Observatoires Hommes-Milieux, outils d'observation de socio-écosystèmes impactés par un événement d'origine anthropique. Créés par le CNRS-INEE en 2007, ils sont répartis en France métropolitaine, en outre-mer et à l’étranger.",
        en: 'Laureate of the Laboratory for Excellence project (LabEx) in the program « Investment in the future », the DRIIHM LabEx, Device for Interdisciplinary Research on human-environments Interactions, aggregate 13 human-environments observatories (OHM in french), tools for observing socio-ecosystems impacted by anthropic events. Created by CNRS-INEE in 2007, they are located in metropolitan France, overseas France and abroad.'
      },
      identifier: '189088ec-baa9-4397-8c6f-eefde9a3790c',
      title: {
        fr: 'Communauté du LabEx DRIIHM',
        en: 'DRIIHM Community'
      },
      logo: 'https://www.driihm.fr/images/images/logos_png/logo_DRIIHM_r%C3%A9duit.png',
      name: 'driihm',
      isSpaceOf:
        'https://www.irit.fr/opencommon/agents/organization/9a20f121-c64e-4049-93a7-4bedbe819fd6',
      color: 'olivine'
    },
    treeNodes: [
      {
        '@id': 'https://www.irit.fr/opencommon/resourceTree/Home',
        '@type': [
          'http://www.w3.org/ns/dcat#Catalog',
          'http://www.w3.org/2002/07/owl#NamedIndividual'
        ],
        identifier: {
          '@type': 'http://www.w3.org/2000/01/rdf-schema#Literal',
          '@value': '1cc0d1cd-e00a-4dfe-aa3d-49c7915b15d1'
        },
        title: { en: 'Home', fr: 'Accueil' },
        parentGraphs: [
          'https://www.irit.fr/opencommon/communities/189088ec-baa9-4397-8c6f-eefde9a3790c/publicResources'
        ],
        catalog: [
          {
            '@id': 'https://www.irit.fr/opencommon/resourceTree/MyResources',
            '@type': [
              'http://www.w3.org/ns/dcat#Catalog',
              'http://www.w3.org/2002/07/owl#NamedIndividual'
            ],
            identifier: {
              '@type': 'http://www.w3.org/2000/01/rdf-schema#Literal',
              '@value': '65ae9254-cf19-41f3-9d1e-cf6ff60b4889'
            },
            title: { en: 'My resources', fr: 'Mes ressources' },
            catalog: [
              'https://www.irit.fr/opencommon/resourceTree/MyCode',
              'https://www.irit.fr/opencommon/resourceTree/MyDMP',
              'https://www.irit.fr/opencommon/resourceTree/MyData',
              'https://www.irit.fr/opencommon/resourceTree/MyDatapapers',
              'https://www.irit.fr/opencommon/resourceTree/MyProjects',
              'https://www.irit.fr/opencommon/resourceTree/MyPublications',
              'https://www.irit.fr/opencommon/resourceTree/MyVocabularies',
              'https://www.irit.fr/opencommon/resourceTree/MyServices'
            ],
            parentGraphs: ['https://www.irit.fr/opencommon/member/123/private']
          },
          {
            '@id': 'https://www.irit.fr/opencommon/resourceTree/CommunityResources',
            '@type': [
              'http://www.w3.org/ns/dcat#Catalog',
              'http://www.w3.org/2002/07/owl#NamedIndividual'
            ],
            identifier: {
              '@type': 'http://www.w3.org/2000/01/rdf-schema#Literal',
              '@value': '3dc83dc4-ef4a-42a8-9dd1-016f0943f72f'
            },
            title: { en: 'Community resources', fr: 'Les ressources de la communauté' },
            catalog: [
              'https://www.irit.fr/opencommon/resourceTree/CommunityActions',
              'https://www.irit.fr/opencommon/resourceTree/CommunityCode',
              'https://www.irit.fr/opencommon/resourceTree/CommunityDMP',
              'https://www.irit.fr/opencommon/resourceTree/CommunityData',
              'https://www.irit.fr/opencommon/resourceTree/CommunityDatapapers',
              'https://www.irit.fr/opencommon/resourceTree/CommunityPublications',
              'https://www.irit.fr/opencommon/resourceTree/CommunityServices',
              'https://www.irit.fr/opencommon/resourceTree/CommunityVocabularies',
              'https://data.driihm.fr/catalogs/59c0f544-a008-48e0-b1dc-ded597025ca1'
            ],
            parentGraphs: [
              'https://www.irit.fr/opencommon/communities/189088ec-baa9-4397-8c6f-eefde9a3790c/communityResources'
            ]
          },
          {
            '@id': 'https://www.irit.fr/opencommon/resourceTree/PublicResources',
            '@type': [
              'http://www.w3.org/ns/dcat#Catalog',
              'http://www.w3.org/2002/07/owl#NamedIndividual'
            ],
            identifier: {
              '@type': 'http://www.w3.org/2000/01/rdf-schema#Literal',
              '@value': 'bd033e43-7c34-4641-a922-d04f77c3e0a6'
            },
            title: { en: 'Public resources', fr: 'Les ressources publiques' },
            catalog: [
              'https://www.irit.fr/opencommon/resourceTree/PublicActions',
              'https://www.irit.fr/opencommon/resourceTree/PublicCode',
              'https://www.irit.fr/opencommon/resourceTree/PublicDMP',
              'https://www.irit.fr/opencommon/resourceTree/PublicData',
              'https://www.irit.fr/opencommon/resourceTree/PublicDatapapers',
              'https://www.irit.fr/opencommon/resourceTree/PublicPublications',
              'https://www.irit.fr/opencommon/resourceTree/PublicServices',
              'https://www.irit.fr/opencommon/resourceTree/PublicVocabularies'
            ],
            parentGraphs: [
              'https://www.irit.fr/opencommon/communities/189088ec-baa9-4397-8c6f-eefde9a3790c/publicResources'
            ]
          }
        ]
      }
    ],
    isAuthenticated: true,
    userPrivateGraph: 'https://www.irit.fr/opencommon/member/123/private',
    expandedKeys: ['1cc0d1cd-e00a-4dfe-aa3d-49c7915b15d1'],
    selectedKey: '65ae9254-cf19-41f3-9d1e-cf6ff60b4889'
  }
}