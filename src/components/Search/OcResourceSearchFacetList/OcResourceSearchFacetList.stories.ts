import type { Meta, StoryObj } from '@storybook/vue3'

import OcResourceSearchFacetList from './OcResourceSearchFacetList.vue'

const meta: Meta<typeof OcResourceSearchFacetList> = {
  component: OcResourceSearchFacetList
}

export default meta
type Story = StoryObj<typeof OcResourceSearchFacetList>

export const Default: Story = {
  render: (args) => ({
    components: { OcResourceSearchFacetList },
    setup() {
      return { args }
    },
    template: '<OcResourceSearchFacetList v-bind="args" />'
  }),
  args: {
    modelValue: {}
  }
}
