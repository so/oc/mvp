import type { Meta, StoryObj } from '@storybook/vue3'
import { fn } from '@storybook/test'

import OcResourceSearchSimple from './OcResourceSearchSimple.vue'

const meta: Meta<typeof OcResourceSearchSimple> = {
  component: OcResourceSearchSimple
}

export default meta
type Story = StoryObj<typeof OcResourceSearchSimple>

export const Default: Story = {
  render: (args) => ({
    components: { OcResourceSearchSimple },
    setup() {
      return { args }
    },
    template: '<OcResourceSearchSimple v-bind="args" />'
  }),
  args: {
    modelValue: {
      params: {}
    },
    onSubmit: fn()
  }
}

export const InitialValues: Story = {
  render: (args) => ({
    components: { OcResourceSearchSimple },
    setup() {
      return { args }
    },
    template: '<OcResourceSearchSimple v-bind="args" />'
  }),
  args: {
    modelValue: {
      q: "DRIIHM",
      params: {
        title: ["mon titre"],
        creator: ["jean", "michel"]
      }
    },
    onSubmit: fn()
  }
}