import type { Meta, StoryObj } from '@storybook/vue3'

import OcSearchResultCard from './OcSearchResultCard.vue'

const meta: Meta<typeof OcSearchResultCard> = {
  component: OcSearchResultCard
}

export default meta
type Story = StoryObj<typeof OcSearchResultCard>

export const Default: Story = {
  render: (args) => ({
    components: { OcSearchResultCard },
    setup() {
      return { args }
    },
    template: '<OcSearchResultCard v-bind="args" />'
  }),
  args: {
    searchResult: {
      '@id': 'urn:testdataset:nakala:6d79f9ed-e10e-4eea-8bfe-93c0bdf370dc',
      '@type': ['http://www.w3.org/ns/dcat#Dataset'],
      identifier: 'test-6d79f9ed-e10e-4eea-8bfe-93c0bdf370dc',
      creator: [
        {
          '@id': 'urn:testdataset:nakala:benedictemadon',
          familyName: 'Madon',
          givenName: 'Bénédicte',
          mbox: 'mailto:benedicte.madon@gmail.com',
          name: 'Madon, Bénédicte'
        },
        {
          '@id': 'urn:testdataset:nakala:eliearnaud',
          familyName: 'Arnaud',
          givenName: 'Elie',
          mbox: 'mailto:elie.arnaud@mnhn.fr',
          name: 'Arnaud, Elie'
        },
        {
          '@id': 'urn:testdataset:nakala:emilielerigoleur',
          familyName: 'Lerigoleur',
          givenName: 'Emilie',
          mbox: 'mailto:emilie.lerigoleur@univ-tlse2.fr',
          name: 'Lerigoleur, Emilie'
        },
        {
          '@id': 'urn:testdataset:nakala:ericfoulquier',
          familyName: 'Foulquier',
          givenName: 'Eric',
          mbox: 'mailto:eric.foulquier@univ-brest.fr',
          name: 'Foulquier, Eric'
        },
        {
          '@id': 'urn:testdataset:nakala:etiennejeannesson',
          familyName: 'Jeannesson',
          givenName: 'Etienne',
          mbox: 'mailto:etienne.jeannesson@ofb.gouv.fr',
          name: 'Jeannesson, Etienne'
        },
        {
          '@id': 'urn:testdataset:nakala:iwanleberre',
          familyName: 'Le Berre',
          givenName: 'Iwan',
          mbox: 'mailto:iwan.leberre@univ-brest.fr',
          name: 'Le Berre, Iwan'
        },
        {
          '@id': 'urn:testdataset:nakala:jeanlucjung',
          familyName: 'Jung',
          givenName: 'Jean-Luc',
          mbox: 'mailto:jean-luc.jung@mnhn.fr',
          name: 'Jung, Jean-Luc'
        },
        {
          '@id': 'urn:testdataset:nakala:juliensananikone',
          familyName: 'Sananikone',
          givenName: 'Julien',
          mbox: 'mailto:julien.sananikone@mnhn.fr',
          name: 'Sananikone, Julien'
        },
        {
          '@id': 'urn:testdataset:nakala:laurentbouveret',
          familyName: 'Bouveret',
          givenName: 'Laurent',
          mbox: 'mailto:laurent.bouveret@gmail.com',
          name: 'Bouveret, Laurent'
        },
        {
          '@id': 'urn:testdataset:nakala:lorrainecoche',
          familyName: 'Coché',
          givenName: 'Lorraine',
          mbox: 'mailto:lorraine.coche@gmail.com',
          name: 'Coché, Lorraine'
        },
        {
          '@id': 'urn:testdataset:nakala:maximesebe',
          familyName: 'Sèbe',
          givenName: 'Maxime',
          mbox: 'mailto:maxime.sebe@gmail.com',
          name: 'Sèbe, Maxime'
        },
        {
          '@id': 'urn:testdataset:nakala:nadegegandilhon',
          familyName: 'Gandilhon',
          givenName: 'Nadège',
          mbox: 'mailto:ngandilhon75@gmail.com',
          name: 'Gandilhon, Nadège'
        },
        {
          '@id': 'urn:testdataset:nakala:pascaljeanlopez',
          familyName: 'Lopez',
          givenName: 'Pascal Jean',
          mbox: 'mailto:pjlopez@mnhn.fr',
          name: 'Lopez, Pascal Jean'
        },
        {
          '@id': 'urn:testdataset:nakala:romaindavid',
          familyName: 'David',
          givenName: 'Romain',
          mbox: 'mailto:david.romain@gmail.com',
          name: 'David, Romain'
        },
        {
          '@id': 'urn:testdataset:nakala:yvanlebras',
          familyName: 'Le Bras',
          givenName: 'Yvan',
          mbox: 'mailto:yvan.le-bras@mnhn.fr',
          name: 'Le Bras, Yvan'
        }
      ],
      description: {
        fr: "Base de données collectée dans le cadre du stage de master 2 Lorraine Coché \"Inventaire et structuration des données d'observation des mammifères marins autour de la Guadeloupe\" en 2020 (Master Écosystèmes marins tropicaux de l'Université des Antilles). Cette base de données a été constituée dans un esprit de science participative. Elle centralise et harmonise les données d'observation collectées par l'équipe du Sanctuaire Agoa (Aire Marine Protégée), l'OMMAG (Observatoire des Mammifères Marins de l'Archipel Guadeloupéen), BREACH Antilles, et les sociétés de whale-watching Cétacés Caraïbes, Guadeloupe Evasion Découverte et Aventures Marines.",
        en: "Database collected as part of Lorraine Coché's Master 2 course entitled \"Inventory and structuring of marine mammal observation data around Guadeloupe\" in 2020 (Master's degree in Tropical Marine Ecosystems at the University of the West Indies). This database has been set up in the spirit of participatory science. It centralises and harmonises the observation data collected by the Agoa Sanctuary team (Marine Protected Area), OMMAG (Observatoire des Mammifères Marins de l'Archipel Guadeloupéen), BREACH Antilles, and the whale-watching companies Cétacés Caraïbes, Guadeloupe Evasion Découverte and Aventures Marines."
      },
      title: {
        fr: "KAKILA, Base de données d'observation des mammifères marins autour de l'archipel de la Guadeloupe dans le sanctuaire AGOA - Antilles françaises",
        en: 'KAKILA, Marine mammal observation database around the Guadeloupe archipelago in the AGOA sanctuary - French West Indies'
      },
      distribution: [
        'urn:testdistribution:nakala:kakila-database',
        'urn:testdistribution:dataindores:kakila-database'
      ],
      parentCatalog:{
        '@id': "https://data.driihm.fr/catalogs/driihm2",
        '@type': ['http://www.w3.org/ns/dcat#Catalog'],
        'identifier': "2af4cfa0-9f3b-11ee-a39a-d6e96336453a",
        'title': {"fr":"LabEx DRIIHM 2","en":"DRIIHM LabEx 2"},
        'graph': ["ex:sandbox2"]
      }
    },
    community: {
      description: {
        fr: "Lauréat de la deuxième vague de l'appel à projet Laboratoire d'Excellence (LabEx) dans le cadre du programme « Investissements d'avenir », le LabEx DRIIHM, Dispositif de Recherche Interdisciplinaire sur les Interactions Hommes-Milieux, regroupe à ce jour 13 Observatoires Hommes-Milieux, outils d'observation de socio-écosystèmes impactés par un événement d'origine anthropique. Créés par le CNRS-INEE en 2007, ils sont répartis en France métropolitaine, en outre-mer et à l’étranger.",
        en: "Laureate of the Laboratory for Excellence project (LabEx) in the program « Investment in the future », the DRIIHM LabEx, Device for Interdisciplinary Research on human-environments Interactions, aggregate 13 human-environments observatories (OHM in french), tools for observing socio-ecosystems impacted by anthropic events. Created by CNRS-INEE in 2007, they are located in metropolitan France, overseas France and abroad."
      },
      identifier: "189088ec-baa9-4397-8c6f-eefde9a3790c",
      title: {
        fr: "Communauté du LabEx DRIIHM",
        en: "DRIIHM Community"
      },
      logo: "https://www.driihm.fr/images/images/logos_png/logo_DRIIHM_r%C3%A9duit.png",
      name: "driihm",
      isSpaceOf: "https://www.irit.fr/opencommon/agents/organization/9a20f121-c64e-4049-93a7-4bedbe819fd6",
      color: 'linen'
    }
  }
}
