import type { Meta, StoryObj } from '@storybook/vue3'

import OcSearchResultCardSkeleton from './OcSearchResultCardSkeleton.vue'

const meta: Meta<typeof OcSearchResultCardSkeleton> = {
  component: OcSearchResultCardSkeleton
}

export default meta
type Story = StoryObj<typeof OcSearchResultCardSkeleton>

export const Default: Story = {
  render: (args) => ({
    components: { OcSearchResultCardSkeleton },
    setup() {
      return { args }
    },
    template: '<OcSearchResultCardSkeleton v-bind="args" />'
  }),
}
