import type { Meta, StoryObj } from '@storybook/vue3'

import OcDescriptorCatalog from './OcDescriptorCatalog.vue'
import catalogExample from '@/assets/catalogExample1.json'

const meta: Meta<typeof OcDescriptorCatalog> = {
  component: OcDescriptorCatalog
}

export default meta
type Story = StoryObj<typeof OcDescriptorCatalog>

export const Default: Story = {
  render: (args) => ({
    components: { OcDescriptorCatalog },
    setup() {
      return { args }
    },
    template: '<OcDescriptorCatalog v-bind="args" />'
  }),
  args: {
    catalog: catalogExample,
    community: {
      name: 'example'
    }
  }
}
