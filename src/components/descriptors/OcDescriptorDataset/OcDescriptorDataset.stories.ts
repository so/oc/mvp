import type { Meta, StoryObj } from '@storybook/vue3'

import OcDescriptorDataset from './OcDescriptorDataset.vue'
import { datasetExample1 } from '@/assets/datasetExample1'

const meta: Meta<typeof OcDescriptorDataset> = {
  component: OcDescriptorDataset
}

export default meta
type Story = StoryObj<typeof OcDescriptorDataset>

export const Default: Story = {
  render: (args) => ({
    components: { OcDescriptorDataset },
    setup() {
      return { args }
    },
    template: '<OcDescriptorDataset v-bind="args" />'
  }),
  args: {
    dataset: datasetExample1,
    community: {
      name: 'example'
    }
  }
}
