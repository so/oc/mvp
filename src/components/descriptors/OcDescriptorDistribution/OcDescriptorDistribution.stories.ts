import type { Meta, StoryObj } from '@storybook/vue3'

import OcDescriptorDistribution from './OcDescriptorDistribution.vue'
import distributionExample from '@/assets/distributionExample1.json'

const meta: Meta<typeof OcDescriptorDistribution> = {
  component: OcDescriptorDistribution
}

export default meta
type Story = StoryObj<typeof OcDescriptorDistribution>

export const Default: Story = {
  render: (args) => ({
    components: { OcDescriptorDistribution },
    setup() {
      return { args }
    },
    template: '<OcDescriptorDistribution v-bind="args" />'
  }),
  args: {
    distribution: distributionExample,
    community: {
      name: 'example'
    }
  }
}
