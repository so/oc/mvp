import type { Meta, StoryObj } from '@storybook/vue3'

import OcDescriptorResource from './OcDescriptorResource.vue'
import { datasetExample1 } from '@/assets/datasetExample1'

const meta: Meta<typeof OcDescriptorResource> = {
  component: OcDescriptorResource
}

export default meta
type Story = StoryObj<typeof OcDescriptorResource>

export const Default: Story = {
  render: (args) => ({
    components: { OcDescriptorResource },
    setup() {
      return { args }
    },
    template: '<OcDescriptorResource v-bind="args" />',
  }),
  args: {
    loading: false,
    community: {
      identifier: "189088ec-baa9-4397-8c6f-eefde9a3790c",
      title: {
        fr: "Communauté du LabEx DRIIHM",
        en: "DRIIHM Community"
      },
      name: 'driihm'
    }
  }
}

export const Loading: Story = {
  render: (args) => ({
    components: { OcDescriptorResource },
    setup() {
      return {
        args
      }
    },
    template: '<OcDescriptorResource v-bind="args" />'
  }),
  args: {
    loading: true,
    community: {
      identifier: "189088ec-baa9-4397-8c6f-eefde9a3790c",
      title: {
        fr: "Communauté du LabEx DRIIHM",
        en: "DRIIHM Community"
      },
      name: 'driihm'
    }
  }
}

export const Kakila: Story = {
  render: (args) => ({
    components: { OcDescriptorResource },
    setup() {
      return {
        args
      }
    },
    template: '<OcDescriptorResource v-bind="args" />'
  }),
  args: {
    loading: false,
    resource: datasetExample1,
    community: {
      identifier: "189088ec-baa9-4397-8c6f-eefde9a3790c",
      title: {
        fr: "Communauté du LabEx DRIIHM",
        en: "DRIIHM Community"
      },
      name: 'driihm'
    }
  }
}
