import type { Meta, StoryObj } from '@storybook/vue3'

import OcDescriptorResourceSkeleton from './OcDescriptorResourceSkeleton.vue'

const meta: Meta<typeof OcDescriptorResourceSkeleton> = {
  component: OcDescriptorResourceSkeleton
}

export default meta
type Story = StoryObj<typeof OcDescriptorResourceSkeleton>

export const Default: Story = {
  render: () => ({
    components: { OcDescriptorResourceSkeleton },
    template: '<OcDescriptorResourceSkeleton />'
  }),
}
  