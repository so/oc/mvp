import type { Meta, StoryObj } from '@storybook/vue3'

import OcDescriptorSpatialCoverage from './OcDescriptorSpatialCoverage.vue'
import { datasetExample1 } from '@/assets/datasetExample1'
import example1 from './example1.json'
import example2 from './example2.json'

const meta: Meta<typeof OcDescriptorSpatialCoverage> = {
  component: OcDescriptorSpatialCoverage
}

export default meta
type Story = StoryObj<typeof OcDescriptorSpatialCoverage>

export const Default: Story = {
  render: (args) => ({
    components: { OcDescriptorSpatialCoverage },
    setup() {
      return { args }
    },
    template: '<OcDescriptorSpatialCoverage v-bind="args" />'
  }),
  args: {
    spatialItems: datasetExample1.spatial?.filter((item) => item.geometry )
  }
}


export const Example1: Story = {
  render: (args) => ({
    components: { OcDescriptorSpatialCoverage },
    setup() {
      return { args }
    },
    template: '<OcDescriptorSpatialCoverage v-bind="args" />'
  }),
  args: {
    spatialItems: example1
  }
}

export const Example2: Story = {
  render: (args) => ({
    components: { OcDescriptorSpatialCoverage },
    setup() {
      return { args }
    },
    template: '<OcDescriptorSpatialCoverage v-bind="args" />'
  }),
  args: {
    spatialItems: example2
  }
}
