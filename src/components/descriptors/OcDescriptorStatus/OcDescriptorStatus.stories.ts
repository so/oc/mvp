import type { Meta, StoryObj } from '@storybook/vue3'
import OcDescriptorStatus from './OcDescriptorStatus.vue'
import status from './status.json'

const meta: Meta<typeof OcDescriptorStatus> = {
  component: OcDescriptorStatus
}

export default meta
type Story = StoryObj<typeof OcDescriptorStatus>

export const Completed: Story = {
  render: (args) => ({
    components: { OcDescriptorStatus },
    setup() {
      return { args }
    },
    template: '<OcDescriptorStatus v-bind="args" />'
  }),
  args: {
    status: status[0]
  }
}
export const Alls: Story = {
  render: () => ({
    components: { OcDescriptorStatus },
    setup() {
      return { statusList: status }
    },
    template: `
        <div class="flex flex-col items-start gap-4">
          <OcDescriptorStatus 
            v-for="(status, idx) in statusList"
            :key="status['@id']"
            :status="status"
          />
        </div>
      `
  })
}
