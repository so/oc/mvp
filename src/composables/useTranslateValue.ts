import type { LocalizedProperty } from '@/declarations'
import { useI18n, type I18nScope } from 'vue-i18n'

const CACHE_TRANSLATE_KEYVALUE = []

export function useTranslateValue(useScope: I18nScope = 'global') {
  const { t, mergeLocaleMessage } = useI18n({
    useScope
  })

  const translateValue = (localizedProperty?: LocalizedProperty<string>): string => {
    if (localizedProperty === undefined) {
      return ''
    } else if (typeof localizedProperty === 'string') {
      return localizedProperty
    }

    let key: string

    /**
     * First time we find this localizedProperty
     * 
     * We init it
     */
    const lpCached = CACHE_TRANSLATE_KEYVALUE.find(e => e.lp === localizedProperty)
    if (!lpCached) {
      // For each localized property of an external resource,
      // we add each localized value as a message in i18n.
      // We take the first found value as the message key,
      // this way, if we can't fallback to any locale, at least we
      // print a real message.
      const keyValue = Object.values(localizedProperty)[0]

      // when we have severeal values for one locale, we arbitrary
      // take the first one.
      key = typeof keyValue === 'string' ? keyValue : keyValue[0]

      Object.keys(localizedProperty).forEach(function (locale) {
        if (Array.isArray(localizedProperty[locale])) {
          // when we have severeal values for one locale, we arbitrary
          // take the first one.
          mergeLocaleMessage(locale, { [key]: localizedProperty[locale][0] })
        } else {
          mergeLocaleMessage(locale, { [key]: localizedProperty[locale] })
        }
      });
      CACHE_TRANSLATE_KEYVALUE.push({ keyValue, lp: localizedProperty })
    } else {
      const keyValue = lpCached.keyValue
      key = typeof keyValue === 'string' ? keyValue : keyValue[0]
    }

    return t(key)
  }

  return { translateValue }
}
