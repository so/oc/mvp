import type { Credentials } from '@/declarations'
import { getProfileInfos } from '@/sparql/connection'
import { useAccountStore } from '@/stores/account'
import { defineBasicLoader } from 'unplugin-vue-router/data-loaders/basic'
import { NavigationResult } from 'unplugin-vue-router/runtime'

/**
 * - Populates account store if user is connected
 * - Handles navigation guard related to authentication
 */
export const useAccountData = defineBasicLoader(
  async (route) => {
    const accountStore = useAccountStore()

    if (accountStore.isAuthenticated && accountStore.profile === null) {
      const { infos, profile, memberships } = await getProfileInfos(accountStore.auth as Credentials)

      accountStore.profile = profile
      accountStore.infos = infos
      accountStore.memberships = memberships
    }

    if (route.meta.needsAuth && !accountStore.isAuthenticated) {
      return new NavigationResult({
        name: 'catchall',
        params: {
          lang: route.params.lang,
          path: 'error'
        },
        query: {
          code: 403,
          from: route.path
        }
      })
    }
  }
)
