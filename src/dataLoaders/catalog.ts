import { defineBasicLoader } from 'unplugin-vue-router/data-loaders/basic'
import { useAccountData } from './account'
import { useAccountStore } from '@/stores/account'
import { getCatalogSummary } from '@/sparql/catalog'
import { NavigationResult } from 'unplugin-vue-router/data-loaders'

/**
 * - Fetchs a catalog asked by route parameters
 */
export const useCatalogData = defineBasicLoader(
  async (route) => {

    const accountStore = useAccountStore()

    if (route.params.identifier){
        await useAccountData()

        const catalog = await getCatalogSummary(route.params.identifier as string, accountStore.auth)

        if (catalog) {
          return catalog
        } else {
          return new NavigationResult({
            name: 'catchall',
            params: {
              lang: route.params.lang,
              path: 'error'
            },
            query: {
              code: 404,
              from: route.path
            }
          })
        }
    } else {
      throw new Error('No identifier in the route')
    }
  }
)
