import { useCommunityStore } from '@/stores/community'
import { defineBasicLoader } from 'unplugin-vue-router/data-loaders/basic'
import { NavigationResult } from 'unplugin-vue-router/runtime'
import { useAccountData } from './account'
import { useAbility } from '@casl/vue'

/**
 * - Fetchs community asked by route parameters
 * - Handles nav guard related to community membership
 */
export const useCommunityData = defineBasicLoader(
  async (route) => {
    const { cannot } = useAbility()
    const communityStore = useCommunityStore()

    await useAccountData()
    await communityStore.fetchIfEmpty()

    const community = communityStore.getByName(route.params.community as string)

    if (!community) {
      return new NavigationResult({
        name: 'catchall',
        params: {
          lang: route.params.lang,
          path: 'error'
        },
        query: {
          code: 404,
          from: route.path
        }
      })
    }

    if (route.meta.needsCommunityMembership && cannot('read', community)) {
      return new NavigationResult({
        name: 'catchall',
        params: {
          lang: route.params.lang,
          path: 'error'
        },
        query: {
          code: 403,
          from: route.path
        }
      })
    }

    return community
  }
)
