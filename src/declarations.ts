import type { ContextDefinition } from 'jsonld'
import type { RouteLocationAsRelativeGeneric } from 'vue-router'
import {
  roleVisitor,
  roleCommunityMember,
  roleCommunityManager,
  rolePlatformManager,
  roleCatalogManager
} from '@/ability'
import type { type2ResourceType } from './helpers/resourceType'

export type Credentials = {
  email: string
  password: string
}

export type HttpResponse = {
  status: number
  data: string
}

export type LocalizedProperty<T> = { [locale: string]: T | T[] }
export type Visibility = 'public' | 'community' | 'private' | 'protected'

export type OcCommunity = {
  '@id': string
  '@type': string | string[]
  identifier: string
  name: string
  title: LocalizedProperty<string>
  description: LocalizedProperty<string>
  abstract: LocalizedProperty<string>
  logo: undefined | string
  color: undefined | string
  isSpaceOf: undefined | string
  catalog: string[]
}

export type OcAdmsIdentifier = {
  '@id'?: string
  notation: string
  creator: string
  schemeAgency?: string
  issued?: Date
}

export type OcResource = {
  '@id': string
  '@type': string[]
  identifier: string
  title: LocalizedProperty<string>
  description: LocalizedProperty<string>
  graph?: string[]
  conformsTo?: OcConcept
  issued?: Date
  license?: OcConcept
}

export type OcDataset = OcResource & {
  otherIdentifier?: OcAdmsIdentifier[]
  creator?: Array<OcPerson | OcOrganization>
  publisher?: OcOrganization
  contactPoint?: Array<OcPerson | OcOrganization>
  type?: OcConcept
  theme?: OcConcept[]
  accrualPeriodicity?: OcConcept
  language?: OcConcept[]
  keyword?: LocalizedProperty<string[]>
  status?: OcConcept
  modified?: Date
  temporal?: [Date, Date][]
  spatial?: Array<OcConcept | OcGeometry>
  landingPage?: string
  version?: string
  catalog?: Partial<OcCatalog>
  distribution?: Partial<OcDistribution>[]
  isPartOf?: any[]
  provenance?: OcProvenance[]
  inputData?: OcDataset[]
  otherResources?: OcResource[]
}

/**
 * A representation of a Dataset used
 * for dashboards.
 */
export type OcDatasetSummary = OcResource & {
  otherIdentifier?: OcAdmsIdentifier[]
  issued?: Date
  modified?: Date
  license?: string
  version?: string
  status?: string
  distribution?: string[]
  distributionSummary?: OcDistributionSummary[]
  loadingDistributionSummary?: boolean
}

export type OcCatalog = OcDataset & {
  contactPoint?: Array<OcPerson | OcOrganization>
  publisher?: OcOrganization
  theme?: OcConcept[]
  issued?: Date
  temporal?: [Date, Date][]
  spatial?: Array<OcConcept | OcGeometry>
  parentCatalog?: OcCatalog | OcCatalogSummary
  datasets: OcDataset[]
  catalogs: OcCatalog[]
}

export type OcCatalogSummary = OcResource & {
  catalog?: string[]
}

export type OcDistribution = OcResource & {
  license?: OcConcept
  type?: OcConcept
  accessRights?: OcConcept
  accessURL?: string
  format?: OcConcept
  accessService?: OcService
  issued?: Date
}

/**
 * A representation of a Distribution used
 * for dashboards.
 */
export type OcDistributionSummary = OcResource & {
  issued?: Date
  license?: string
}

/** 
 * A representation of a search result used
 * for search page results cards
 */
export type OcSearchResult = OcResource & {
  version?: string
  creator?: Array<OcPerson | OcOrganization>
  catalog?: string[]
  dataset?: string[]
  distribution?: string[]
  parentCatalog?: OcCatalog | OcCatalog[]
}

/** A representation of a search query */
export type OcSearchQuery = {
  q?: string
  params: OcSearchParameters
}

export type OcSearchParameters = Partial<Record<keyof typeof SearchQueryParams, string[]>>

export enum SearchQueryParams {
  title = "http://purl.org/dc/terms/title",
  description = "http://purl.org/dc/terms/description",
  creator = "http://purl.org/dc/terms/creator"
}

export type OcTreeNode = OcResource & {
  children: Array<OcTreeNode>
  catalog?: Array<OcTreeNode | string>
  dataset?: Array<OcTreeNode | string>
  distribution?: Array<OcTreeNode | string>
  loading?: boolean
}

export type OcService = OcResource

export type OcProvenance = OcResource & {
  label: LocalizedProperty<string>
}

export type OcConcept = {
  '@id': string
  prefLabel: LocalizedProperty<string>
  altLabel?: LocalizedProperty<string>
  narrower?: string[]
}

export type OcConceptScheme = {
  '@id': string
  prefLabel: LocalizedProperty<string>
}

export type OcGeometry = {
  '@id'?: string
  geometry: Array<{ '@type': string; '@value': string }>
}

export type OcOrganization = {
  '@id': string
  '@type': string | string[]
  identifier?: string
  name: LocalizedProperty<string>
  mbox?: string | string[]
}

export type OcPerson = {
  '@id': string
  '@type': string | string[]
  identifier?: string
  familyName?: string
  givenName?: string
  firstName?: string
  homepage?: string
  description?: string
  access?: Array<string>
  mbox?: Array<string> | string
  organization?: Array<string> | string
  topic_interest?: Array<string> | string
}

export type OcProfileInfo = {
  infos: OcMemberInfos | null,
  profile: OcPerson | null,
  memberships: OcMembership[]
}

export type OcMemberInfos = {
  '@id': string
  loginID: string
  hasPrivateGraph: string
  hasSystemGraph: string
}

export type OcMembership = {
  role: OcRole
  organization: string
}

export type HomeCommunityAction = {
  id: string
  title: LocalizedProperty<string>
  description: LocalizedProperty<string>
  fair?: OcFairValues
  needAccount: boolean
}

export type OcFairValues = Array<'f' | 'a' | 'i' | 'r'>

/**
 * @see https://ror.readme.io/docs/data-structure
 */
export type RorOrganization = {
  id: string
  name: string
  acronyms: string[]
  country: { country_name: string; country_code: string }
}

export type OcBreadcrumbItem = {
  label: string
  key: string
  type: string
  to?: RouteLocationAsRelativeGeneric
}

export type OcFieldMetadata = {
  label: LocalizedProperty<string>
  propertyUri?: string
  dereferencement?: string
  required?: boolean
  desc?: LocalizedProperty<string>
  fair?: OcFairValues
  comment?: LocalizedProperty<string>
  vocabularies?: string[]
  context?: ContextDefinition
}

export type OcModelMetadata<T> = Record<keyof T, OcFieldMetadata>

export type OcLocale = {
  '@id': string
  prefLabel: LocalizedProperty<string>
  code: string
}

export type OcRole =
  | typeof roleVisitor
  | typeof roleCommunityMember
  | typeof roleCommunityManager
  | typeof rolePlatformManager
  | typeof roleCatalogManager
