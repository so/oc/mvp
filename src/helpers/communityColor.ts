import type { OcCommunity } from "@/declarations";

const availableColors = ['olivine', 'linen', 'bluesky']

export function getColor(community?: OcCommunity): string {
  const color = community?.color ?? 'olivine'

  return availableColors.includes(color) ? color : 'olivine'
}
