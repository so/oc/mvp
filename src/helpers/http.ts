import type { Credentials, HttpResponse } from "@/declarations";

/**
 * Perform XMLHttpRequest as Promise
 */
export function httpFetch(
  url: string,
  options?: {
    method?: 'HEAD' | 'GET' | 'POST',
    headers?: Record<string, string>,
    body?: XMLHttpRequestBodyInit,
    auth?: Credentials,
  }
): Promise<HttpResponse> {
  return new Promise<HttpResponse>(function (resolve, reject) {
    const xhr = new XMLHttpRequest();
    xhr.open(
      options?.method ?? 'GET',
      url,
      true,
      options?.auth?.email,
      options?.auth?.password
    );

    for (const key in options?.headers) {
      xhr.setRequestHeader(key, options.headers[key])
    }

    xhr.onload = function () {
      if (xhr.status >= 200 && xhr.status < 300) {
        resolve({
          status: xhr.status,
          data: xhr.response,
        });
      } else {
        reject({
          status: xhr.status,
          data: xhr.statusText,
        });
      }
    };
    xhr.onerror = function () {
      reject({
        status: xhr.status,
        data: xhr.statusText,
      });
    };

    xhr.send(options?.body);
  });
}
