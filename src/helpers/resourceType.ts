export enum ResourceType {
  CATALOG ='catalog',
  DATASET ='dataset',
  DISTRIBUTION ='distribution',
  SERVICE ='service',
  CONCEPT = 'concept',
  UNKNOWN = 'unknown',
}

export const type2ResourceType: Record<string, ResourceType> = {
  'http://www.w3.org/ns/dcat#Catalog': ResourceType.CATALOG,
  'http://www.w3.org/ns/dcat#Dataset': ResourceType.DATASET,
  'http://www.w3.org/ns/dcat#Distribution': ResourceType.DISTRIBUTION,
  'http://www.w3.org/ns/dcat#DataService': ResourceType.SERVICE,
  'http://www.w3.org/2004/02/skos/core#Concept': ResourceType.CONCEPT
}

export const getResourceTypeFromAtType = (atType: string | string[]): ResourceType => {
  if (Array.isArray(atType)){
    const compatibleTypes = atType.filter((type) => type in type2ResourceType)
    return type2ResourceType[compatibleTypes[0]] ?? ResourceType.UNKNOWN
  } else {
    return type2ResourceType[atType] ?? ResourceType.UNKNOWN
  }
}