import type { OcCommunity, OcResource, Visibility } from "@/declarations";
import type { Subject } from "@casl/ability";

export function getCommunityPublicGraph(community?: OcCommunity, legacy: boolean = false) {
  // TODO Remove this management of legacy graphs once the graphs have been cleaned. (#66)
  return `https://www.irit.fr/opencommon/${legacy ? 'spaces' : 'communities'}/${community?.identifier}/publicResources`
}

export function getCommunityGraph(community?: OcCommunity, legacy: boolean = false) {
  // TODO Remove this management of legacy graphs once the graphs have been cleaned. (#66)
  return `https://www.irit.fr/opencommon/${legacy ? 'spaces' : 'communities'}/${community?.identifier}/communityResources`
}

export function getResourceVisibility(
  community?: OcCommunity,
  resource?: OcResource,
  userPrivateGraph?: string
): Visibility {
  const graph = resource?.graph ?? []

  if (
    graph.includes(getCommunityPublicGraph(community))
    || graph.includes(getCommunityPublicGraph(community, true))
  ) {
    return 'public'
  } else if (
    graph.includes(getCommunityGraph(community))
    || graph.includes(getCommunityGraph(community, true))
  ) {
    return 'community'
  } else if (userPrivateGraph && graph.includes(userPrivateGraph)) {
    return 'private'
  } else {
    // If we can't say, then it's protected
    return 'protected'
  }
}

export function getVisibilityOptionsFor(
  can: (action: string, subject: Subject, field?: string | undefined) => boolean,
  community?: OcCommunity,
  resource?: OcResource,
  userPrivateGraph?: string,
) {
  const publicGraph = getCommunityPublicGraph(community)
  const communityGraph = getCommunityGraph(community)
  const options = {
    public: {
      key: 'public',
      graph: publicGraph,
      disabled: !can('write', publicGraph)
    },
    community: {
      key: 'community',
      graph: communityGraph,
      disabled: !can('write', communityGraph)
    },
    private: {
      key: 'private',
      graph: userPrivateGraph,
      disabled: false
    },
  }

  if (resource === undefined) {
    options.public.disabled = true
    options.community.disabled = true
    options.private.disabled = true

    return Object.values(options)
  }

  switch (getResourceVisibility(community, resource, userPrivateGraph)) {
    case 'public':
      // Nothing to do, all options are available
      break
    case 'community':
    case 'protected':
      options.public.disabled = true
      break
    case 'private':
      options.public.disabled = true
      options.community.disabled = true
      break
  }

  return Object.values(options)
}
