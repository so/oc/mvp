export function truncateText(text: string, maxSize: number = 40) {
  if (text.length > maxSize) {
    return text.substring(0, maxSize) + ' ...'
  } else {
    return text
  }
}
