import { createI18n } from 'vue-i18n'
import messageEn from './locales/en'
import messageFr from './locales/fr'
import { setLocale as setYupLocale } from 'yup'

export const availableLocales: { [key: string]: string } = {
  en: 'English',
  fr: 'Français'
}

const i18n = createI18n({
  legacy: false,
  locale: 'en',
  inheritLocale: false,
  messages: {
    en: messageEn,
    fr: messageFr
  },
  fallbackLocale: ['en', 'en-t-fr-t0-mtec', '@none'],
  missingWarn: false,
  fallbackWarn: false,
})

// Defines yup translations
setYupLocale({
  mixed: {
    default: ({ label }) => i18n.global.t('form.validation.mixed.default', { label: label }),
    required: ({ label }) => i18n.global.t('form.validation.mixed.required', { label: label }),
    notNull: ({ label }) => i18n.global.t('form.validation.mixed.notNull', { label: label })
  },
  string: {
    url: ({ label }) => i18n.global.t('form.validation.string.url', { label: label }),
    email: ({ label }) => i18n.global.t('form.validation.string.email', { label: label })
  },
  array: {
    min: ({ label, min }) =>
      i18n.global.t('form.validation.array.min', { label: label, min: min }, min)
  }
})

export default i18n
