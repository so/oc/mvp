import type { Meta, StoryObj } from '@storybook/vue3';

import OcLayoutBiColumn from './OcLayoutBiColumn.vue';

const meta: Meta<typeof OcLayoutBiColumn> = {
  component: OcLayoutBiColumn,
};

export default meta;
type Story = StoryObj<typeof OcLayoutBiColumn>;

export const Default: Story = {
  render: (args) => ({
    components: { OcLayoutBiColumn },
    setup() {
      return { args };
    },
    template: '<OcLayoutBiColumn v-bind="args"><h1>content</h1></OcLayoutBiColumn>',
  }),
  args: {
  },
};

export const WithCommunity: Story = {
  render: (args) => ({
    components: { OcLayoutBiColumn },
    setup() {
      return { args };
    },
    template: '<OcLayoutBiColumn v-bind="args"><h1>content</h1></OcLayoutBiColumn>',
  }),
  args: {
    community: {
      description: {
        fr: "Lauréat de la deuxième vague de l'appel à projet Laboratoire d'Excellence (LabEx) dans le cadre du programme « Investissements d'avenir », le LabEx DRIIHM, Dispositif de Recherche Interdisciplinaire sur les Interactions Hommes-Milieux, regroupe à ce jour 13 Observatoires Hommes-Milieux, outils d'observation de socio-écosystèmes impactés par un événement d'origine anthropique. Créés par le CNRS-INEE en 2007, ils sont répartis en France métropolitaine, en outre-mer et à l’étranger.",
        en: "Laureate of the Laboratory for Excellence project (LabEx) in the program « Investment in the future », the DRIIHM LabEx, Device for Interdisciplinary Research on human-environments Interactions, aggregate 13 human-environments observatories (OHM in french), tools for observing socio-ecosystems impacted by anthropic events. Created by CNRS-INEE in 2007, they are located in metropolitan France, overseas France and abroad."
      },
      identifier: "189088ec-baa9-4397-8c6f-eefde9a3790c",
      title: {
        fr: "Communauté du LabEx DRIIHM",
        en: "DRIIHM Community"
      },
      logo: "https://www.driihm.fr/images/images/logos_png/logo_DRIIHM_r%C3%A9duit.png",
      name: "driihm",
      isSpaceOf: "https://www.irit.fr/opencommon/agents/organization/9a20f121-c64e-4049-93a7-4bedbe819fd6",
      color: 'linen'
    }
  },
};

export const WithBreadcrumb: Story = {
  render: (args) => ({
    components: { OcLayoutBiColumn },
    setup() {
      return { args };
    },
    template: '<OcLayoutBiColumn v-bind="args"><h1>content</h1></OcLayoutBiColumn>',
  }),
  args: {
    breadcrumbItems: [
      {
        label: "LabEx DRIIHM",
        key: "driihm",
        type: "community",
      },
      {
        label: "Rejoindre une communauté",
        key: "driihm-join",
        type: "join",
      }]
  },
};