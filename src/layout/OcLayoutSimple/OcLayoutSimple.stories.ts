import type { Meta, StoryObj } from '@storybook/vue3';

import OcLayoutSimple from './OcLayoutSimple.vue';

const meta: Meta<typeof OcLayoutSimple> = {
  component: OcLayoutSimple,
};

export default meta;
type Story = StoryObj<typeof OcLayoutSimple>;

export const Default: Story = {
  render: (args) => ({
    components: { OcLayoutSimple },
    setup() {
      return { args };
    },
    template: '<OcLayoutSimple v-bind="args"><h1>content</h1></OcLayoutSimple>',
  }),
  args: {
  },
};

export const WithBreadcrumb: Story = {
  render: (args) => ({
    components: { OcLayoutSimple },
    setup() {
      return { args };
    },
    template: '<OcLayoutSimple v-bind="args"><h1>content</h1></OcLayoutSimple>',
  }),
  args: {
    breadcrumbItems: [
      {
        label: "Dashboard",
        key: "dashboard",
        type: "dashboard",
      },{
        label: "Manage my datasets",
        key: "managemydatasets",
        type: "dashboardDatasets",
      }
    ]
  },
};
