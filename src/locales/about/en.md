The [OpenCommon platform](https://opencommon.irit.fr/last/dist/spa/#/) was initiated by research prototypes from the [DataNoos](https://datanoos.univ-toulouse.fr/) project project, funded by RTRA STAE from 2018 to 2021. Today, DataNoos is a [platform of expertise on the valorization of interdisciplinary data at the University of Toulouse](https://mshs.univ-toulouse.fr/plateformes/les-plateformes-dexpertise/datanoos/).

This work continued with the specification of data formatting during the RTRA STAE's [ENV'IA project](https://groupes.renater.fr/wiki/envia/). ENV'IA aims to connect the environment and AI communities. Following on from this, the [ANR Semantics4FAIR](https://anr.fr/Project-ANR-19-DATA-0014) project studied semantic web technologies to implement [FAIR principles](https://www.go-fair.org/fair-principles/) for the reuse of scientific data. In this case, MétéoFrance data was used as a case study to make it easier to reuse in scientific research.
Subsequently, the [ANR So-DRIIHM](https://anr.fr/fr/lanr/engagements/la-science-ouverte/les-projets-laureats-de-lappel-flash-science-ouverte/projet-so-driihm/) project set out to offer [DRIIHM labex](https://www.driihm.fr/) researchers a solution for reusing their data.

The solution studied and developed is that described by the use case of an [open science intermediation platform for an interdisciplinary](https://datanoos.univ-toulouse.fr/fr/use-case-plateforme-science-ouverte) DataNoos community.
A follow-up is underway with the PFR-DRIIHM project of France Relance and the [University of Paul Sabatier](https://www.univ-tlse3.fr/) ([IRIT](http://www.irit.fr/)) to further develop the current Open Common platform.

The application's source code is available on [IRIT's software forge](https://gitlab.irit.fr/so/oc/mvp).

[Subscribe to our newsletter](https://groupes.renater.fr/sympa/subscribe/oc-newsletter?previous_action=info)
