La [plateforme OpenCommon](https://opencommon.irit.fr/last/dist/spa/#/) a été initiée par des prototypes de recherche du chantier [DataNoos](https://datanoos.univ-toulouse.fr/)
financé par le RTRA STAE de 2018 à 2021. DataNoos est aujourd'hui une [plateforme d'expertise
sur la valorisation des données interdisciplinaires de l'université de Toulouse](https://mshs.univ-toulouse.fr/plateformes/les-plateformes-dexpertise/datanoos/).

Ces travaux se sont poursuivis notamment par la spécifications de la mise en forme des
données lors du [chantier ENV'IA](https://groupes.renater.fr/wiki/envia/) du RTRA STAE. ENV'IA a pour objectif de connecter les
communautés de l'environnement et de l'IA. Dans la continuité, le projet [ANR Semantics4FAIR](https://anr.fr/Project-ANR-19-DATA-0014) a
étudié les technologies du web sémantique pour mettre en oeuvre des [principes FAIR](https://www.go-fair.org/fair-principles/) pour la
réutilisation des données scientifiques. En l'occurrence, les données de MétéoFrance ont
servi de cas d'application pour les rendre plus facilement réutilisables dans les recherches
scientifiques.

Par la suite, le projet [ANR So-DRIIHM](https://anr.fr/fr/lanr/engagements/la-science-ouverte/les-projets-laureats-de-lappel-flash-science-ouverte/projet-so-driihm/) a eu l'ambition d'offrir aux chercheurs du [labex DRIIHM](https://www.driihm.fr/) une solution pour ses chercheurs et la valorisation de leurs données.

La solution étudiée et développée est celle décrite par le cas d'usage [d'une plateforme
d'intermédiation de la science ouverte pour une communauté interdisciplinaire](https://datanoos.univ-toulouse.fr/fr/use-case-plateforme-science-ouverte) de DataNoos.

Une suite est en cours avec le projet PFR-DRIIHM de France Relance et de l'[université de Paul Sabatier](https://www.univ-tlse3.fr/) ([IRIT](http://www.irit.fr/)) pour continuer les développements de l'actuelle plateforme Open Common.

Le code source de l'application est disponible sur la [forge lociciel de l'IRIT](https://gitlab.irit.fr/so/oc/mvp).

[S'abonner à la lettre d'information](https://groupes.renater.fr/sympa/subscribe/oc-newsletter?previous_action=info)
