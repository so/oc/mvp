import aboutContent from './about/en.md?raw'

// carriage return character
const crEncoded = '%0D%0A'

export default {
  topMenu: {
    contact: {
      subject: "Contact from OpenCommon's welcome",
      body: `Please tell us what is your request:
      Community creation request? Member account creation request?
      Incident reporting? Contribution proposition? A suggestion?
      Best regards, OpenCommon team`
    },
    signIn: 'Sign in'
  },
  home: {
    catchphrase: {
      1: 'Research Data Portal:',
      2: 'Submit, Search and Share'
    },
    goToCommunity: 'Go to community {label}'
  },
  about: {
    title: 'About',
    content: aboutContent
  },
  communities: 'Communities',
  dashboard: 'Dasboard',
  datasetsLabel: 'Datasets',
  distributionsLabel: 'Distributions',
  community: {
    askJoin: {
      buttonLabel: 'Request an access to this community',
      error:
        'An error has occurred. If the problem persists, please contact us (via the envelope icon in the top right-hand corner).',
      confirmation: {
        1: 'Your request has been received and you will be given access to the community within a few days.',
        2: 'In the meantime, you can use the guest mode.'
      }
    },
    homepage: {
      presentationLabel: 'Presentation',
      searchBarPlaceholder: 'Search',
      searchBarLegend: 'Search your scientific resources (datasets, codes, ...)',
      connectForButtonLabel: 'Sign in to',
      connectButtonLabel: 'Sign in to access {communityName} services',
      joinButtonLabel: 'Join the {communityName} community',
      yourServices: 'Your services'
    },
    unfoldTreePannel: 'Unfold tree panel',
    foldTreePannel: 'Fold tree panel',
    addCatalogButtonLabel: 'Add a catalogue'
  },
  backHome: 'Go back to home page',
  or: 'or',
  loading: 'Loading...',
  error: {
    title: 'Error',
    genericMessage: 'Sorry, an error has occurred.',
    notFoundMessage: {
      1: 'Page',
      2: 'does not exist.'
    },
    unauthorizedMessage: 'You are not authorized to access page'
  },
  connection: {
    catchphrase: 'One account, different community data-sharing areas',
    emailAddress: 'E-mail address',
    password: 'Password',
    login: 'Login',
    logout: 'Logout',
    logoutSuccess: 'You have been correctly logout.',
    createAccount: 'Create an account',
    continueAsGuest: 'Continue as a guest',
    alreadyLoggedIn: 'You are already logged in as {login}.',
    errorOnLogin: 'An error has occurred during login. Please check your username and password'
  },
  register: {
    requestAccount: 'Request an account',
    sendRequest: 'Send request',
    selectCommunities: 'Which communities would you like to join?',
    error:
      'An error has occurred. If the problem persists, please contact us (via the envelope icon in the top right-hand corner).',
    confirmation: {
      1: 'Your request has been received and you will be given access within a few days.',
      2: 'In the meantime, you can use the guest mode.'
    }
  },
  profile: {
    profile: 'Profile',
    email: 'Email',
    familyName: 'Family name',
    givenName: 'Given name',
    description: 'Bio',
    homepage: 'Homepage',
    organization: 'Organization',
    topicInterest: 'Topic(s) of interest'
  },
  form: {
    period: {
      start: 'the beginning of the period',
      end: 'the end of the period'
    },
    element: {
      organizationRor: 'Search for an organization in the ROR registry',
      organizationCustom:
        'Enter a custom value if you cannot find your organization in the ROR register'
    },
    validation: {
      mixed: {
        default: '{label} is invalid',
        required: '{label} is a required field',
        notNull: '{label} cannot be null'
      },
      string: {
        url: '{label} must be a valid URL',
        email: '{label} must be a valid email'
      },
      array: {
        min: '{label} must have at least {min} item | {label} must have at least {min} items'
      }
    },
    error: {
      autocomplete: 'An error occurred during the search'
    }
  },
  homeActions: {
    accountNeeded: 'Account needed'
  },
  breadcrumb: {
    signIn: 'Sign in',
    signOut: 'Sign out',
    register: 'Request an account',
    joinCommunity: 'Join a community',
    search: 'Search'
  },
  back: 'Back',
  next: 'Next',
  skip: 'Skip',
  save: 'Save',
  close: 'Close',
  cancel: 'Cancel',
  delete: 'Delete',
  add: 'Add',
  confirmMessage: 'Are you sure you want to proceed?',
  send: 'Send',
  information: 'Information',
  viewCompleteDefinition: 'View complete definition',
  selectForm: 'Select form',
  mandatory: 'Mandatory',
  fair: {
    fair: 'FAIR',
    title: 'FAIR Principles',
    f: 'Findability',
    a: 'Accessibility',
    i: 'Interoperability',
    r: 'Reuse'
  },
  datasets: {
    new: {
      buttonLabel: 'Create new dataset',
      new: 'New',
      datasetDepositWorkflow: 'Dataset deposit workflow',
      recommended: 'Recommended',
      sendingMessage: 'Dataset creation in progress...',
      confirmMessage: 'Your dataset has been correctly created!',
      errorMessage:
        'An error has occurred while creating your dataset. Please try again later or contact the site administrator.',
      seeDatasetPage: 'See the dataset page',
      steps: {
        1: {
          label: 'Describe the dataset',
          description: {
            1: 'The more metadata fields you fill in, the more FAIR the dataset will be!',
            2: 'By default, your dataset will be stored in: Home / My resources / My data'
          }
        },
        2: {
          label: 'Select a catalogue and manage metadata access',
          description: {
            1: "Select a location to store the dataset's metadata.",
            2: 'Select the level of metadata access in the chosen catalogue.'
          },
          access: 'Access rights to the resource',
          accessHelp: {
            1: 'Access rights determine who can view the resource.',
            2: 'Depending on your role within the community and the catalog in which you wish to insert the resource, the available options may vary.',
            3: "If you want your resource to be public, but the option is not available, it's because you don't have sufficient rights to do so.",
            4: "In this case, create your resource as a community resource. Once created, you'll be able to ask a moderator to publish it.",
            5: 'A community resource is only visible to members of the community.',
            6: 'You can choose to keep your resource private, in which case it will only be visible to you. It will then be possible to modify this access from the dashboard.'
          }
        },
        3: {
          label: 'Add Distribution(s)',
          description: {
            1: 'Optional but recommended',
            2: 'A distribution is a physical representation of the dataset or an access link to this physical resource.',
            3: 'Upload your files and describe how they will be stored and publish in a data repository. You can also refer to a data service if the data can be accessed via this channel.'
          },
          noDistributionText:
            'Add a data file (distribution, e.g. spreasheet file) or a data access link (data service, e.g. WMS geoservice feed',
          addDistribution: 'Add distribution'
        },
        4: {
          label: 'Select a data repository',
          description: {
            1: 'This step is optional but compulsory if you have deposited a distribution with a file.',
            2: 'Choose a data repository to publish your dataset and its distributions, and obtain a DOI.'
          }
        },
        5: {
          label: 'Summary and confirmation',
          description: {
            1: 'This stage summaries the informations you have entered and the ones generated by Open Common. You can confirm the creation of the dataset or go back to modify it.',
            2: 'The dataset is in private status, and is sent to a moderator for inclusion in the catalogue you have chosen.',
            3: 'If distributions with files have been added, the dataset and its distributions will be sent to the chosen data repository and contact you if any changes need to be made.',
            4: 'You can check the status of your dataset at any time from your dashboard.'
          }
        }
      }
    },
    edit: {
      edit: 'Edit',
      savingMessage: 'Dataset update in progress...',
      confirmMessage: 'Your dataset has been correctly updated!',
      errorMessage:
        'An error has occurred while updating your dataset. Please try again later or contact the site administrator.',
      seeDatasetPage: 'Go back to the dataset page'
    },
    dashboard: {
      title: 'Manage my datasets',
      noDistribution: 'This dataset has no distribution yet.'
    }
  },
  datasetStatus: {
    COMPLETED: 'Completed',
    DEPRECATED: 'Deprecated',
    DEVELOP: 'In development',
    DISCONT: 'Discontinued',
    OP_DATPRO: '??',
    WITHDRAWN: 'Withdrawn'
  },
  descriptors: {
    default: {
      noTitle: 'No title for this resource',
      noDescription: 'No description for this resource',
      noTheme: 'No theme for this resource',
      noKeyword: 'No keyword for this resource',
      noType: 'No type for this resource'
    },
    toolbar: {
      share: 'Share',
      update: 'Update',
      remove: 'Remove',
      report: 'Report',
      contact: 'Contact'
    },
    distribution: {
      access: 'Access and use'
    },
    dataset: {
      keyword: 'Keywords',
      description: 'Description',
      title: 'Title',
      identifier: 'Identifier',
      version: 'Version',
      temporal: 'Temporal coverage',
      spatial: 'Spatial coverage',
      contactPoint: 'Contact(s)',
      quality: {
        title: 'Quality information',
        provenance: 'Provenance',
        input: 'Input data',
        other: 'Others resources'
      },
      theme: 'Theme(s)',
      distribution: {
        title: 'Distributions',
        license: 'License',
        desciptorLink: 'Access to the distribution detail',
        accessURL: 'Access to the file repository'
      },
      creator: 'Creator(s)',
      technical: {
        title: 'Technical informations',
        property: 'Property',
        value: 'Value'
      }
    }
  },
  noTranslationAvailable: 'No translation available',
  conceptAutocomplete: {
    search: 'Search for a term',
    browseTerms: 'Browse terms',
    vocabularies: 'Vocabularies',
    selectedTerms: 'Selected terms'
  },
  resourceVisibility: {
    public: 'Public resource',
    community: 'Community resource',
    private: 'Private resource',
    protected: 'Protected resource'
  },
  emails: {
    share: {
      subject: `[SO DRIIHM] Share of the {resourceType} : {resourceTitle}`,
      body: `
Hello, ${crEncoded}
${crEncoded}
I would like to share with you the {resourceType} whose title is : ${crEncoded}
{resourceTitle}${crEncoded}
${crEncoded}
You can access through the following link : {resourceLink}${crEncoded}
${crEncoded}
Greetings,
      `
    },
    ask: {
      subject: `[SO DRIIHM] Ask for contact for the {resourceType} : {resourceTitle}`,
      body: `
Hello, ${crEncoded}
${crEncoded}
I would like to ask contact with you for the {resourceType} whose title is : ${crEncoded}
{resourceTitle}${crEncoded}
${crEncoded}
and can be found by following the link : {resourceLink}${crEncoded}
${crEncoded}
Greetings,
      `
    }
  },
  catalog: {
    new: {
      new: 'New catalogue',
      newsubcatalog: 'Create a sub-catalogue',
      sendingMessage: 'Catalogue creation in progress...',
      confirmMessage: 'Your catalogue has been correctly created!',
      errorMessage:
        'An error has occurred while creating your catalogue. Please try again later or contact the site administrator.',
      seeCatalogPage: 'See the catalogue page',
      access: 'Access rights to the resource',
      accessHelp: {
        1: "Access rights determine who can view the resource.",
        2: "Depending on your role within the community and the catalog in which you wish to insert the resource, the available options may vary.",
        3: "If you want your resource to be public but the option is not available, it's because you don't have sufficient rights to do so.",
        4: "In this case, create your resource as a community resource. Once created, you'll be able to ask a moderator to publish it.",
        5: "A community resource is only visible to members of the community.",
        6: "You can choose to keep your resource private, in which case it will only be visible to you. It will then be possible to modify this access from the dashboard."
      }
    },
    edit: {
      edit: 'Edit',
      savingMessage: 'Catalog update in progress...',
      confirmMessage: 'Your catalog has been correctly updated!',
      errorMessage:
        'An error has occurred while updating your catalog. Please try again later or contact the site administrator.',
      seeCatalogPage: 'Go back to the catalog page'
    }
  },
  search: {
    title: 'Search',
    resourceType: 'Resource type',
    results: 'Results',
    launchSearch: 'Launch the search',
    searchBarParamButtonLabel: 'Parameters',
    searchResult: {
      description: 'Description',
      datasets: 'Datasets',
      distributions: 'Distributions',
      parentCatalog: 'Parent catalogue',
      parentCatalogs: 'Parent catalogues',
      id: 'Id',
      version: 'Version'
    }
  },
  resourceType: {
    catalog: 'catalogue',
    dataset: 'dataset',
    distribution: 'distribution',
    service: 'service',
    concept: 'concept',
    unknown: 'unknown'
  },
  and: 'and'
}
