import aboutContent from './about/fr.md?raw'

// carriage return character
const crEncoded = '%0D%0A'

export default {
  topMenu: {
    contact: {
      subject: 'Contact depuis la page de bienvenue OpenCommon',
      body: `Merci de préciser votre demande :
      Demande de création d'un espace communautaire ?
      Demande de création de compte ? Rapport d'incident ?
      Proposition de contribution ? Suggestion ?
      Cordialement, l'équipe OpenCommon`
    },
    signIn: 'Se connecter'
  },
  home: {
    catchphrase: {
      1: 'Portail pour les données de la recherche :',
      2: 'Déposer, Rechercher et Partager'
    },
    goToCommunity: 'Voir la communauté {label}'
  },
  about: {
    title: 'À propos',
    content: aboutContent,
  },
  communities: 'Communautés',
  dashboard: 'Dasboard',
  datasetsLabel: 'Datasets',
  distributionsLabel: 'Distributions',
  community: {
    askJoin: {
      buttonLabel: 'Demander l’accès à cette communauté',
      error:
        "Une erreur est survenue. Si le problème persiste, contactez-nous (via l'icone enveloppe en haut à droite).",
      confirmation: {
        1: 'Votre demande a été prise en compte, vous recevrez sous quelques jours une autorisation d’accès à la communauté.',
        2: 'En attendant vous pouvez utiliser le mode visteur.'
      }
    },
    homepage: {
      presentationLabel: 'Présentation',
      searchBarPlaceholder: 'Rechercher',
      searchBarLegend: 'Rechercher vos ressources scientifiques (jeux de données, codes, ...)',
      connectButtonLabel: 'Se connecter pour accéder aux services de la {communityName}',
      joinButtonLabel: 'Rejoindre la communauté {communityName}',
      yourServices: 'Vos services'
    },
    unfoldTreePannel: "Déplier le panneau de l'arborescence",
    foldTreePannel: "Plier le panneau de l'arborescence",
    addCatalogButtonLabel: 'Ajouter un catalogue'
  },
  backHome: "Retourner à la page d'accueil",
  or: 'ou',
  loading: 'Chargement en cours...',
  error: {
    title: 'Erreur',
    genericMessage: 'Désolé, une erreur est survenue.',
    notFoundMessage: {
      1: 'La page',
      2: "n'existe pas."
    },
    unauthorizedMessage: "Vous n 'êtes pas autorisé à accéder à la page"
  },
  connection: {
    catchphrase: 'Un seul compte, différents espaces communautaires de partage de données',
    emailAddress: 'Adresse e-mail',
    password: 'Mot de passe',
    login: 'Connexion',
    logout: 'Déconnexion',
    logoutSuccess: 'Vous avez été correctement déconnecté.',
    createAccount: 'Créer un compte',
    continueAsGuest: 'Poursuivre en mode visiteur',
    alreadyLoggedIn: 'Vous êtes déjà connecté en tant que {login}.',
    errorOnLogin:
      "Une erreur est survenue à la connexion. Veuillez vérifier votre nom d'utilisateur et votre mot de passe."
  },
  register: {
    requestAccount: "Demander la création d'un compte",
    sendRequest: 'Envoyer la demande',
    selectCommunities: 'Quelles communautés souhaitez-vous rejoindre ?',
    error:
      "Une erreur est survenue. Si le problème persiste, contactez-nous (via l'icone enveloppe en haut à droite).",
    confirmation: {
      1: 'Votre demande a été prise en compte, vous recevrez sous quelques jours une autorisation d’accès.',
      2: 'En attendant vous pouvez utiliser le mode visteur.'
    }
  },
  profile: {
    profile: 'Profil',
    email: 'E-mail',
    familyName: 'Nom',
    givenName: 'Prénom',
    description: 'Bio',
    homepage: 'Site internet',
    organization: 'Organisation',
    topicInterest: "Centre(s) d'intérêt"
  },
  form: {
    period: {
      start: 'le début de la période',
      end: 'la fin de la période'
    },
    element: {
      organizationRor: 'Cherchez une organisation à partir du registre ROR',
      organizationCustom:
        'Rentrez une autre valeur si vous ne trouvez pas votre organisation dans le registre ROR'
    },
    validation: {
      mixed: {
        default: '{label} est invalide',
        required: '{label} est un champ obligatoire',
        notNull: '{label} ne peut être vide'
      },
      string: {
        url: '{label} doit être une URL valide',
        email: '{label} doit être une adresse e-mail valide'
      },
      array: {
        min: '{label} doit au moins avoir {min} valeur | {label} doit au moins avoir {min} valeurs'
      }
    },
    error: {
      autocomplete: "Une erreur s'est produite lors de la recherche."
    }
  },
  homeActions: {
    accountNeeded: 'Compte nécessaire'
  },
  breadcrumb: {
    signIn: 'Se connecter',
    signOut: 'Déconnexion',
    register: "Demander la création d'un compte",
    joinCommunity: 'Rejoindre une communauté',
    search: 'Rechercher'
  },
  back: 'Précédent',
  next: 'Suivant',
  skip: 'Passer',
  save: 'Enregistrer',
  close: 'Fermer',
  cancel: 'Annuler',
  delete: 'Supprimer',
  add: 'Ajouter',
  confirmMessage: 'Êtes-vous sûr de vouloir continuer ?',
  send: 'Envoyer',
  information: 'Information',
  viewCompleteDefinition: 'Voir la définition complète',
  selectForm: 'Choisir la fiche',
  mandatory: 'Obligatoire',
  fair: {
    fair: 'FAIR',
    title: 'Les principes FAIR',
    f: 'Capacité à être trouvé (Findability)',
    a: 'Accessibilité (Accessibility)',
    i: 'Interopérabilité (Interoperability)',
    r: 'Réutilisation (Reuse)'
  },
  datasets: {
    new: {
      buttonLabel: 'Créer un nouveau jeu de données',
      new: 'Nouveau',
      datasetDepositWorkflow: "Processus de dépôt d'un jeu de données",
      recommended: 'Recommandé',
      sendingMessage: 'Jeu de données en cours de création...',
      confirmMessage: 'Votre jeu de données a été créé correctement !',
      errorMessage:
        "Une erreur est survenue lors de la création de votre jeu de données. Merci de réessayer plus tard ou bien de contacter l'administrateur du site.",
      seeDatasetPage: 'Voir la page du jeu de données',
      steps: {
        1: {
          label: 'Décrire le jeu de données',
          description: {
            1: 'Plus vous complétez les métadonnées, plus le jeu de données sera FAIR !',
            2: 'Par défaut, les métadonnées de votre jeu de données seront stockées dans : Accueil / Mes ressources / Mes données'
          }
        },
        2: {
          label: "Sélectionner un catalogue et gérer l'accès aux métadonnées",
          description: {
            1: 'Sélectionner un endroit où stocker les métadonnées du jeu de données.',
            2: "Sélectionner le niveau d'accès aux métadonnées dans le catalogue choisi."
          },
          access: "Droit d'accès de la ressource",
          accessHelp: {
            1: "Les droits d'accès déterminent qui aura le droit de voir la ressource.",
            2: 'En fonction de votre rôle au sein de la communauté et du catalogue dans lequel vous souhaitez insérer la ressource, les options disponibles peuvent varier.',
            3: "Si vous souhaitez qui votre ressource soit publique mais que l'option n'est pas disponible, c'est que vous n'avez pas les droits suffisants pour le faire.",
            4: 'Dans ce cas, créez votre jeu de données en tant que ressource communautaire et une fois votre ressource créée, vous aurez la possibilité de demander sa publication à un modérateur.',
            5: "Une ressource communautaire n'est visible que des membre de la communauté.",
            6: 'Vous pouvez choisir de garder votre ressource privée, dans ce cas, elle ne sera visible que par vous. Il sera ensuite possible de modifier accès à partir du tableau de bord.'
          }
        },
        3: {
          label: 'Ajouter une ou des distributions',
          description: {
            1: 'Étape facultative mais recommandée.',
            2: "Une distribution est une représentation physique du jeu de données ou un lien d'accès vers cette ressource physique.",
            3: 'Déposez vos fichiers et décrivez-les pour les stocker et les publier dans un entrepôt de données. Vous pouvez également faire référence à un service de données si les données sont accessibles par ce biais.'
          },
          noDistributionText:
            "Ajouter un fichier de données (distribution, par exemple un fichier tableur) ou un lien d'accès aux données (service de données, par exemple un flux de géoservices WMS).",
          addDistribution: 'Ajouter une distribution'
        },
        4: {
          label: 'Sélectionner un entrepôt de données',
          description: {
            1: 'Étape facultative, elle est obligatoire si vous avez déposé une distribution avec fichier.',
            2: 'Choisissez un entrepôt de données pour publier votre jeu de données et ses distributions, et obtenez ainsi un DOI.'
          }
        },
        5: {
          label: 'Résumé et confirmation',
          description: {
            1: 'Cette étape résume les informations que vous avez saisies et celles générées par Open Common. Vous pouvez confirmer la création du jeu de données ou revenir en arrière pour le modifier.',
            2: 'Le jeu de données est en statut privé, il est envoyé à un modérateur pour être intégré dans le catalogue que vous avez choisi.',
            3: "Si des distributions avec des fichiers ont été ajoutées, le jeu de données et ses distributions seront envoyés à l'entrepôt de données choisi. Ce dernier pourra modérer votre dépôt et vous contacter en cas de modifications nécessaires.",
            4: "Vous pouvez à tout moment vérifier l'état de votre jeu de données à partir de votre tableau de bord."
          }
        }
      }
    },
    edit: {
      edit: 'Modifier',
      savingMessage: 'Mise à jour du jeu de données en cours...',
      confirmMessage: 'Votre jeu de données a été correctement mis à jour !',
      errorMessage:
        "Une erreur est survenue lors de la mise à jour de votre jeu de données. Merci de réessayer plus tard ou bien de contacter l'administrateur du site.",
      seeDatasetPage: 'Retourner à la page du jeu de données'
    },
    dashboard: {
      title: 'Gérer mes jeux de données',
      noDistribution: "Ce jeu de données n'a pas encore de distribution."
    }
  },
  datasetStatus: {
    COMPLETED: 'Complet',
    DEPRECATED: 'Déprécié',
    DEVELOP: 'En cours',
    DISCONT: 'Discontinué',
    OP_DATPRO: '??',
    WITHDRAWN: 'Brouillon'
  },
  descriptors: {
    default: {
      noTitle: 'Pas de titre pour cette ressouce',
      noDescription: 'Pas de description pour cette ressource',
      noTheme: 'Pas de thème pour cette ressource',
      noKeyword: 'Pas de mots clefs pour cette ressource',
      noType: 'Pas de type pour cette ressource'
    },
    toolbar: {
      share: 'Partager',
      update: 'Modifier',
      remove: 'Supprimer',
      report: 'Signaler',
      contact: 'Contacter'
    },
    distribution: {
      access: 'Accès et usage'
    },
    dataset: {
      keyword: 'Mots clefs',
      description: 'Description',
      title: 'Titre',
      identifier: 'Code identification',
      version: 'Version',
      temporal: 'Emprise temporelle',
      spatial: 'Emprise spatiale',
      contactPoint: 'Contact(s)',
      quality: {
        title: 'Informations qualitatives',
        provenance: 'Provenance',
        input: "Données d'entrée",
        other: 'Autres ressources'
      },
      theme: 'Thèmes(s)',
      distribution: {
        title: 'Distributions',
        license: 'Licence',
        desciptorLink: 'Voir la fiche détaillée',
        accessURL: 'Accès au dépôt du fichier'
      },
      creator: 'Auteur(s)',
      technical: {
        title: 'Informations techniques',
        property: 'Propriété',
        value: 'Valeur'
      }
    }
  },
  noTranslationAvailable: 'Pas de traduction disponible',
  conceptAutocomplete: {
    search: 'Rechercher un terme',
    browseTerms: 'Parcourir les termes',
    vocabularies: 'Vocabulaires',
    selectedTerms: 'Termes sélectionnés'
  },
  resourceVisibility: {
    public: 'Ressource publique',
    community: 'Ressource communautaire',
    private: 'Ressource privée',
    protected: 'Ressource protégée'
  },
  emails: {
    share: {
      subject: ({ linked, named }) =>
        `[SO DRIIHM] Partage ${linked('emails.resourceTypeSubject.' + named('resourceType'))} : ${named('resourceTitle')}`,
      body: ({ linked, named }) => `
Bonjour, ${crEncoded}
${crEncoded}
Je souhaite vous partager ${linked('emails.resourceTypeBody.' + named('resourceType'))} dont le titre est : ${crEncoded}
${named('resourceTitle')}${crEncoded}
${crEncoded}
Vous pouvez y accéder en suivant le lien suivant : ${named('resourceLink')}${crEncoded}
${crEncoded}
Cordialement,
      `
    },
    ask: {
      subject: ({ linked, named }) =>
        `[SO DRIIHM] Prise de contact pour ${linked('emails.resourceTypeBody.' + named('resourceType'))} : ${named('resourceTitle')}`,
      body: ({ linked, named }) => `
Bonjour, ${crEncoded}
${crEncoded}
Je souhaite prendre contact avec vous concernant ${linked('emails.resourceTypeBody.' + named('resourceType'))} dont le titre est : ${crEncoded}
${named('resourceTitle')}${crEncoded}
${crEncoded}
et est accessible en suivant le lien suivant : ${named('resourceLink')}${crEncoded}
${crEncoded}
Cordialement,
      `
    },
    resourceTypeSubject: {
      dataset: 'du dataset',
      catalog: 'du catalogue',
      service: 'du service',
      distribution: 'de la distribution'
    },
    resourceTypeBody: {
      dataset: 'le dataset',
      catalog: 'le catalogue',
      service: 'le service',
      distribution: 'la distribution'
    }
  },
  catalog: {
    new: {
      new: 'Nouveau catalogue',
      newsubcatalog: 'Créer un sous-catalogue',
      sendingMessage: 'Catalogue en cours de création...',
      confirmMessage: 'Votre catalogue a été créé correctement !',
      errorMessage:
        "Une erreur est survenue lors de la création de votre catalogue. Merci de réessayer plus tard ou bien de contacter l'administrateur du site.",
      seeCatalogPage: 'Voir la page du catalogue',
      access: "Droit d'accès de la ressource",
      accessHelp: {
        1: "Les droits d'accès déterminent qui aura le droit de voir la ressource.",
        2: "En fonction de votre rôle au sein de la communauté et du catalogue dans lequel vous souhaitez insérer la ressource, les options disponibles peuvent varier.",
        3: "Si vous souhaitez qui votre ressource soit publique mais que l'option n'est pas disponible, c'est que vous n'avez pas les droits suffisants pour le faire.",
        4: "Dans ce cas, créez votre jeu de données en tant que ressource communautaire et une fois votre ressource créée, vous aurez la possibilité de demander sa publication à un modérateur.",
        5: "Une ressource communautaire n'est visible que des membres de la communauté.",
        6: "Vous pouvez choisir de garder votre ressource privée, dans ce cas, elle ne sera visible que par vous. Il sera ensuite possible de modifier l'accès à partir du tableau de bord."
      }
    },
    edit: {
      edit: 'Modifier',
      savingMessage: 'Mise à jour du catalogue en cours...',
      confirmMessage: 'Votre catalogue a été correctement mis à jour !',
      errorMessage:
        "Une erreur est survenue lors de la mise à jour de votre catalogue. Merci de réessayer plus tard ou bien de contacter l'administrateur du site.",
      seeCatalogPage: 'Retourner à la page du catalogue'
    }
  },
  search: {
    title: 'Rechercher',
    resourceType: 'Type de ressource',
    results: 'Résultats',
    launchSearch: 'Lancer la recherche',
    searchBarParamButtonLabel: 'Paramètres',
    searchResult: {
      description: 'Description',
      catalogues: 'Sous-catalogues',
      datasets: 'Jeux de données',
      distributions: 'Distributions',
      parentCatalog: 'Catalogue parent',
      parentCatalogs: 'Catalogues parent',
      id: 'Id',
      version: 'Version'
    }
  },
  resourceType: {
    catalog: 'catalogue',
    dataset: 'jeu de données',
    distribution: 'distribution',
    service: 'service',
    concept: 'concept',
    unknown: 'inconnu'
  },
  and: 'et'
}
