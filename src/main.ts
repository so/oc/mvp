import './assets/main.css'
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import router from '@/router'
import i18n from '@/i18n'
import primeVueConfig from '@/primevue.config'
import PrimeVue from 'primevue/config'
import App from './App.vue'
import { DataLoaderPlugin } from 'unplugin-vue-router/data-loaders'
import ConfirmationService from 'primevue/confirmationservice'
import { abilitiesPlugin } from '@casl/vue'
import ability from '@/ability'

const app = createApp(App)

app.use(createPinia())
app.use(DataLoaderPlugin, { router })
app.use(router)
app.use(i18n)
app.use(PrimeVue, primeVueConfig)
app.use(ConfirmationService)
app.use(abilitiesPlugin, ability)

app.mount('#app')
