import type { OcCatalog, OcModelMetadata } from '@/declarations'
import { datasetMetadata } from './dataset'

export const catalogMetadata: OcModelMetadata<OcCatalog> = {
  ...datasetMetadata,
  identifier: {
    ...datasetMetadata.identifier,
    dereferencement: 'https://ec-jrc.github.io/dcat-ap-jrc/#catalogue-identifier'
  },
  title: {
    ...datasetMetadata.title,
    dereferencement: 'https://ec-jrc.github.io/dcat-ap-jrc/#catalogue-title',
    desc: {
      en: 'The name / title of the catalogue',
      fr: 'Le nom / titre du catalogue'
    }
  },
  description: {
    ...datasetMetadata.description,
    dereferencement: 'https://ec-jrc.github.io/dcat-ap-jrc/#catalogue-description',
    desc: {
      en: 'The description of the catalogue',
      fr: 'La description du catalogue'
    }
  },
  contactPoint: {
    ...datasetMetadata.contactPoint,
    dereferencement: 'https://ec-jrc.github.io/dcat-ap-jrc/#catalogue-contact-point',
    desc: {
      en: 'The contact point for the catalogue',
      fr: 'Le point de contact du catalogue'
    }
  },
  publisher: {
    ...datasetMetadata.publisher,
    dereferencement: 'https://ec-jrc.github.io/dcat-ap-jrc/#catalogue-publisher',
    desc: {
      en: 'The organisation responsible for the publication of the catalogue',
      fr: "L'organisation responsable de la publication du catalogue"
    }
  },
  theme: {
    ...datasetMetadata.theme,
    dereferencement: 'https://semiceu.github.io/DCAT-AP/releases/3.0.0/#Catalogue.themes',
    required: false,
    desc: {
      en: 'A main category of the catalogue. A catalogue can have multiple themes.',
      fr: 'Catégorie principale du catalogue. Un catalogue peut avoir plusieurs thèmes.'
    }
  },
  issued: {
    ...datasetMetadata.issued,
    required: false,
    desc: {
      en: 'Date of formal issuance (e.g., publication) of the catalogue.',
      fr: "Date d'émission officielle (par exemple, publication) de la ressource."
    },
    dereferencement: 'https://semiceu.github.io/DCAT-AP/releases/3.0.0/#Catalogue.releasedate'
  },
  temporal: {
    ...datasetMetadata.temporal,
    desc: {
      en: 'The temporal period that the catalogue and its contents covers.',
      fr: 'Couverture temporelle du catalogue et de ses contenus.'
    },
    dereferencement: 'https://semiceu.github.io/DCAT-AP/releases/3.0.0/#Catalogue.temporalcoverage'
  },
  spatial: {
    ...datasetMetadata.spatial,
    desc: {
      en: 'The geographical area covered by the catalogue and its contents.',
      fr: 'Emprise spatiale/géographique du catalogue et de ses contenus.'
    },
    dereferencement:
      'https://semiceu.github.io/DCAT-AP/releases/3.0.0/#Catalogue.geographicalcoverage',
    comment: {
      en: 'The spatial coverage of a catalogue may be encoded as an instance of dcterms:Location, or may be indicated using an IRI reference (link) to a resource describing a location. It is recommended that links are to entries in a well maintained gazetteer such as Geonames.',
      fr: "La couverture spatiale d'un catalogue peut être encodée comme une instance de dcterms:Location, ou peut être indiquée en utilisant une référence IRI (lien) vers une ressource décrivant une localisation. Il est recommandé que les liens renvoient à des entrées d'un répertoire toponymique bien tenu, tel que Geonames."
    }
  },
  parentCatalog: {
    label: {
      en: 'Parent catalogue',
      fr: 'Catalogue parent'
    },
    required: true,
    desc: {
      en: 'The parent catalogue of this catalogue',
      fr: 'Le catalogue parent de ce catalogue'
    },
    fair: ['f']
  },
  catalogs: {
    label: {
      en: 'Sub catalogs',
      fr: 'Sous catalogues'
    }
  },
  datasets: {
    label: {
      en: 'Datasets',
      fr: 'Jeux de données'
    }
  }
}
