import type { OcDataset, OcModelMetadata } from '@/declarations'
import { resourceMetadata } from './resource'

export const datasetMetadata: OcModelMetadata<OcDataset> = {
  ...resourceMetadata,
  title: {
    ...resourceMetadata.title,
    desc: {
      en: 'The name / title of the dataset',
      fr: 'Le nom / titre du jeu de données'
    }
  },
  description: {
    ...resourceMetadata.description,
    desc: {
      en: 'The description of the dataset',
      fr: 'La description du jeu de données'
    }
  },
  contactPoint: {
    propertyUri: 'http://www.w3.org/ns/dcat#contactPoint',
    dereferencement: 'https://www.w3.org/TR/vocab-dcat-3/#Property:resource_contact_point',
    label: {
      en: 'Contact Point',
      fr: 'Point de contact'
    },
    required: true,
    desc: {
      en: 'The contact point for the dataset',
      fr: 'Le point de contact du dataset'
    },
    fair: ['f', 'a', 'r']
  },
  publisher: {
    propertyUri: 'http://purl.org/dc/terms/publisher',
    dereferencement: 'https://www.w3.org/TR/vocab-dcat-3/#Property:resource_publisher',
    label: {
      en: 'Publisher',
      fr: 'Éditeur'
    },
    required: true,
    desc: {
      en: 'The organisation responsible for the publication of the dataset',
      fr: "L'organisation responsable de la publication du jeu de données"
    },
    fair: ['f', 'a']
  },
  accrualPeriodicity: {
    propertyUri: 'http://purl.org/dc/terms/accrualPeriodicity',
    dereferencement: 'https://www.w3.org/TR/vocab-dcat/#Property:dataset_frequency',
    label: {
      en: 'Update frequency',
      fr: 'Fréquence de mise à jour'
    },
    required: true,
    vocabularies: ['http://publications.europa.eu/resource/authority/frequency'],
    desc: {
      en: 'The frequency at which a dataset is published.',
      fr: "La fréquence de publication d'un ensemble de données."
    },
    fair: ['f']
  },
  theme: {
    propertyUri: 'https://www.w3.org/ns/dcat#theme',
    dereferencement: 'https://www.w3.org/TR/vocab-dcat-3/#Property:resource_theme',
    label: {
      en: 'Theme',
      fr: 'Thème'
    },
    required: true,
    desc: {
      en: 'A main category of the dataset. A dataset can have multiple themes.',
      fr: 'Catégorie principale du jeu de donnée. Un jeu de données peut avoir plusieurs thèmes.'
    },
    fair: ['f', 'i'],
    vocabularies: ['http://publications.europa.eu/resource/authority/data-theme'],
    context: {
      '@container': '@set'
    }
  },
  keyword: {
    label: {
      en: 'Keyword(s)',
      fr: 'Mot(s)-clé(s)'
    },
    required: false,
    propertyUri: 'http://www.w3.org/ns/dcat#keyword',
    dereferencement: 'https://www.w3.org/TR/vocab-dcat-3/#Property:resource_keyword',
    desc: {
      en: 'A keyword or tag describing the dataset.',
      fr: 'Un mot-clé ou tag décrivant le jeu de données.'
    },
    fair: ['f', 'i'],
    comment: {
      en: 'The expected value for keyword is a Literal',
      fr: 'La valeur attendue du mot-clé est une valeur littérale.'
    },
    context: {
      '@container': '@language'
    }
  },
  creator: {
    label: {
      en: 'Creator(s)',
      fr: 'Créateur(s)'
    },
    desc: {
      en: 'An individual/organization who contributed in the creation of the resource.',
      fr: 'Une personne ou une organisation qui a contribué à la création de la ressource.'
    },
    propertyUri: 'http://purl.org/dc/terms/creator',
    required: true,
    dereferencement: 'https://www.w3.org/TR/vocab-dcat-3/#Property:resource_creator',
    fair: ['f', 'r'],
    comment: {
      en: "If, and only if, the creator does not appear in your search list, you can create it by clicking on the 'Add agent' button. Please note that once it has been created, it cannot be modified or deleted.",
      fr: "Si, et seulement si, le créateur n'apparaît pas dans votre liste de recherche, vous pouvez le créer en cliquant sur le bouton 'Ajouter un agent'. Veuillez noter qu'une fois créé, il ne peut être ni modifié ni supprimé."
    },
    context: {
      '@container': '@set'
    }
  },
  type: {
    label: {
      en: 'Resource type',
      fr: 'Type de ressource'
    },
    required: true,
    desc: {
      en: 'The nature or genre of the resource.',
      fr: 'Nature ou genre de la ressource.'
    },
    propertyUri: 'http://purl.org/dc/terms/type',
    dereferencement: 'https://www.w3.org/TR/vocab-dcat-3/#Property:resource_type',
    fair: ['f', 'i', 'r'],
    comment: {
      en: 'The value SHOULD be taken from a well governed and broadly recognised controlled vocabulary.',
      fr: "La valeur DEVRAIT être tirée d'un vocabulaire contrôlé bien géré et largement reconnu."
    },
    vocabularies: ['http://publications.europa.eu/resource/authority/dataset-type']
  },
  status: {
    label: {
      en: 'Status',
      fr: 'Statut'
    },
    required: false,
    desc: {
      en: 'The lifecycle status of the dataset.',
      fr: "L'état du cycle de vie du jeu de données."
    },
    propertyUri: 'http://www.w3.org/ns/adms#status',
    dereferencement: 'https://www.w3.org/TR/vocab-adms/#adms_status',
    fair: [],
    vocabularies: ['http://publications.europa.eu/resource/authority/dataset-status']
  },
  modified: {
    label: {
      en: 'Update/modification date',
      fr: 'Date de mise à jour'
    },
    required: false,
    desc: {
      en: 'Most recent date on which the resource was changed, updated or modified.',
      fr: 'Date la plus récente à laquelle la ressource a été mise à jour, changée ou modifiée.'
    },
    propertyUri: 'http://purl.org/dc/terms/modified',
    dereferencement: 'https://www.w3.org/TR/vocab-dcat-3/#Property:resource_update_date',
    fair: ['a'],
    comment: {
      en: 'The value of this property indicates a change to the actual resource, not a change to the catalog record. An absent value MAY indicate that the resource has never changed after its initial publication, or that the date of last modification is not known, or that the resource is continuously updated.',
      fr: "La valeur de cette propriété indique une modification de la ressource réelle, et non une modification de la notice de catalogue. Une valeur absente PEUT indiquer que la ressource n'a jamais été modifiée après sa publication initiale, que la date de la dernière modification n'est pas connue ou que la ressource est continuellement mise à jour."
    }
  },
  temporal: {
    label: {
      en: 'Temporal coverage',
      fr: 'Couverture temporelle'
    },
    required: false,
    desc: {
      en: 'The temporal period that the dataset covers.',
      fr: 'Couverture temporelle du jeu de données.'
    },
    propertyUri: 'http://purl.org/dc/terms/temporal',
    dereferencement: 'https://www.w3.org/TR/vocab-dcat-3/#Property:dataset_temporal',
    fair: ['f'],
    comment: {
      en: 'The temporal coverage of a dataset may be encoded as an instance of dcterms:PeriodOfTime, or may be indicated using an IRI reference (link) to a resource describing a time period or interval.',
      fr: "La couverture temporelle d'un ensemble de données peut être encodée comme une instance de dcterms:PeriodOfTime, ou peut être indiquée en utilisant une référence IRI (lien) vers une ressource décrivant une période ou un intervalle de temps."
    }
  },
  spatial: {
    label: {
      en: 'Spatial/geographical coverage',
      fr: 'Couverture spatiale'
    },
    required: false,
    desc: {
      en: 'The geographical area covered by the dataset.',
      fr: 'Emprise spatiale/géographique du jeu de données.'
    },
    propertyUri: 'http://purl.org/dc/terms/spatial',
    dereferencement: 'https://www.w3.org/TR/vocab-dcat-3/#Property:dataset_spatial',
    fair: ['f'],
    vocabularies: [
      'http://publications.europa.eu/resource/authority/continent',
      'http://publications.europa.eu/resource/authority/country'
    ],
    comment: {
      en: 'The spatial coverage of a dataset may be encoded as an instance of dcterms:Location, or may be indicated using an IRI reference (link) to a resource describing a location. It is recommended that links are to entries in a well maintained gazetteer such as Geonames.',
      fr: "La couverture spatiale d'un jeu de données peut être encodée comme une instance de dcterms:Location, ou peut être indiquée en utilisant une référence IRI (lien) vers une ressource décrivant une localisation. Il est recommandé que les liens renvoient à des entrées d'un répertoire toponymique bien tenu, tel que Geonames."
    }
  },
  landingPage: {
    label: {
      en: 'Landing page',
      fr: "Page d'accueil"
    },
    required: false,
    desc: {
      en: 'A Web page that can be navigated to in a Web browser to gain access to the catalog, a dataset, its distributions and/or additional information.',
      fr: 'Une page Web à laquelle on peut accéder dans un navigateur Web pour accéder au catalogue, aux jeux de données, à ses distributions et/ou à des informations supplémentaires.'
    },
    propertyUri: 'https://www.w3.org/ns/dcat#landingPage',
    dereferencement: 'https://www.w3.org/TR/vocab-dcat-3/#Property:resource_landing_page',
    fair: ['f', 'a', 'i', 'r'],
    comment: {
      en: 'Typically, the landing page is specified only with its URI.',
      fr: 'Généralement, la page de destination est spécifiée uniquement par son URI.'
    }
  },
  language: {
    label: {
      en: 'Language',
      fr: 'Language'
    },
    required: true,
    desc: {
      en: 'A language of the resource. This refers to the natural language used for textual metadata (i.e., titles, descriptions, etc.) of a cataloged resource (i.e., dataset or service) or the textual values of a dataset distribution',
      fr: "Langue de la ressource. Il s'agit de la langue naturelle utilisée pour les métadonnées textuelles (c'est-à-dire les titres, les descriptions, etc.) d'une ressource cataloguée (c'est-à-dire un ensemble de données) ou les valeurs textuelles d'une distribution d'ensembles de données."
    },
    propertyUri: 'http://purl.org/dc/terms/language',
    dereferencement: 'https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#language',
    fair: ['a'],
    vocabularies: ['http://publications.europa.eu/resource/authority/language'],
    comment: {
      en: 'Repeat this property if the resource is available in multiple languages.',
      fr: 'Répéter cette propriété si la ressource est disponible en plusieurs langues.'
    }
  },
  version: {
    label: {
      en: 'Version',
      fr: 'Version'
    },
    required: false,
    desc: {
      en: 'The version indicator (name or identifier) of a resource.',
      fr: "L'indicateur de version (nom ou identifiant) d'une ressource."
    },
    propertyUri: 'https://www.w3.org/ns/dcat#version',
    dereferencement: 'https://www.w3.org/TR/vocab-dcat/#Property:resource_version',
    fair: [],
    comment: {
      en: "DCAT does not prescribe how a version name / identifier should be specified, and refers for guidance to [DWBP]'s Best Practice 7: Provide a version indicator. Ex: 3.4.5: 3.4 is the version number, 5 is the revision number",
      fr: 'Le DCAT ne prescrit pas la manière dont un nom de version ou un identifiant doit être spécifié, et renvoie à la meilleure pratique 7 du [DWBP] : Fournir un indicateur de version. Ex: 3.4.5 : 3.4 est le numéro de version, 5 est la révision'
    }
  },
  catalog: {
    label: {
      en: 'Catalog',
      fr: 'Catalogue'
    },
    required: true,
    desc: {
      en: 'The catalogue where the dataset is documented.',
      fr: 'Le catalogue dans lequel est référencé le jeu de données'
    },
    fair: ['f']
  },

  distribution: {
    label: {
      en: 'Distribution',
      fr: 'Distribution'
    },
    desc: {
      en: 'A physical embodiment of the Dataset in a particular format.',
      fr: "Une version physique de l'ensemble de données dans un format particulier."
    },
    fair: ['f', 'a', 'i', 'r']
  },
  isPartOf: {
    propertyUri: 'http://purl.org/dc/terms/isPartOf',
    label: {
      en: 'Is part of',
      fr: 'Membre de'
    },
    desc: {
      en: 'Catalogs where the dataset is linked.',
      fr: 'Catalogues de rattachement du jeu de donnée.'
    },
    fair: ['f', 'i']
  },
  provenance: {
    propertyUri: 'http://purl.org/dc/terms/provenance',
    label: {
      en: 'Provenance',
      fr: 'Provenance'
    }
  },
  inputData: {
    propertyUri: 'http://purl.org/dc/terms/source',
    label: {
      en: 'Source',
      fr: 'Source'
    },
    desc: {
      en: 'A related resource from which the described resource is derived.',
      fr: 'Une ressource corrélée depuis laquelle la ressource décrite est dérivée.'
    }
  },
  otherResources: {
    propertyUri: 'http://purl.org/dc/terms/relation',
    label: {
      en: 'Relation',
      fr: 'Relation'
    },
    desc: {
      en: 'A related resource.',
      fr: 'Une ressource corrélée.'
    }
  }
}
