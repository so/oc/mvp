import type { OcDistribution, OcModelMetadata } from '@/declarations'
import { resourceMetadata } from './resource'

export const distributionMetadata: OcModelMetadata<OcDistribution> = {
  ...resourceMetadata,
  type: {
    label: {
      en: 'Type',
      fr: 'Type'
    },
    desc: {
      en: 'The distribution type, i.e., whether the distribution points to a file / download page, or instead to a service interface.',
      fr: "Le type de distribution, c'est-à-dire si la distribution pointe vers un fichier / une page de téléchargement, ou plutôt vers une interface de service."
    },
    required: true,
    propertyUri: 'http://purl.org/dc/terms/type',
    dereferencement: 'https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#type',
    fair: ['f', 'i', 'r'],
    vocabularies: ['http://purl.org/coar/resource_type/scheme'],
    comment: {
      en: 'The value SHOULD be taken from a well governed and broadly recognised controlled vocabulary.',
      fr: "La valeur DEVRAIT être tirée d'un vocabulaire contrôlé bien géré et largement reconnu."
    }
  },
  accessRights: {
    label: {
      en: 'Access rights',
      fr: "Droits d'accès"
    },
    desc: {
      en: 'A rights statement that concerns how the distribution is accessed.',
      fr: "Une déclaration de droits concernant les modalités d'accès à la distribution."
    },
    propertyUri: 'http://purl.org/dc/terms/accessRights',
    dereferencement:
      'https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/accessRights',
    fair: ['a', 'r'],
    vocabularies: ['http://data.jrc.ec.europa.eu/access-rights']
  },
  accessURL: {
    label: {
      en: 'Access URL',
      fr: "URL d'accès"
    },
    desc: {
      en: 'A URL of the resource that gives access to a distribution of the dataset. E.g., landing page, feed, SPARQL endpoint.',
      fr: "URL de la ressource qui donne accès à une distribution de l'ensemble de données. Par exemple, une page d'accueil, un flux, un endpoint SPARQL."
    },
    propertyUri: 'https://www.w3.org/ns/dcat#accessURL',
    dereferencement: 'https://www.w3.org/TR/vocab-dcat-3/#Property:distribution_access_url',
    fair: ['a']
  },
  format: {
    label: {
      en: 'Format',
      fr: 'Format'
    },
    desc: {
      en: 'The file format of the distribution.',
      fr: 'Le format de fichier de la distribution.'
    },
    required: true,
    propertyUri: 'http://purl.org/dc/terms/format',
    dereferencement: 'https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#format',
    fair: ['i', 'r'],
    vocabularies: ['http://publications.europa.eu/resource/authority/file-type']
  },
  accessService: {
    label: {
      en: 'Access service',
      fr: "Service d'accès"
    },
    desc: {
      en: 'A data service that gives access to the distribution of the dataset.',
      fr: 'Un service de données qui donne accès à la distribution du jeux de données.'
    },
    propertyUri: 'https://www.w3.org/ns/dcat#accessService',
    dereferencement: 'https://www.w3.org/TR/vocab-dcat/#Property:distribution_access_service',
    fair: ['i', 'r']
  }
}
