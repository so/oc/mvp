import type { OcResource, OcModelMetadata } from '@/declarations'

export const resourceMetadata: OcModelMetadata<OcResource> = {
  '@id': {
    label: '@id'
  },
  '@type': {
    label: '@type'
  },
  identifier: {
    label: {
      en: 'Identifier',
      fr: 'Identifiant'
    },
    required: true,
    propertyUri: 'http://purl.org/dc/terms/identifier',
    dereferencement: 'https://www.w3.org/TR/vocab-dcat-3/#Property:resource_identifier',
    desc: {
      en: 'A unique identifier of the resource being described or catalogued.',
      fr: 'Un identifiant unique de la ressource décrite ou cataloguée.'
    },
    fair: ['f'],
    comment: {
      en: 'The identifier is a text string which is assigned to the resource to provide an unambiguous reference within a particular context.',
      fr: "L'identifiant est une chaîne de texte attribuée à la ressource pour fournir une référence non ambiguë dans un contexte particulier."
    }
    // context: {
    //   "@type": "http://www.w3.org/2000/01/rdf-schema#Literal"
    // }
  },
  title: {
    propertyUri: 'http://purl.org/dc/terms/title',
    dereferencement: 'https://www.w3.org/TR/vocab-dcat-3/#Property:resource_title',
    label: {
      en: 'Title',
      fr: 'Titre'
    },
    required: true,
    desc: {
      en: 'The name / title of the resource',
      fr: 'Le nom / titre de la ressource'
    },
    fair: ['f', 'r'],
    context: {
      '@container': '@language'
    }
  },
  description: {
    propertyUri: 'http://purl.org/dc/terms/description',
    dereferencement: 'https://www.w3.org/TR/vocab-dcat-3/#Property:resource_description',
    label: {
      en: 'Description',
      fr: 'Description'
    },
    required: true,
    desc: {
      en: 'The description of the resource',
      fr: 'La description de la ressource'
    },
    fair: ['f', 'r'],
    context: {
      '@container': '@language'
    }
  },
  conformsTo: {
    label: {
      en: 'Conforms to',
      fr: 'Conforme à'
    },
    required: false,
    desc: {
      en: 'A specification defining the quality criteria the dataset conforms to.',
      fr: "Une spécification définissant les critères de qualité auxquels l'ensemble de données est conforme."
    },
    propertyUri: 'http://purl.org/dc/terms/conformsTo',
    dereferencement:
      'https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/conformsTo',
    fair: ['a'],
    vocabularies: ['http://data.jrc.ec.europa.eu/service-protocol'],
    comment: {
      en: 'This property SHOULD be used to indicate the model, schema, ontology, view or profile that the cataloged resource content conforms to. The specification is typically denoted with its URI. Alternatively, it MAY be described as a document.',
      fr: "Cette propriété DEVRAIT être utilisée pour indiquer le modèle, le schéma, l'ontologie, la vue ou le profil auquel le contenu de la ressource cataloguée est conforme. La spécification est généralement désignée par son URI. Elle peut également être décrite sous la forme d'un document."
    }
  },
  issued: {
    label: {
      en: 'Publication date (release date)',
      fr: 'Date de publication'
    },
    required: true,
    desc: {
      en: 'Date of formal issuance (e.g., publication) of the resource.',
      fr: "Date d'émission officielle (par exemple, publication) de la ressource."
    },
    propertyUri: 'http://purl.org/dc/terms/issued',
    dereferencement: 'https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#issued',
    fair: ['a'],
    comment: {
      en: 'This property SHOULD be set using the first known date of issuance.',
      fr: "Cette propriété DEVRAIT être définie en utilisant la première date d'émission connue."
    }
  },
  license: {
    label: {
      en: 'License',
      fr: 'Licence'
    },
    required: false,
    desc: {
      en: 'A legal document under which the resource is made available.',
      fr: 'Document juridique en vertu duquel la ressource est mise à disposition.'
    },
    propertyUri: 'http://purl.org/dc/terms/license',
    dereferencement: 'https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#license',
    fair: ['a', 'r'],
    vocabularies: ['http://publications.europa.eu/resource/authority/licence'],
    comment: {
      en: 'Information about licenses and rights MAY be provided for the Resource.',
      fr: 'Des informations sur les licences et les droits PEUVENT être fournies pour la ressource.'
    }
  }
}
