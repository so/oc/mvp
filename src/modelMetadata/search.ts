import type { OcModelMetadata, OcSearchParameters } from "@/declarations";

export const searchMetadata: OcModelMetadata<OcSearchParameters> = {
  title: {
    label: {
        fr: "Titre",
        en: "Title"
    }
  },
  description: {
    label: {
        fr: "Description",
        en: "Description"
    }
  },
  creator: {
    label: {
        fr: "Auteur",
        en: "Author"
    }
  }
}
