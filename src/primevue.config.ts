import { definePreset } from '@primevue/themes'
import Aura from '@primevue/themes/aura'
import type { PrimeVueConfiguration } from 'primevue/config'

const config: PrimeVueConfiguration = {
  theme: {
    preset: definePreset(Aura, {
      semantic: {
        primary: {
          50: '#f1f8f4',
          100: '#ddeee3',
          200: '#bedcca',
          300: '#92c3aa',
          400: '#66a586',
          500: '#428767',
          600: '#306b51',
          700: '#275542',
          800: '#214436',
          900: '#1c382d',
          950: '#0f1f19'
        },
      }
    }),
    options: {
      darkModeSelector: '.dark',
      cssLayer: {
        name: 'primevue',
        order: 'tailwind-base, primevue, tailwind-utilities'
      }
    }
  }
}

export default config
