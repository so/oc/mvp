import i18n from '@/i18n'
import {
  createRouter,
  createWebHashHistory
} from 'vue-router'
import { routes, handleHotUpdate } from 'vue-router/auto-routes'

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL ?? ''),
  routes: [
    {
      path: '/',
      redirect() {
        const language = navigator.language.split('-')[0] as typeof i18n.global.locale.value
        if (i18n.global.availableLocales.includes(language)) {
          return { name: 'index', params: { lang: language } }
        } else {
          return { name: 'index', params: { lang: i18n.global.fallbackLocale } }
        }
      }
    },
    ...routes
  ]
})

/**
 * Configure i18n from the first slug of current route.
 */
router.beforeResolve((to) => {
  const language = (
    Array.isArray(to.params.lang) ? to.params.lang[0] : to.params.lang
  ) as typeof i18n.global.locale.value

  if (i18n.global.availableLocales.includes(language)) {
    i18n.global.locale.value = language
  } else {
    return {
      name: `catchall`,
      params: {
        lang: i18n.global.locale.value,
        path: 'error'
      },
      query: {
        code: 404,
        from: to.path
      }
    }
  }
})

// This will update routes at runtime without reloading the page
if (import.meta.hot) {
  handleHotUpdate(router)
}

export default router
