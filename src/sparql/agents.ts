import type { Credentials, OcOrganization, OcPerson } from "@/declarations"
import { executeSparqlConstruct } from "./sparql"

export const queryPersons = async (query: string, auth?: Credentials) => {
  return await executeSparqlConstruct<OcPerson>(
    `
      CONSTRUCT { ?person ?p ?o. }
      FROM <https://www.irit.fr/opencommon/agents>
      WHERE {
        ?person a foaf:Person;
                foaf:familyName|foaf:firstName ?name;
                ?p ?o.
        FILTER (regex(?name, "${query}", "i"))
      }
    `,
    {
      auth: auth,
      context: {
        "identifier": {
          "@id": "http://purl.org/dc/terms/identifier",
          "@type": "http://www.w3.org/2000/01/rdf-schema#Literal"
        },
        "@type": {
          "@container": "@set"
        },
        "page": {
          "@id": "http://www.irit.fr/opencommon/page",
          "@type": "@id"
        },
        "homepage": {
          "@id": "http://xmlns.com/foaf/0.1/homepage",
          "@type": "@id"
        },
        "topic_interest": {
          "@id": "http://xmlns.com/foaf/0.1/topic_interest",
          "@type": "@id"
        },
        "familyName": {
          "@id": "http://xmlns.com/foaf/0.1/familyName"
        },
        "firstName": {
          "@id": "http://xmlns.com/foaf/0.1/firstName"
        }
      }
    }
  )
}

export const queryOrganizations = async (query: string, auth?: Credentials) => {
  return await executeSparqlConstruct<OcOrganization>(
    `
      CONSTRUCT { ?organization ?p ?o. }
      FROM <https://www.irit.fr/opencommon/agents>
      WHERE {
        ?organization a foaf:Organization;
                      foaf:name ?name;
                      ?p ?o.
        FILTER regex(?name, "${query}", "i")
      }
    `,
    {
      auth: auth,
      context: {
        "identifier": {
          "@id": "http://purl.org/dc/terms/identifier",
          "@type": "http://www.w3.org/2000/01/rdf-schema#Literal"
        },
        "@type": {
          "@container": "@set"
        },
        "name": {
          "@id": "http://xmlns.com/foaf/0.1/name",
          "@container": "@language"
        },
      }
    }
  )
}