import {
  executeSparqlConstruct,
  executeSparqlInsert,
  executeSparqlUpdate,
  formatDate,
  formatLocalizedProperty
} from './sparql'
import { resourceContext } from './resource'
import { datasetContext } from './datasets'
import type {
  Credentials,
  OcCatalog,
  OcConcept,
  OcDataset,
  OcPerson,
  OcCatalogSummary,
  OcOrganization
} from '@/declarations'
import { conceptContext } from './vocabularies'

const catalogGetContext = {
  ...datasetContext
}

export const getCatalogSummary = async (identifier: string, auth?: Credentials) => {
  const result = await executeSparqlConstruct<OcCatalogSummary>(
    `
    CONSTRUCT {
      ?catalog dct:title ?title;
               dct:identifier ?identifier;
               ?p ?o;
               oct:graph ?g.
    }
    WHERE {
      GRAPH ?g {
        ?catalog a dcat:Catalog;
                 dct:identifier ?identifier.
        FILTER (STR(?identifier) = "${identifier}")
      
        VALUES ?p {
          rdf:type
          dct:title
          dct:description
          dcat:catalog
        }
        
        ?catalog ?p ?o.
      }
    }
    `,
    {
      auth: auth,
      context: {
        ...resourceContext,
        catalog: {
          '@id': 'http://www.w3.org/ns/dcat#catalog',
          '@type': '@id',
          '@container': '@set'
        }
      }
    }
  )
  if (result.length) {
    return result[0]
  } else {
    console.warn('Catalog with identifier "' + identifier + '" not found')
    return null
  }
}

export const getCatalogSummaryFromUri = async (uri: string, auth?: Credentials) => {
  const result = await executeSparqlConstruct<OcCatalogSummary>(
    `
    CONSTRUCT {
      <${uri}> ?p ?o;
               oct:graph ?g.
    }
    WHERE {
      GRAPH ?g {
        <${uri}> a dcat:Catalog.
        VALUES ?p {
          dct:identifier
          rdf:type
          dct:title
          dct:description
          dcat:catalog
        }
        <${uri}> ?p ?o.
      }
    }
    `,
    {
      auth: auth,
      context: {
        ...resourceContext,
        catalog: {
          '@id': 'http://www.w3.org/ns/dcat#catalog',
          '@type': '@id',
          '@container': '@set'
        }
      }
    }
  )

  if (result.length) {
    return result[0]
  } else {
    console.warn('Catalog with uri "' + uri + '" not found')
    return null
  }
}

export const getCatalogSummaryFromParentUri = async (uri: string, auth?: Credentials) => {
  return await executeSparqlConstruct<OcCatalogSummary>(
    `
    CONSTRUCT {
      ?node ?p ?o;
            oct:graph ?g.
    }
    WHERE {
      <${uri}> a dcat:Catalog;
               dcat:catalog ?node.
      GRAPH ?g {
        VALUES ?p {
          dct:identifier
          rdf:type
          dct:title
          dct:description
          dcat:catalog
        }
        ?node ?p ?o.
      }
    }
    `,
    {
      auth: auth,
      context: {
        ...resourceContext,
        catalog: {
          '@id': 'http://www.w3.org/ns/dcat#catalog',
          '@type': '@id',
          '@container': '@set'
        }
      }
    }
  )
}

export const getCatalogSummaryFromChildUri = async (uri: string, auth?: Credentials) => {
  return await executeSparqlConstruct<OcCatalogSummary>(
    `
    CONSTRUCT {
      ?node ?p ?o;
            oct:graph ?g.
    }
    WHERE {
      ?node a dcat:Catalog;
            dcat:catalog <${uri}>.
      
      GRAPH ?g {
        VALUES ?p {
          dct:identifier
          rdf:type
          dct:title
          dct:description
          dcat:catalog
        }
        ?node ?p ?o.
      }
    }
    `,
    {
      auth: auth,
      context: {
        ...resourceContext,
        catalog: {
          '@id': 'http://www.w3.org/ns/dcat#catalog',
          '@type': '@id',
          '@container': '@set'
        }
      }
    }
  )
}

export async function insertCatalog(
  catalog: OcCatalog,
  parentCatalog: OcCatalog,
  profile: OcPerson,
  auth?: Credentials
) {
  catalog.identifier ||= crypto.randomUUID()
  catalog['@id'] ||= `${import.meta.env.VITE_OC_URI_BASE_URL}/catalogs/${catalog.identifier}`

  if (!catalog.graph) {
    throw Error('Catalog has no graph!')
  }
  if (!catalog.parentCatalog) {
    throw Error('Catalog has no parent catalog!')
  }

  let insertQuery = `
    INSERT INTO <${catalog.graph}> {
      <${parentCatalog['@id']}> dcat:catalog <${catalog['@id']}>.
  `
  insertQuery += buildCatalogTriples(catalog, profile)
  insertQuery += `}`

  await executeSparqlInsert(insertQuery, { auth: auth })

  return catalog
}

export async function updateCatalog(catalog: OcCatalog, profile: OcPerson, auth?: Credentials) {
  // An update consiste of a concomitant delete
  // and insert.
  // We first delete all triples handled by our
  // form, then we insert new values.
  // Note that we keep `prov:qualifiedAttribution`
  // values.

  if (!catalog.graph) {
    throw Error('Dataset has no graph!')
  }

  await executeSparqlUpdate(
    `
    WITH <${catalog.graph[0]}>
    DELETE {
      <${catalog['@id']}> ?p ?o.
      ?temporal ?p2 ?o2.
    }
    WHERE {
      VALUES ?p {
        dct:type
        dct:identifier
        dct:title
        dct:description
        dcat:contactPoint
        dct:publisher
        dcat:theme
        dct:issued
        dct:temporal
        dct:spatial
      }
      OPTIONAL {
        <${catalog['@id']}> dct:temporal ?temporal.
        ?temporal ?p2 ?o2.
        FILTER isblank(?temporal)
      }
      <${catalog['@id']}> ?p ?o.
    };

    WITH <${catalog.graph[0]}>
    INSERT { ${buildCatalogTriples(catalog, profile)} };
    `,
    { auth }
  )

  return catalog
}

function buildCatalogTriples(catalog: OcCatalog, profile: OcPerson) {
  const contactPoint = (catalog.contactPoint as (OcPerson | OcOrganization)[])
    ?.map((item) => `<${item['@id']}>`)
    .join(',')

  let triples = `
    <${catalog['@id']}> a dcat:Catalog;
            dct:identifier "${catalog.identifier}";
            dct:title ${formatLocalizedProperty(catalog.title)};
            dct:description ${formatLocalizedProperty(catalog.description)};
            dcat:contactPoint ${contactPoint};
            dct:publisher <${(catalog.publisher as OcOrganization)?.['@id']}>;
            prov:qualifiedAttribution [
              a prov:Attribution;
              prov:agent <${profile['@id']}>;
              prov:generatedAtTime ${formatDate(new Date())};
              dcat:hadRole <https://www.irit.fr/opencommon/terms/dataStewardRole>
            ]`

  if (catalog.theme?.length) {
    const theme = catalog.theme?.map((item) => `<${item['@id']}>`).join(',')
    triples += `;
    dcat:theme ${theme}`
  }
  if (catalog.issued) {
    triples += `;
    dct:issued ${formatDate(catalog.issued as Date)}`
  }
  if (catalog.temporal && catalog.temporal.length) {
    const temporal = (catalog.temporal as [Date, Date][]).map((value) => {
      return `[
        a dct:PeriodOfTime;
          dcat:startDate ${formatDate(value[0])};
          dcat:endDate ${formatDate(value[1])};
      ]`
    })

    triples += `;
    dct:temporal ${temporal.join(',')}`
  }
  if (catalog.spatial && catalog.spatial.length) {
    const spatial = catalog.spatial?.map((item) => `<${item['@id']}>`).join(',')
    triples += `;
    dct:spatial ${spatial}`
  }

  triples += '.'

  return triples
}

/**
 * Retrieve a catalog,
 * feeded with creators, sub catalogs, datasets, spatial, temporals and themes.
 *
 * @param identifier Local identifier for Virtuoso
 * @param auth Credentials to use if provided
 * @returns The catalog retrieved
 */
export async function getCatalog(identifier: string, auth?: Credentials): Promise<OcCatalog> {
  /**
   * First, get the @id of the catalog
   * to better filtering requests.
   */
  const catalogIdResponse = await executeSparqlConstruct<OcCatalog>(
    `
    CONSTRUCT {
      ?catalog ?p ?o;
               oct:graph ?g.
    }
    WHERE {
      GRAPH ?g {
        ?catalog a dcat:Catalog;
                 dct:identifier ?identifier.
        FILTER (str(?identifier) = "${identifier}")

        ?catalog ?p ?o.
      }
    }
  `,
    {
      auth,
      context: catalogGetContext
    }
  )
  const catalog = catalogIdResponse?.[0]
  const catalogUri = catalog['@id']

  /**
   * Temporal
   */
  const catalogTemporalPromise = executeSparqlConstruct<{ startDate: Date; endDate: Date }>(
    `
    CONSTRUCT {
      ?temporal ?p ?o.
    }
    WHERE {
      <${catalogUri}> dct:temporal ?temporal.
      OPTIONAL {
        ?temporal ?p ?o.
      }
    }
  `,
    {
      auth,
      context: {
        startDate: {
          '@id': 'http://www.w3.org/ns/dcat#startDate',
          '@type': 'http://www.w3.org/2001/XMLSchema#dateTime'
        },
        endDate: {
          '@id': 'http://www.w3.org/ns/dcat#endDate',
          '@type': 'http://www.w3.org/2001/XMLSchema#dateTime'
        }
      }
    }
  )

  /**
   * Creators
   */
  const catalogCreatorPromise = executeSparqlConstruct<OcPerson>(
    `
    CONSTRUCT {
      ?creator ?p1 ?o1.
    }
    WHERE {
      <${catalogUri}> dct:creator ?creator.
      VALUES ?p1 { rdf:type foaf:familyName foaf:name foaf:givenName foaf:firstName foaf:lastName foaf:mbox }
      ?creator ?p1 ?o1.
    }
    `,
    {
      auth
    }
  )

  /**
   * Contact points
   */
  const catalogContactPointPromise = executeSparqlConstruct<OcPerson>(
    `
    CONSTRUCT {
      ?contactPoint ?p1 ?o1.
    }
    WHERE {
      <${catalogUri}> dcat:contactPoint ?contactPoint.
      VALUES ?p1 { rdf:type foaf:familyName foaf:name foaf:givenName foaf:firstName foaf:lastName foaf:mbox }
      ?contactPoint ?p1 ?o1.
    }
    `,
    {
      auth
    }
  )

  /**
   * Spatial
   */
  const catalogSpatialPromise = executeSparqlConstruct<OcConcept>(
    `
    CONSTRUCT {
      ?spatial skos:prefLabel ?o.
      }
    WHERE {
      <${catalogUri}> dct:spatial ?spatial.
      ?spatial skos:prefLabel ?o.
    }
    `,
    {
      auth,
      context: conceptContext
    }
  )

  /**
   * Theme
   */
  const catalogThemePromise = executeSparqlConstruct<OcConcept>(
    `
    CONSTRUCT {
      ?theme skos:prefLabel ?o.
      }
    WHERE {
      <${catalogUri}> dcat:theme ?theme.
      ?theme skos:prefLabel ?o.
    }
    `,
    {
      auth,
      context: conceptContext
    }
  )

  /**
   * language
   */
  const catalogLanguagePromise = executeSparqlConstruct<OcConcept>(
    `
    CONSTRUCT {
      ?language skos:prefLabel ?o1.
    }
    WHERE {
      <${catalogUri}> dct:language ?language.
      ?language skos:prefLabel ?o1.
    }
    `,
    {
      auth,
      context: conceptContext
    }
  )

  /**
   * status
   */
  const catalogStatusPromise = executeSparqlConstruct<OcConcept>(
    `
    CONSTRUCT {
      ?status skos:prefLabel ?o1.
    }
    WHERE {
      <${catalogUri}> adms:status ?status.
      ?status skos:prefLabel ?o1.
    }
    `,
    {
      auth,
      context: conceptContext
    }
  )

  /**
   * Type
   */
  const catalogTypePromise = executeSparqlConstruct<OcConcept>(
    `
    CONSTRUCT {
      ?type skos:prefLabel ?o.
    }
    WHERE {
      <${catalogUri}> dct:type ?type.
      ?type skos:prefLabel ?o.
    }
    `,
    {
      auth,
      context: conceptContext
    }
  )

  /**
   * Sub catalogs
   */
  const catalogSubsPromise = executeSparqlConstruct<OcCatalog>(
    `
    CONSTRUCT {
      ?subcatalog ?p ?o.
    }
    WHERE {
      <${catalogUri}> dcat:catalog ?subcatalog.
        VALUES ?p { dct:identifier rdf:type dct:title dct:description }
        ?subcatalog ?p ?o.
    }
    `,
    {
      auth,
      context: resourceContext
    }
  )

  /**
   * Datasets
   */
  const catalogDatasetsPromise = executeSparqlConstruct<OcDataset>(
    `
    CONSTRUCT {
      ?dataset ?p ?o.
    }
    WHERE {
      <${catalogUri}> dcat:dataset ?dataset.
        VALUES ?p { dct:identifier rdf:type dct:title dct:description }
        ?dataset ?p ?o.
    }
    `,
    {
      auth,
      context: resourceContext
    }
  )

  /**
   * Publisher
   */
  const catalogPublisherPromise = executeSparqlConstruct<OcOrganization>(
    `
    CONSTRUCT {
      ?publisher ?p ?o.
    }
    WHERE {
      <${catalogUri}> dct:publisher ?publisher.
        VALUES ?p { dct:identifier rdf:type dct:title dct:description foaf:name }
        ?publisher ?p ?o.
    }
    `,
    {
      auth,
      context: resourceContext
    }
  )

  const catalogResponse = await Promise.all([
    catalogTemporalPromise, // 0
    catalogStatusPromise, // 1
    catalogCreatorPromise, // 2
    catalogContactPointPromise, // 3
    catalogThemePromise, // 4
    catalogSpatialPromise, // 5
    catalogLanguagePromise, // 6
    catalogTypePromise, // 7
    catalogSubsPromise, // 8
    catalogDatasetsPromise, // 9
    catalogPublisherPromise // 10
  ])

  catalog.temporal = catalogResponse[0].map<[Date, Date]>((temporal) => [
    temporal.startDate,
    temporal.endDate
  ])
  catalog.status = catalogResponse[1][0]
  catalog.creator = catalogResponse[2]
  catalog.contactPoint = catalogResponse[3]
  catalog.theme = catalogResponse[4]
  catalog.spatial = catalogResponse[5]
  catalog.language = catalogResponse[6]
  catalog.type = catalogResponse[7][0]
  catalog.catalogs = catalogResponse[8]
  catalog.datasets = catalogResponse[9]
  catalog.publisher = catalogResponse[10][0]

  return catalog
}
