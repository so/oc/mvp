import type { OcCommunity, Credentials } from "@/declarations";
import { executeSparqlConstruct, executeSparqlInsert } from "./sparql";
import type { ContextDefinition } from "jsonld";

const communityContext: ContextDefinition = {
  "identifier": {
    "@id": "http://purl.org/dc/terms/identifier",
    "@type": "http://www.w3.org/2000/01/rdf-schema#Literal"
  },
  "title": {
    "@id": "http://purl.org/dc/terms/title",
    "@container": "@language"
  },
  "description": {
    "@id": "http://purl.org/dc/terms/description",
    "@container": "@language"
  },
  "abstract": {
    "@id": "http://purl.org/dc/terms/abstract",
    "@container": "@language"
  },
  "name": {
    "@id": "http://xmlns.com/foaf/0.1/name",
  },
  "color": {
    "@id": "https://www.irit.fr/opencommon/terms/color",
  },
  "relation": {
    "@id": "http://purl.org/dc/terms/relation",
    "@type": "@id"
  },
  "isSpaceOf": {
    "@id": "https://www.irit.fr/opencommon/system/isSpaceOf",
    "@type": "@id"
  },
  "logo": {
    "@id": "http://xmlns.com/foaf/0.1/logo",
    "@type": "@id"
  },
}

export const getCommunityList = async (auth?: Credentials) => {
  return executeSparqlConstruct<OcCommunity>(
    `
      CONSTRUCT { ?space ?p ?o. }
      WHERE {
        ?space a oct:Community;
               ?p ?o.
      }
    `,
    {
      auth: auth,
      context: communityContext,
    }
  )
}

export function insertCommunityAccessRequest(communityUri: string, userLoginID: string, auth?: Credentials) {
  const uuid = crypto.randomUUID()
  return executeSparqlInsert(`
    INSERT INTO <test:graph:garbage> {
      <https://www.irit.fr/opencommon/members/${uuid}> a foaf:Person;
                                                       oct:loginID '${userLoginID}';
                                                       foaf:name '${userLoginID}';
                                                       prov:generatedAtTime "${new Date().toISOString()}"^^xsd:dateTime;
                                                       org:hasMembership _:membership.
      _:membership org:organization <${communityUri}>.
    }
  `,
    { auth: auth }
  )
}
