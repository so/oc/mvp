import { getMemberMemberships, getMemberInfos, getMemberProfile } from "./members"
import { executeSparqlInsert } from "./sparql"
import type { Credentials, OcProfileInfo } from "@/declarations"

export async function getProfileInfos(auth: Credentials): Promise<OcProfileInfo> {

  const profileInfos: OcProfileInfo = {
    infos: null,
    profile: null,
    memberships: []
  }

  const memberInfosList = await getMemberInfos(auth)
  if (memberInfosList.length === 0) {
    throw new Error('No infos found for current user.')
  }
  profileInfos.infos = memberInfosList[0]

  const memberProfileList = await getMemberProfile(auth)
  if (memberProfileList.length === 0) {
    console.warn('No profile found for current user.')
    // This should not happen but could, due to existing broken data.
    // At this point, we can at least provide a profile with the ID.
    profileInfos.profile = {
      '@id': profileInfos.infos["@id"],
      '@type': ["http://xmlns.com/foaf/0.1/Person"]
    }
  } else {
    profileInfos.profile = memberProfileList[0]
  }

  profileInfos.memberships = await getMemberMemberships(auth)

  return profileInfos
}

export async function insertRegistrationRequest(
  login: string,
  firstName: string,
  givenName: string,
  organizationRor?: string,
  organizationCustom?: string
) {
  const uuid = crypto.randomUUID()

  if (!organizationCustom && !organizationRor) {
    throw new Error('Either organizationRor or organizationCustom should be provided.')
  }
  const organization = organizationRor ? `<${organizationRor}>` : `'${organizationCustom}'`

  return await executeSparqlInsert(
    `
    INSERT INTO <oct:visitors> {
      <${import.meta.env.VITE_OC_URI_BASE_URL}/opencommon/members/${uuid}> a foaf:Person;
                                                       oct:loginID '${login}';
                                                       dct:identifier '${uuid}';
                                                       prov:generatedAtTime "${new Date().toISOString()}"^^xsd:dateTime;
                                                       foaf:givenName '${givenName}';
                                                       foaf:familyName '${firstName}';
                                                       org:memberOf ${organization}.
    }
    `,
    { asVisitor: true }
  )
}
