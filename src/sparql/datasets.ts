import type {
  OcResource,
  Credentials,
  OcDataset,
  OcPerson,
  OcConcept,
  OcOrganization,
  OcDatasetSummary,
  OcAdmsIdentifier,
  OcGeometry,
  OcProvenance
} from '@/declarations'
import {
  defaultLocalizedPropFormatter,
  escapeString,
  executeSparqlConstruct,
  executeSparqlInsert,
  executeSparqlUpdate,
  formatDate,
  formatLocalizedProperty
} from './sparql'
import type { ContextDefinition } from 'jsonld'
import { resourceContext } from './resource'

export async function insertDataset(dataset: OcDataset, profile: OcPerson, auth?: Credentials) {
  dataset.identifier ||= crypto.randomUUID()
  dataset['@id'] ||= `${import.meta.env.VITE_OC_URI_BASE_URL}/datasets/${dataset.identifier}`

  if (!dataset.graph) {
    throw Error('Dataset has no graph!')
  }
  if (!dataset.catalog) {
    throw Error('Dataset has no catalog!')
  }

  let insertQuery = `
    INSERT INTO <${dataset.graph[0]}> {
      <${dataset.catalog['@id']}> dcat:dataset <${dataset['@id']}>.
  `
  insertQuery += buildDatasetTriples(dataset, profile)
  insertQuery += `}`

  await executeSparqlInsert(insertQuery, { auth: auth })

  return dataset
}

export async function updateDataset(dataset: OcDataset, profile: OcPerson, auth?: Credentials) {
  // An update consiste of a concomitant delete
  // and insert.
  // We first delete all triples handled by our
  // form, then we insert new values.
  // Note that we keep `prov:qualifiedAttribution`
  // values.

  if (!dataset.graph) {
    throw Error('Dataset has no graph!')
  }

  await executeSparqlUpdate(
    `
    WITH <${dataset.graph[0]}>
    DELETE {
      <${dataset['@id']}> ?p ?o.
      ?temporal ?p2 ?o2.
    }
    WHERE {
      VALUES ?p {
        dct:type
        dct:identifier
        dct:title
        dcat:theme
        dct:description
        dct:publisher
        dct:issued
        dct:creator
        dcat:contactPoint
        dct:accrualPeriodicity
        dct:language
        dcat:keyword
        dcat:status
        dct:modified
        dct:temporal
        dct:spatial
        dcat:landingPage
        dct:license
        dct:conformsTo
        dcat:version
      }
      OPTIONAL {
        <${dataset['@id']}> dct:temporal ?temporal.
        ?temporal ?p2 ?o2.
        filter isblank(?temporal)
      }
      <${dataset['@id']}> ?p ?o.
    };

    WITH <${dataset.graph[0]}>
    INSERT { ${buildDatasetTriples(dataset, profile)} };
    `,
    { auth }
  )

  return dataset
}

function buildDatasetTriples(dataset: OcDataset, profile: OcPerson) {
  const theme = dataset.theme?.map((item) => `<${item['@id']}>`).join(',')
  const creator = (dataset.creator as (OcPerson | OcOrganization)[])
    ?.map((item) => `<${item['@id']}>`)
    .join(',')
  const language = (dataset.language as OcConcept[])?.map((item) => `<${item['@id']}>`).join(',')
  const contactPoint = (dataset.contactPoint as (OcPerson | OcOrganization)[])
    ?.map((item) => `<${item['@id']}>`)
    .join(',')

  let triples = `
    <${dataset['@id']}> a dcat:Dataset;
            dct:type <${dataset.type?.['@id']}>;
            dct:identifier "${dataset.identifier}";
            dct:title ${formatLocalizedProperty(dataset.title)};
            dcat:theme ${theme};
            dct:description ${formatLocalizedProperty(dataset.description)};
            dct:publisher <${(dataset.publisher as OcOrganization)?.['@id']}>;
            dct:issued ${formatDate(dataset.issued as Date)};
            dct:creator ${creator};
            dcat:contactPoint ${contactPoint};
            dct:accrualPeriodicity <${dataset.accrualPeriodicity?.['@id']}>;
            dct:language ${language};
            prov:qualifiedAttribution [
              a prov:Attribution;
              prov:agent <${profile['@id']}>;
              prov:generatedAtTime ${formatDate(new Date())};
              dcat:hadRole <https://www.irit.fr/opencommon/terms/dataStewardRole>
            ]`

  if (dataset.keyword) {
    const keywords: string = formatLocalizedProperty<string[]>(dataset.keyword, (item, locale) => {
      return item.map((item) => defaultLocalizedPropFormatter(item, locale)).join(', ')
    })
    triples += `;
    dcat:keyword ${keywords}`
  }
  if (dataset.status) {
    triples += `;
    adms:status <${dataset.status['@id']}>`
  }
  if (dataset.modified) {
    triples += `;
    dct:modified ${formatDate(dataset.modified as Date)}`
  }
  if (dataset.temporal && dataset.temporal.length) {
    const temporal = (dataset.temporal as [Date, Date][]).map((value) => {
      return `[
        a dct:PeriodOfTime;
          dcat:startDate ${formatDate(value[0])};
          dcat:endDate ${formatDate(value[1])};
      ]`
    })

    triples += `;
    dct:temporal ${temporal.join(',')}`
  }
  if (dataset.spatial && dataset.spatial.length) {
    const spatial = dataset.spatial?.map((item) => `<${item['@id']}>`).join(',')
    triples += `;
    dct:spatial ${spatial}`
  }
  if (dataset.landingPage) {
    triples += `;
    dcat:landingPage "${escapeString(dataset.landingPage)}"`
  }
  if (dataset.license) {
    triples += `;
    dct:license <${dataset.license['@id']}>`
  }
  if (dataset.conformsTo) {
    triples += `;
    dct:conformsTo <${dataset.conformsTo['@id']}>`
  }
  if (dataset.version) {
    triples += `;
    dcat:version "${escapeString(dataset.version)}"`
  }

  triples += `.
  `

  return triples
}

export const datasetContext: ContextDefinition = {
  ...resourceContext,
  keyword: {
    '@id': 'http://www.w3.org/ns/dcat#keyword',
    '@container': ['@language', '@set']
  }
}

/**
 * Retrieve a dataset,
 * feeded with creators, distributions, spatial, temporals and themes.
 *
 * @param identifier Local identifier for Virtuoso
 * @param auth Credentials to use if provided
 * @returns The dataset retrieved
 */
export async function getDataset(identifier: string, auth?: Credentials): Promise<OcDataset> {
  /**
   * First, get the @id of the dataset
   * to better filtering requests.
   */
  const datasetIdResponse = await executeSparqlConstruct<OcDataset>(
    `
    CONSTRUCT {
      ?dataset ?p ?o.
      ?dataset oct:graph ?g.
    }
    WHERE {
      ?dataset ?p ?o;
        dct:identifier "${identifier}".
      GRAPH ?g {
        ?dataset dct:identifier ?id
      }
    }
  `,
    {
      auth,
      context: datasetContext
    }
  )
  const dataset = datasetIdResponse?.[0]
  const datasetUri = dataset['@id']

  /**
   * Temporal
   */
  const datasetTemporalPromise = executeSparqlConstruct<{ startDate: Date; endDate: Date }>(
    `
    CONSTRUCT {
      ?temporal ?p ?o.
    }
    WHERE {
      <${datasetUri}> dct:temporal ?temporal.
      OPTIONAL {
        ?temporal ?p ?o.
      }
    }
  `,
    {
      auth,
      context: {
        startDate: {
          '@id': 'http://www.w3.org/ns/dcat#startDate',
          '@type': 'http://www.w3.org/2001/XMLSchema#dateTime'
        },
        endDate: {
          '@id': 'http://www.w3.org/ns/dcat#endDate',
          '@type': 'http://www.w3.org/2001/XMLSchema#dateTime'
        }
      }
    }
  )

  /**
   * Provenance
   */
  const datasetProvenancePromise = executeSparqlConstruct<OcProvenance>(
    `
    CONSTRUCT {
      ?provenance ?p ?o.
    }
    WHERE {
      <${datasetUri}> dct:provenance ?provenance.
      OPTIONAL {
        ?provenance ?p ?o.
      }
    }
  `,
    {
      auth,
      context: {
        label: {
          '@id': 'http://www.w3.org/2000/01/rdf-schema#label',
          '@container': '@language'
        }
      }
    }
  )

  /**
   * Creators
   */
  const datasetCreatorPromise = executeSparqlConstruct<OcPerson>(
    `
    CONSTRUCT {
      ?creator ?p1 ?o1.
    }
    WHERE {
      <${datasetUri}> dct:creator ?creator.
      ?creator ?p1 ?o1.
      FILTER (?p1 IN (rdf:type, foaf:familyName, foaf:name, foaf:givenName, foaf:firstName, foaf:lastName, foaf:mbox))
    }
    `,
    {
      auth
    }
  )

  /**
   * Contact points
   */
  const datasetContactPointPromise = executeSparqlConstruct<OcPerson>(
    `
    CONSTRUCT {
      ?contactPoint ?p1 ?o1.
    }
    WHERE {
      <${datasetUri}> dcat:contactPoint ?contactPoint.
      ?contactPoint ?p1 ?o1.
      FILTER (?p1 IN (rdf:type, foaf:familyName, foaf:name, foaf:givenName, foaf:firstName, foaf:lastName, foaf:mbox))
    }
    `,
    {
      auth
    }
  )

  /**
   * Publisher
   */
  const datasetPublisherPromise = executeSparqlConstruct<OcOrganization>(
    `
    CONSTRUCT {
      ?publisher ?p1 ?o1.
    }
    WHERE {
      <${datasetUri}> dct:publisher ?publisher.
      ?publisher ?p1 ?o1.
      FILTER (?p1 IN (rdf:type, foaf:name, foaf:mbox))
    }
    `,
    {
      auth,
      context: {
        name: {
          '@id': 'http://xmlns.com/foaf/0.1/name',
          '@container': '@language'
        },
        mbox: {
          '@id': 'http://xmlns.com/foaf/0.1/mbox',
          '@type': '@id'
        }
      }
    }
  )

  /**
   * Spatial concept(s)
   */
  const datasetSpatialConceptPromise = executeSparqlConstruct<OcConcept>(
    `
    CONSTRUCT {
      ?spatial skos:prefLabel ?o.
      }
    WHERE {
      <${datasetUri}> dct:spatial ?spatial.
      ?spatial skos:prefLabel ?o.
    }
    `,
    {
      auth,
      context: {
        prefLabel: {
          '@id': 'http://www.w3.org/2004/02/skos/core#prefLabel',
          '@container': '@language'
        }
      }
    }
  )

  /**
   * Spatial geometry(s)
   */
  const datasetSpatialGeometryPromise = executeSparqlConstruct<OcGeometry>(
    `
    CONSTRUCT {
      ?spatial ?p ?geometry.
      ?spatial rdf:type dct:Location.
    }
    WHERE {
      <${datasetUri}> dct:spatial ?spatial.
      ?spatial a dct:Location.
      ?spatial ?p ?geometry.
      FILTER (bif:isgeometry(?geometry))
    }
    `,
    {
      auth,
      context: {
        geometry: {
          '@id': 'http://www.w3.org/ns/locn#geometry',
          '@container': '@set'
        }
      }
    }
  )

  /**
   * Theme
   */
  const datasetThemePromise = executeSparqlConstruct<OcConcept>(
    `
    CONSTRUCT {
      ?theme skos:prefLabel ?o.
      }
    WHERE {
      <${datasetUri}> dcat:theme ?theme.
      ?theme skos:prefLabel ?o.
    }
    `,
    {
      auth,
      context: {
        prefLabel: {
          '@id': 'http://www.w3.org/2004/02/skos/core#prefLabel',
          '@container': '@language'
        }
      }
    }
  )

  /**
   * Distribution
   */
  const datasetDistributionPromise = executeSparqlConstruct<OcResource>(
    `
    CONSTRUCT {
      ?distribution ?p ?o.
      }
    WHERE {
      <${datasetUri}> dcat:distribution ?distribution.
      ?distribution ?p ?o.
    }
    `,
    {
      auth,
      context: {
        ...resourceContext,
        accessURL: {
          '@id': 'http://www.w3.org/ns/dcat#accessURL',
          '@type': '@id'
        },
        license: {
          '@id': 'http://purl.org/dc/terms/license',
          '@type': '@id'
        },
        type: {
          '@id': 'http://purl.org/dc/terms/type',
          '@type': '@id'
        },
        accessRights: {
          '@id': 'http://purl.org/dc/terms/accessRights',
          '@type': '@id'
        },
        format: {
          '@id': 'http://purl.org/dc/terms/format',
          '@type': '@id'
        }
      }
    }
  )

  /**
   * Identifiers DOI
   */
  const datasetIdentifierPromise = executeSparqlConstruct<OcAdmsIdentifier>(
    `
    CONSTRUCT {
      ?identifier ?p ?o.
      }
    WHERE {
      <${datasetUri}> adms:identifier ?identifier.
      ?identifier ?p ?o.
    }
    `,
    {
      auth,
      context: {
        notation: {
          '@id': 'http://www.w3.org/2004/02/skos/core#notation'
        },
        creator: {
          '@id': 'http://purl.org/dc/terms/creator',
          '@type': '@id'
        },
        schemeAgency: {
          '@id': 'http://www.w3.org/ns/adms#schemeAgency'
        },
        issued: {
          '@id': 'http://purl.org/dc/terms/issued',
          '@type': 'http://www.w3.org/2001/XMLSchema#date'
        }
      }
    }
  )

  /**
   * Type
   */
  const datasetTypePromise = executeSparqlConstruct<OcConcept>(
    `
    CONSTRUCT {
      ?type skos:prefLabel ?o.
    }
    WHERE {
      <${datasetUri}> dct:type ?type.
      ?type skos:prefLabel ?o.
    }
    `,
    {
      auth,
      context: {
        prefLabel: {
          '@id': 'http://www.w3.org/2004/02/skos/core#prefLabel',
          '@container': '@language'
        }
      }
    }
  )

  /**
   * language
   */
  const datasetLanguagePromise = executeSparqlConstruct<OcConcept>(
    `
    CONSTRUCT {
      ?language skos:prefLabel ?o1.
    }
    WHERE {
      <${datasetUri}> dct:language ?language.
      ?language skos:prefLabel ?o1.
    }
    `,
    {
      auth,
      context: {
        prefLabel: {
          '@id': 'http://www.w3.org/2004/02/skos/core#prefLabel',
          '@container': '@language'
        }
      }
    }
  )

  /**
   * status
   */
  const datasetStatusPromise = executeSparqlConstruct<OcConcept>(
    `
    CONSTRUCT {
      ?status skos:prefLabel ?o1.
    }
    WHERE {
      <${datasetUri}> adms:status ?status.
      ?status skos:prefLabel ?o1.
    }
    `,
    {
      auth,
      context: {
        prefLabel: {
          '@id': 'http://www.w3.org/2004/02/skos/core#prefLabel',
          '@container': '@language'
        }
      }
    }
  )

  /**
   * AccrualPeriodicity (Frequency)
   */
  const datasetAccrualPeriodicityPromise = executeSparqlConstruct<OcConcept>(
    `
    CONSTRUCT {
      ?accrualPeriodicity skos:prefLabel ?o1.
    }
    WHERE {
      <${datasetUri}> dct:accrualPeriodicity ?accrualPeriodicity.
      ?accrualPeriodicity skos:prefLabel ?o1.
    }
    `,
    {
      auth,
      context: {
        prefLabel: {
          '@id': 'http://www.w3.org/2004/02/skos/core#prefLabel',
          '@container': '@language'
        }
      }
    }
  )

  /**
   * license
   */
  const datasetLicensePromise = executeSparqlConstruct<OcConcept>(
    `
    CONSTRUCT {
      ?license skos:prefLabel ?o1.
    }
    WHERE {
      <${datasetUri}> dct:license ?license.
      ?license skos:prefLabel ?o1.
    }
    `,
    {
      auth,
      context: {
        prefLabel: {
          '@id': 'http://www.w3.org/2004/02/skos/core#prefLabel',
          '@container': '@language'
        }
      }
    }
  )

  /**
   * conformsTo
   */
  const datasetConformsToPromise = executeSparqlConstruct<OcConcept>(
    `
    CONSTRUCT {
      ?conformsTo skos:prefLabel ?o1.
    }
    WHERE {
      <${datasetUri}> dct:conformsTo ?conformsTo.
      ?conformsTo skos:prefLabel ?o1.
    }
    `,
    {
      auth,
      context: {
        prefLabel: {
          '@id': 'http://www.w3.org/2004/02/skos/core#prefLabel',
          '@container': '@language'
        }
      }
    }
  )

  const datasetResponse = await Promise.all([
    datasetTemporalPromise, // 0
    datasetStatusPromise, // 1
    datasetCreatorPromise, // 2
    datasetContactPointPromise, // 3
    datasetPublisherPromise, // 4
    datasetThemePromise, // 5
    datasetDistributionPromise, // 6
    datasetIdentifierPromise, // 7
    datasetTypePromise, // 8,
    datasetLanguagePromise, // 9
    datasetSpatialConceptPromise, // 10
    datasetAccrualPeriodicityPromise, // 11
    datasetLicensePromise, // 12
    datasetConformsToPromise, // 13
    datasetSpatialGeometryPromise, // 14
    datasetProvenancePromise // 15
  ])

  dataset.temporal = datasetResponse[0].map<[Date, Date]>((temporal) => [
    temporal.startDate,
    temporal.endDate
  ])
  dataset.status = datasetResponse[1][0]
  dataset.creator = datasetResponse[2]
  dataset.contactPoint = datasetResponse[3]
  dataset.publisher = datasetResponse[4][0]
  dataset.theme = datasetResponse[5]
  dataset.distribution = datasetResponse[6]
  dataset.otherIdentifier = datasetResponse[7]
  dataset.type = datasetResponse[8][0]
  dataset.language = datasetResponse[9]
  dataset.spatial = [...datasetResponse[10], ...datasetResponse[14]]
  dataset.accrualPeriodicity = datasetResponse[11][0]
  dataset.license = datasetResponse[12][0]
  dataset.conformsTo = datasetResponse[13][0]
  dataset.provenance = datasetResponse[15]

  // Format Dates
  // dataset.issued = dataset.issued ? new Date(Date.parse(dataset.issued)) : dataset.issued
  // dataset.modified = dataset.modified ? new Date(Date.parse(dataset.modified)) : dataset.modified

  return dataset
}

export async function getUserDatasetSummaries(
  userUri: string,
  auth?: Credentials
): Promise<OcDatasetSummary[]> {
  return await executeSparqlConstruct<OcDatasetSummary>(
    `
    CONSTRUCT {
      ?dataset ?p ?o.
      ?dataset oct:graph ?g.
    }
    WHERE {
      ?dataset ?p ?o;
        a dcat:Dataset;
        prov:qualifiedAttribution ?prov.
      ?prov prov:agent <${userUri}>.

      GRAPH ?g {
        ?dataset dct:identifier ?id
      }

    }
    `,
    {
      context: {
        ...datasetContext,
        distribution: {
          '@id': 'http://www.w3.org/ns/dcat#distribution',
          '@type': '@id',
          '@container': '@set'
        }
      },
      auth
    }
  )
}
