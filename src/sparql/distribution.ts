import type {
  Credentials,
  OcDataset,
  OcDistribution,
  OcDistributionSummary,
  OcPerson
} from '@/declarations'
import {
  escapeString,
  executeSparqlInsert,
  formatLocalizedProperty,
  formatDate,
  executeSparqlConstruct
} from './sparql'
import { resourceContext } from './resource'
import { type ContextDefinition } from 'jsonld'

export async function insertDistribution(
  distribution: Partial<OcDistribution>,
  dataset: OcDataset,
  profile: OcPerson,
  auth?: Credentials
) {
  distribution.identifier ||= crypto.randomUUID()
  distribution['@id'] ||=
    `${import.meta.env.VITE_OC_URI_BASE_URL}/distributions/${distribution.identifier}`

  if (!dataset.graph) {
    throw Error('Dataset has no graph!')
  }

  let insertQuery = `
    INSERT INTO <${dataset.graph[0]}> {
      <${dataset['@id']}> dcat:distribution <${distribution["@id"]}>.
      <${distribution['@id']}> a dcat:Distribution;
              dct:type <${distribution.type?.['@id']}>;
              dct:identifier "${distribution.identifier}";
              dct:title ${formatLocalizedProperty(distribution.title)};
              dct:description ${formatLocalizedProperty(distribution.description)};
              prov:qualifiedAttribution [
                a prov:Attribution;
                prov:agent <${profile['@id']}>;
                prov:generatedAtTime ${formatDate(new Date())};
                dcat:hadRole <https://www.irit.fr/opencommon/terms/dataStewardRole>
              ]`

  if (distribution.issued) {
    insertQuery += `;
    dct:issued ${formatDate(distribution.issued)}`
  }
  if (distribution.license) {
    insertQuery += `;
    dct:license <${distribution.license['@id']}>`
  }
  if (distribution.conformsTo) {
    insertQuery += `;
    dct:conformsTo <${distribution.conformsTo['@id']}>`
  }
  if (distribution.accessRights) {
    insertQuery += `;
    dct:accessRights <${distribution.accessRights['@id']}>`
  }
  if (distribution.format) {
    insertQuery += `;
    dct:format <${distribution.format['@id']}>`
  }
  if (distribution.accessService) {
    insertQuery += `;
    dcat:accessService <${distribution.accessService['@id']}>`
  }
  if (distribution.accessURL) {
    insertQuery += `;
    dcat:accessURL "${escapeString(distribution.accessURL)}"`
  }

  insertQuery += `.
    }
  `
  await executeSparqlInsert(insertQuery, { auth: auth })

  return distribution
}

export async function getDistributionSummaries(
  distributionUriList: string | string[],
  auth?: Credentials
): Promise<OcDistributionSummary[]> {
  distributionUriList = Array.isArray(distributionUriList)
    ? distributionUriList
    : [distributionUriList]

  return await executeSparqlConstruct<OcDistributionSummary>(
    `
    CONSTRUCT {
      ?distribution ?p ?o.
      ?distribution oct:graph ?g.
    }
    WHERE {
      ?distribution ?p ?o;
        a dcat:Distribution.

      GRAPH ?g {
        ?distribution dct:identifier ?id
      }
      FILTER(?distribution IN (${distributionUriList.map((uri) => `<${uri}>`).join(', ')}))
    }
    `,
    {
      context: resourceContext,
      auth
    }
  )
}

/**
 * Retrieve a distribution,
 * feeded with creators.
 *
 * @param identifier Local identifier for Virtuoso
 * @param auth Credentials to use if provided
 * @returns The distribution retrieved
 */
export async function getDistribution(
  identifier: string,
  auth?: Credentials
): Promise<OcDistribution> {

  const context: ContextDefinition = {
    ...resourceContext,
    accessURL: { '@id': 'http://www.w3.org/ns/dcat#accessURL', '@type': '@id' },
    license: { '@id': 'http://purl.org/dc/terms/license', '@type': '@id' },
    type: { '@id': 'http://purl.org/dc/terms/type', '@type': '@id' },
    accessRights: { '@id': 'http://purl.org/dc/terms/accessRights', '@type': '@id' },
    format: { '@id': 'http://purl.org/dc/terms/format', '@type': '@id' },
    label: {
      '@id': 'http://www.w3.org/2004/02/skos/core#label',
      '@container': '@language'
    },
    prefLabel: {
      '@id': 'http://www.w3.org/2004/02/skos/core#prefLabel',
      '@container': '@language'
    }
  }

  const frame = {
    '@context': context,
    '@type': [
      'http://www.w3.org/ns/dcat#Distribution',
    ],
    contains: {
      accessRights: {
        '@embed': '@always',
      },
      type: {
        '@embed': '@always'
      },
      format: {
        '@embed': '@always'
      },
      license: {
        '@embed': '@always'
      }
    }
  }

  const distributionResponse = await executeSparqlConstruct<OcDistribution>(
    `
    CONSTRUCT {
      ?distribution ?p ?o.
      ?distribution oct:graph ?g.
      ?o2 ?p3 ?o3.
    }
    WHERE {
      GRAPH ?g {
        ?distribution a dcat:Distribution;
                      dct:identifier ?identifier.
        FILTER (str(?identifier) = "${identifier}")
      }

      ?distribution ?p ?o.

      VALUES ?p2 { dct:accessRights dct:license dct:type dct:format }
      ?distribution ?p2 ?o2.
      ?o2 ?p3 ?o3.
    }

  `,
    {
      auth,
      context: context,
      frame: frame
    }
  )
  const distribution = distributionResponse?.[0]

  return distribution
}
