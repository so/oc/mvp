import type { Credentials } from "@/declarations"
import { executeSparqlSelect } from "./sparql"

export const queryKeyword = async (query: string, locale: string, auth?: Credentials): Promise<string[]> => {
  const data = await executeSparqlSelect(
    `
    SELECT distinct ?keyword
    WHERE {
        ?s dcat:keyword ?keyword
        FILTER regex(?keyword, "${query}", "i")
        FILTER(LANG(?keyword) = "${locale}")
    }
    `,
    {
      auth: auth,
    }
  )

  return data.map((item => item.keyword.value))
}