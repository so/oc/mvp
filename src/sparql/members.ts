import { type OcPerson, type OcMemberInfos, type Credentials, type OcMembership } from "@/declarations"
import { executeSparqlConstruct } from "./sparql"

/**
 * For a member, i.e. a connected user, get the IRI of their graphs (private & system).
 */
export const getMemberInfos = async (auth: Credentials) => {
  return await executeSparqlConstruct<OcMemberInfos>(
    `
      CONSTRUCT {
        ?member oct:hasSystemGraph ?systemGraph;
                oct:hasPrivateGraph ?privateGraph;
                oct:loginID ?loginID.
      }
      WHERE {
        ?member oct:hasSystemGraph ?systemGraph;
                oct:hasPrivateGraph ?privateGraph;
                oct:loginID "${auth.email}".
      }
    `,
    {
      auth: auth
    }
  )
}

/**
 * For a member, i.e. a connected user, get their profile.
 */
export const getMemberProfile = async (auth: Credentials) => {
  return await executeSparqlConstruct<OcPerson>(
    `
      CONSTRUCT { ?member ?p ?o. }
      WHERE {
        ?member oct:loginID "${auth.email}";
                ?p ?o.
        }
    `,
    {
      auth: auth
    }
  )
}

/**
 * For a member, i.e. a connected user, get the liste of his memberships to communities
 */
export const getMemberMemberships = async (auth: Credentials) => {
  return await executeSparqlConstruct<OcMembership>(
    `
      CONSTRUCT {?membership ?p2 ?o2.}
      WHERE {
        ?member oct:loginID "${auth.email}";
                org:hasMembership ?membership.
        ?membership ?p2 ?o2
      }
    `,
    {
      auth: auth
    }
  )
}
