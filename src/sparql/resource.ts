import { type OcResource, type Credentials } from '@/declarations'
import { executeSparqlConstruct } from './sparql'
import type { ContextDefinition } from 'jsonld'

export const resourceContext: ContextDefinition = {
  '@type': {
    '@container': '@set'
  },
  identifier: {
    '@id': 'http://purl.org/dc/terms/identifier'
  },
  title: {
    '@id': 'http://purl.org/dc/terms/title',
    '@container': '@language'
  },
  description: {
    '@id': 'http://purl.org/dc/terms/description',
    '@container': '@language'
  },
  graph: {
    '@id': 'https://www.irit.fr/opencommon/terms/graph',
    '@type': '@id',
    '@container': ['@set']
  }
}

/**
 * Retrieve a non specific resource,
 * can be a dataset, catalog, service or distribution
 *
 * @param identifier Local identifier for Virtuoso
 * @param auth Credentials to use if provided
 * @returns The resource retrieved
 */
export async function getResource(identifier: string, auth?: Credentials): Promise<OcResource> {
  /**
   * First, get the @id of the resource
   * to better filtering requests.
   */
  const resources = await executeSparqlConstruct<OcResource>(
    `
    CONSTRUCT {
      ?resource ?p ?o.
                oct:graph ?g.
    }
    WHERE {
      GRAPH ?g {
        ?resource dct:identifier ?id.
        FILTER (STR(?id) = "${identifier}")
        ?resource ?p ?o.        
      }
    }
    `,
    {
      auth,
      context: resourceContext
    }
  )

  return resources[0]
}
