import type { Credentials, OcSearchQuery, OcSearchResult } from '@/declarations'
import { executeSparqlConstruct, executeSparqlSelect } from './sparql'
import type { ContextDefinition } from 'jsonld'
import { resourceContext } from './resource'

/**
 * Get results URI of a search query
 */
export const searchResources = async (query: OcSearchQuery, auth?: Credentials) => {
  let queryFilter = ''
  if (query.q) {
    queryFilter = `
      VALUES ?p { dct:title dct:description }
      ?resource ?p ?o.
      FILTER regex(?o, "${query.q}", "i")
    `
  }

  if (query.params?.title){
    query.params.title.forEach((title, index) => {
      queryFilter += `
        ?resource dct:title ?title${index}.
        FILTER regex(?title${index}, "${title}", "i")
      `
    })    
  }

  if (query.params?.description){
    query.params.description.forEach((description, index) => {
      queryFilter += `
        ?resource dct:description ?description${index}.
        FILTER regex(?description${index}, "${description}", "i")
      `
    })
  }

  if (query.params?.creator){
    query.params.creator.forEach((creator, index) => {
      queryFilter += `
        ?resource dct:creator ?creator${index}.
        VALUES ?pCreator${index} { foaf:name foaf:givenName foaf:familyName foaf:firstName }
        ?creator${index} ?pCreator${index} ?oCreator${index}.
        FILTER regex(?oCreator${index}, "${creator}", "i")
      `
    })
  }
  
  /**
   * On retire les ressources ayant un espace dans leur IRI car SPARQL ne les autorise pas
   * 
   * FILTER (!contains(str(?resource), ' '))
   */
  const res = await executeSparqlSelect(
    `
      SELECT DISTINCT ?resource
      WHERE {
        ?resource rdf:type ?type.
        ?resource dct:identifier ?identifier.

        FILTER (!contains(str(?resource), ' '))

        ${queryFilter}

      } ORDER BY ASC(?resource)
    `,
    {
      auth: auth
    }
  )

  return res.map((result) => result.resource.value)
}

/** Get searchResults objects from URI list */
export const getSearchResults = async (resourceUriList: string[], auth?: Credentials) => {
  const searchResultContext: ContextDefinition = {
    ...resourceContext,
    catalog: {
      '@id': 'http://www.w3.org/ns/dcat#catalog',
      '@type': '@id',
      '@container': '@set'
    },
    dataset: {
      '@id': 'http://www.w3.org/ns/dcat#dataset',
      '@type': '@id',
      '@container': '@set'
    },
    distribution: {
      '@id': 'http://www.w3.org/ns/dcat#distribution',
      '@type': '@id',
      '@container': '@set'
    },
    creator: {
      '@id': 'http://purl.org/dc/terms/creator'
    },
    parentCatalog: {
      '@id': 'http://purl.org/dc/terms/isPartOf'
    },
    version: {
      '@id': 'http://www.w3.org/ns/dcat#version'
    }
  }

  const creatorContext: ContextDefinition = {
    familyName: {
      '@id': 'http://xmlns.com/foaf/0.1/familyName'
    },
    firstName: {
      '@id': 'http://xmlns.com/foaf/0.1/firstName'
    },
    givenName: {
      '@id': 'http://xmlns.com/foaf/0.1/givenName'
    },
    name: {
      '@id': 'http://xmlns.com/foaf/0.1/name',
      '@container': '@language'
    }
  }

  const frame = {
    '@context': {...searchResultContext, ...creatorContext},
    '@type': [
      'http://www.w3.org/ns/dcat#Catalog',
      'http://www.w3.org/ns/dcat#Dataset',
      'http://www.w3.org/ns/dcat#Distribution',
      'http://www.w3.org/ns/dcat#DataService',
      'http://www.w3.org/2004/02/skos/core#Concept'
    ],
    contains: {
      creator: {
        '@id': 'http://purl.org/dc/terms/creator',
        '@embed': '@always',
      },
      parentCatalog: {
        '@id': 'http://purl.org/dc/terms/isPartOf',
        '@embed': '@always'
      }
    }
  }

  const formattedUris = '<' + resourceUriList.join('> <') + '>'

  const res = await executeSparqlConstruct<OcSearchResult>(
    `
      CONSTRUCT {
        ?s ?p ?o.
        ?s oct:graph ?g.
        ?s dct:identifier ?identifier.
        ?s dct:creator ?creator.
        ?creator ?p2 ?o2.
        ?s dct:isPartOf ?parentCatalog.
        ?parentCatalog ?pparentCatalog ?oparentCatalog.
      }
      WHERE {
        VALUES ?s { ${formattedUris} }
        GRAPH ?g {
          ?s dct:identifier ?identifier.
        
          VALUES ?p {
            rdf:type
            dct:title
            dct:description
            dcat:version
            dcat:catalog
            dcat:dataset
            dcat:distribution
          }
          ?s ?p ?o.
        }
        OPTIONAL {
          ?s dct:creator ?creator.
          VALUES ?p2 { foaf:name foaf:givenName foaf:familyName foaf:firstName }
          ?creator ?p2 ?o2.
        }
        OPTIONAL {
          VALUES ?parentLink { dcat:dataset dcat:catalog }
          ?parentCatalog a dcat:Catalog;
                         ?parentLink ?s.
          VALUES ?pparentCatalog { dct:identifier dct:title }
          ?parentCatalog ?pparentCatalog ?oparentCatalog.
        }
      }
    `,
    {
      context: searchResultContext,
      auth: auth,
      frame: frame,
      withoutContext: true
    }
  )
  return res.sort((a, b) => a['@id'] > b['@id'])
}
