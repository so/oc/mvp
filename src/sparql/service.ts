import type { Credentials, OcService } from "@/declarations"
import { executeSparqlConstruct } from "./sparql"
import type { ContextDefinition } from "jsonld"

const serviceContext: ContextDefinition = {
  "identifier": {
    "@id": "http://purl.org/dc/terms/identifier",
    "@type": "http://www.w3.org/2000/01/rdf-schema#Literal"
  },
  "title": {
    "@id": "http://purl.org/dc/terms/title",
    "@container": "@language"
  },
}

export const queryService = async (query: string, locale: string, auth?: Credentials): Promise<OcService[]> => {
  return executeSparqlConstruct<OcService>(
    `
      CONSTRUCT {?s ?p ?o.}
      WHERE {
        ?s a dcat:DataService;
           dct:title ?title.
        FILTER regex(?title, "${query}", "i")
        FILTER(LANG(?title) = "${locale}" || LANG(?title) = "")
        ?s ?p ?o.
      }
    `,
    {
      auth: auth,
      context: serviceContext
    }
  )
}