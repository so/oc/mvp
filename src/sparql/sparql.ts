import type { Credentials, LocalizedProperty } from '@/declarations'
import { httpFetch } from '@/helpers/http'
import { compact, frame, type ContextDefinition, type NodeObject } from 'jsonld'
import type { Frame } from 'jsonld/jsonld-spec'

/**
 * Execute a CONTRUCT SparQL query and return
 * a result compacted with the help of
 *
 * - the given context
 * - the context available in the server response if no
 * context if given.
 */
export async function executeSparqlConstruct<T>(
  constructQuery: string,
  options?: {
    auth?: Credentials
    context?: ContextDefinition
    frame?: Frame
    withoutContext?: boolean
  }
): Promise<T[]> {
  const response = await httpFetch(
    `${import.meta.env.VITE_OC_BASE_URL}/${options?.auth ? 'sparql-auth' : 'sparql'}`,
    {
      method: 'POST',
      headers: {
        'Content-Type': "application/sparql-update",
        "Accept": options?.withoutContext === true ? "application/x-ld+json" : "application/ld+json"
      },
      body: constructQuery,
      auth: options?.auth
    }
  )
  const data = JSON.parse(response.data)

  // No data, no need to continue
  if (!data || !(data['@context'] || options?.withoutContext!!)) {
    return []
  }

  const context = { ...data['@context'], ...options?.context  }

  let compacted = await compact(data, context)

  normalizeIdentifier(compacted)

  if (options?.frame) {
    compacted = await frame(compacted, options.frame)
  }

  // If there's only one result, `compacted['@graph']` does not exist
  // and the only object is at the root of the `compacted` Object.
  // The lines below are here to normalize this.
  const graph: T[] = []
  if (compacted['@graph'] === undefined) {
    const { ['@context']: _, ...data } = compacted
    graph.push(data as T)
  } else {
    graph.push(...(compacted['@graph'] as Iterable<T>))
  }

  // Casting Dates
  const dateProp = Object.entries(context)
    .filter(
      ([_, prop]: [string, any]) => prop['@type'] === 'http://www.w3.org/2001/XMLSchema#dateTime'
    )
    .map(([key, _]: [string, any]) => key)

  if (dateProp.length) {
    graph.map((item: any) => {
      for (const prop of dateProp) {
        item[prop] = new Date(item[prop])
      }

      return item
    })
  }

  return graph
}

function normalizeIdentifier(object: NodeObject) {
  const normalize = (identifier: any) => {
    if (Array.isArray(identifier)) {
      console.warn('Object as several identifier : ', identifier)
      identifier = identifier[0]
    }
    if (typeof identifier === 'string') {
      return identifier
    } else {
      return ((identifier as NodeObject)?.['@value'] as string) ?? ((identifier as NodeObject)?.['@id'] as string)
    }
  }

  // Normalizing identifiers
  if (object['@graph'] === undefined || (object['@graph'] === null && object.identifier)) {
    // Unique value
    object.identifier = normalize(object.identifier)
  } else if (Array.isArray(object['@graph'])) {
    object['@graph'] = object['@graph']?.map((item: any) => {
      item.identifier = normalize(item.identifier)

      return item
    })
  }
}

/**
 * Execute a SELECT SparQL query.
 *
 * @note In most cases, it is preferable to use a CONSTRUCT request.
 */
export async function executeSparqlSelect<T>(
  selectQuery: string,
  options?: {
    auth?: Credentials
  }
): Promise<T[]> {
  const response = await httpFetch(
    `${import.meta.env.VITE_OC_BASE_URL}/${options?.auth ? 'sparql-auth' : 'sparql'}`,
    {
      method: 'POST',
      headers: {
        'Content-Type': 'application/sparql-update',
        Accept: 'application/json'
      },
      body: selectQuery,
      auth: options?.auth
    }
  )
  const data = JSON.parse(response.data)

  return data.results.bindings
}

/**
 * Execute a SparQL INSERT query
 */
export async function executeSparqlInsert(
  insertQuery: string,
  options?: {
    auth?: Credentials
    asVisitor?: boolean
  }
) {
  if (options?.auth === undefined && options?.asVisitor === undefined) {
    throw new Error(
      'Insert SparQL query should be launched with an authentification or as the visitor user.'
    )
  }

  return await httpFetch(`${import.meta.env.VITE_OC_BASE_URL}/sparql-auth`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/sparql-update',
      Accept: 'application/sparql-results+xml'
    },
    body: insertQuery,
    auth: options?.auth ?? {
      email: import.meta.env.VITE_VISITOR_USER,
      password: import.meta.env.VITE_VISITOR_PWD
    }
  })
}

/**
 * Execute a SparQL update query
 * (DELETE then INSERT)
 */
export async function executeSparqlUpdate(
  insertQuery: string,
  options: {
    auth?: Credentials
  }
) {
  if (options?.auth === undefined) {
    throw new Error('Update SparQL query should be launched with an authentification.')
  }

  return await httpFetch(`${import.meta.env.VITE_OC_BASE_URL}/sparql-auth`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/sparql-update',
      Accept: 'application/sparql-results+xml'
    },
    body: insertQuery,
    auth: options.auth
  })
}

export function escapeString(value?: string) {
  // eslint-disable-next-line no-useless-escape
  return value?.replace(/[\""]/g, '\\"').replace(/\n/g, '\\n')
}

export function formatDate(value: Date | undefined): string | undefined {
  if (value === undefined) {
    return value
  }

  return `"${value.toISOString()}"^^xsd:dateTime`
}

export function defaultLocalizedPropFormatter<T>(item: T | T[], locale: string) {
  if (Array.isArray(item)) {
    // when we have severeal values for one locale, we arbitrary
    // take the first one.
    return `"${escapeString(item[0] as string)}"@${locale}`
  } else {
    return `"${escapeString(item as string)}"@${locale}`
  }
}

export function formatLocalizedProperty<T>(
  value: LocalizedProperty<T> | undefined,
  formatter: (item: T | T[], locale: string) => string = defaultLocalizedPropFormatter<T>
) {
  if (typeof value === 'string') {
    return `"${escapeString(value)}"`
  }

  const ret = []
  for (const locale in value) {
    ret.push(formatter(value[locale], locale))
  }

  return ret.join(', ')
}
