import { type OcTreeNode, type Credentials } from '@/declarations'
import { executeSparqlConstruct } from './sparql'
import type { ContextDefinition } from 'jsonld'
import { resourceContext } from './resource'

/**
 * Get tree nodes from a parent node URI
 */
export const getTreeNodesFromParentNodeURI = async (parentNodeUri: string, auth?: Credentials) => {
  const context: ContextDefinition = {
    '@type': {
      '@container': '@set'
    },
    catalog: {
      '@id': 'http://www.w3.org/ns/dcat#catalog',
      '@type': '@id',
      '@container': ['@set']
    },
    dataset: {
      '@id': 'http://www.w3.org/ns/dcat#dataset',
      '@type': '@id',
      '@container': ['@set']
    },
    distribution: {
      '@id': 'http://www.w3.org/ns/dcat#distribution',
      '@type': '@id',
      '@container': ['@set']
    },
    title: {
      '@id': 'http://purl.org/dc/terms/title',
      '@container': '@language'
    },
    identifier: {
      '@id': 'http://purl.org/dc/terms/identifier'
    },
    graph: {
      '@id': 'https://www.irit.fr/opencommon/terms/graph',
      '@type': '@id',
      '@container': ['@set']
    }
  }

  const frame = {
    '@id': parentNodeUri,
    '@context': context,
    contains: {
      '@type': 'http://www.w3.org/ns/dcat#Catalog',
      '@embed': '@always',
      contains: {
        '@type': [
          'http://www.w3.org/ns/dcat#Catalog',
          'http://www.w3.org/ns/dcat#Dataset',
          'http://www.w3.org/ns/dcat#Distribution'
        ],
        '@embed': '@always'
      }
    }
  }

  return await executeSparqlConstruct<OcTreeNode>(
    `
      CONSTRUCT {
        <${parentNodeUri}> ?p ?o.
        ?node ?p2 ?o2.
        ?node oct:graph ?g.
      }
      WHERE {
        <${parentNodeUri}> rdf:type ?type.
        FILTER (?type in (dcat:Catalog, dcat:Dataset, dcat:Distribution, dcat:DataService))
        {
          VALUES ?p { dcat:catalog dcat:dataset dcat:distribution dct:title dct:identifier rdf:type }
          <${parentNodeUri}> ?p ?o.
        } UNION {
          VALUES ?p2 { dcat:catalog dcat:dataset dcat:distribution dct:title dct:identifier rdf:type }
          <${parentNodeUri}> (dcat:catalog|dcat:dataset|dcat:distribution){0,1} ?node.
          ?node ?p2 ?o2 .
          GRAPH ?g {
            ?node dct:identifier ?id
          }
        }
      }
    `,
    {
      context,
      auth,
      frame,
      withoutContext: true
    }
  )
}

/**
 * Get URIs of nodes between home and a specific resource
 */
export const getUrisBetweenHomeAndResource = async (
  homeCatalogueUri: string,
  resourceId: string,
  resourceType: string,
  auth?: Credentials
) => {
  const context: ContextDefinition = {
    ...resourceContext,
    catalog: {
      '@id': 'http://www.w3.org/ns/dcat#catalog',
      '@type': '@id',
      '@container': ['@set']
    },
    dataset: {
      '@id': 'http://www.w3.org/ns/dcat#dataset',
      '@type': '@id',
      '@container': ['@set']
    },
    distribution: {
      '@id': 'http://www.w3.org/ns/dcat#distribution',
      '@type': '@id',
      '@container': ['@set']
    }
  }

  let constructClause = ''
  let whereClause = ''

  switch (resourceType) {
    case 'distribution':
      constructClause = `
        ?child ?p ?o.
        ?subchild ?p2 ?o2.
        ?subchild dcat:dataset ?dataset.
        ?dataset ?p3 ?o3.
        ?dataset dcat:distribution ?distribution.
        ?distribution ?p4 ?o4.
      `
      whereClause = `
        <${homeCatalogueUri}> dcat:catalog* ?child.
        ?child dcat:catalog+ ?subchild.
        ?subchild dcat:dataset ?dataset.
        ?dataset dcat:distribution ?distribution.
        ?distribution dct:identifier ?identifier.
        FILTER (str(?identifier) = "${resourceId}")

        VALUES ?p { dcat:catalog dct:identifier rdf:type }
        VALUES ?p2 { dct:identifier rdf:type }
        VALUES ?p3 { dct:identifier rdf:type }
        VALUES ?p4 { dct:identifier rdf:type }
        ?child ?p ?o.
        ?subchild ?p2 ?o2.
        ?dataset ?p3 ?o3.
        ?distribution ?p4 ?o4.
      `
      break
    case 'dataset':
      constructClause = `
        ?child ?p ?o.
        ?subchild ?p2 ?o2.
        ?subchild dcat:dataset ?dataset.
        ?dataset ?p3 ?o3.
      `
      whereClause = `
        <${homeCatalogueUri}> dcat:catalog* ?child.
        ?child dcat:catalog+ ?subchild.
        ?subchild dcat:dataset ?dataset.
        ?dataset dct:identifier ?identifier.
        FILTER (str(?identifier) = "${resourceId}")

        VALUES ?p { dcat:catalog dct:identifier rdf:type }
        VALUES ?p2 { dct:identifier rdf:type }
        VALUES ?p3 { dct:identifier rdf:type }
        ?child ?p ?o.
        ?subchild ?p2 ?o2.
        ?dataset ?p3 ?o3.
      `
      break
    case 'catalog':
      constructClause = `
        ?child ?p ?o.
        ?subchild ?p2 ?o2.
      `
      whereClause = `
        <${homeCatalogueUri}> dcat:catalog* ?child.
        ?child dcat:catalog+ ?subchild.
        ?subchild dct:identifier ?identifier.
        FILTER (str(?identifier) = "${resourceId}")

        VALUES ?p { dcat:catalog dct:identifier rdf:type }
        VALUES ?p2 { dct:identifier rdf:type }
        ?child ?p ?o.
        ?subchild ?p2 ?o2.
      `
      break
    case 'service':
      break
  }

  return await executeSparqlConstruct<OcTreeNode>(
    `
      CONSTRUCT { ${constructClause} }
      WHERE { ${whereClause} }
    `,
    {
      context,
      auth,
      withoutContext: true
    }
  )
}
