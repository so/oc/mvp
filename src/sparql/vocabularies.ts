import type { Credentials, OcConcept, OcConceptScheme, OcLocale } from '@/declarations'
import { executeSparqlConstruct } from './sparql'
import type { ContextDefinition } from 'jsonld'

export const conceptContext: ContextDefinition = {
  identifier: {
    '@id': 'http://purl.org/dc/terms/identifier',
    '@type': 'http://www.w3.org/2000/01/rdf-schema#Literal'
  },
  prefLabel: {
    '@id': 'http://www.w3.org/2004/02/skos/core#prefLabel',
    '@container': '@language'
  },
  narrower: {
    '@id': 'http://www.w3.org/2004/02/skos/core#narrower'
  }
}

const schemeContext: ContextDefinition = {
  identifier: {
    '@id': 'http://purl.org/dc/terms/identifier',
    '@type': 'http://www.w3.org/2000/01/rdf-schema#Literal'
  },
  prefLabel: {
    '@id': 'http://www.w3.org/2004/02/skos/core#prefLabel',
    '@container': '@language'
  }
}

export const getVocabulariesList = (schemes: string[], auth?: Credentials) => {
  const promiseList = schemes.map((scheme) => getVocabularyList(scheme, auth))
  return Promise.all(promiseList).then((values) => {
    return ([] as OcConcept[]).concat(...values)
  })
}

export const getVocabularyList = (scheme: string, auth?: Credentials) => {
  return executeSparqlConstruct<OcConcept>(
    `
      CONSTRUCT {?s skos:prefLabel ?label.}
      WHERE {
        ?s a skos:Concept;
           skos:prefLabel ?label;
           skos:inScheme <${scheme}>.
      }
    `,
    {
      auth: auth,
      context: conceptContext
    }
  )
}

export const queryVocabulariesList = (
  schemes: string[],
  query: string,
  locale: string,
  auth?: Credentials
) => {
  const promiseList = schemes.map((scheme) => queryVocabulary(scheme, query, locale, auth))
  return Promise.all(promiseList).then((values) => {
    return ([] as OcConcept[]).concat(...values)
  })
}

export const queryVocabulary = (
  scheme: string,
  query: string,
  locale: string,
  auth?: Credentials
) => {
  return executeSparqlConstruct<OcConcept>(
    `
      CONSTRUCT {?s skos:prefLabel ?label.}
      WHERE {
        ?s a skos:Concept;
           skos:inScheme <${scheme}>;
           skos:prefLabel ?label.
           
        FILTER regex(?label, "${query}", "i").
        FILTER(LANG(?label) = "${locale}" || LANG(?title) = "").
      }
    `,
    {
      auth: auth,
      context: conceptContext
    }
  )
}

export const getConceptChildren = (scheme: string, parentTerm?: string, auth?: Credentials) => {
  return executeSparqlConstruct<OcConcept>(
    `
        CONSTRUCT {
          ?s skos:prefLabel ?label.
          ?s skos:narrower ?narrower.
        }
        WHERE {
          ?s a skos:Concept;
             skos:prefLabel ?label;
             skos:broader|^skos:narrower <${parentTerm}>;
             skos:inScheme <${scheme}>.
          OPTIONAL {
            ?s skos:narrower|^skos:broader ?narrower
          }
        }
      `,
    {
      auth: auth,
      context: conceptContext
    }
  )
}

export const getVocabularyRootConcepts = async (scheme: string, auth?: Credentials) => {
  const result = await executeSparqlConstruct<OcConcept>(
    `
      CONSTRUCT {
        ?s skos:prefLabel ?label.
        ?s skos:narrower ?narrower.
      }
      WHERE {
        ?s a skos:Concept;
          skos:topConceptOf|^skos:hasTopConcept <${scheme}>;
          skos:prefLabel ?label.
        OPTIONAL {
          ?s skos:narrower|^skos:broader ?narrower
        }
      }
    `,
    {
      auth: auth,
      context: conceptContext
    }
  )

  if (result.length) {
    return result
  } else {
    // Si il n'y a pas de résulats il peut s'agir d'un vocabulaire sans arborescence et donc on utilise `getVocabularyList`
    return getVocabularyList(scheme, auth)
  }
}

export const getVocabulariesInformations = async (schemes: string[]) => {
  const schemesValues = '<' + schemes.join('> <') + '>'
  return executeSparqlConstruct<OcConceptScheme>(
    `
      CONSTRUCT {
        ?s skos:prefLabel ?o.
      }
      WHERE {
        VALUES ?s { ${schemesValues} }
        ?s skos:prefLabel|rdfs:label ?o.
      }
    `,
    {
      context: schemeContext
    }
  )
}

export const getLocaleList = () => {
  return executeSparqlConstruct<OcLocale>(
    `
    CONSTRUCT {
      ?s skos:prefLabel ?prefLabel.
      ?s <http://publications.europa.eu/ontology/authority/legacy-code> ?codeValue.
    }
    WHERE {
      ?s a skos:Concept;
         skos:inScheme <http://publications.europa.eu/resource/authority/language>;
         <http://publications.europa.eu/ontology/authority/op-mapped-code> ?code;
         skos:prefLabel ?prefLabel.
      
        ?code ?p2 ?o2;
              dc:source 'iso-639-1';
              <http://publications.europa.eu/ontology/authority/legacy-code> ?codeValue.
    }
    `,
    {
      context: {
        identifier: {
          '@id': 'http://purl.org/dc/terms/identifier',
          '@type': 'http://www.w3.org/2000/01/rdf-schema#Literal'
        },
        prefLabel: {
          '@id': 'http://www.w3.org/2004/02/skos/core#prefLabel',
          '@container': '@language'
        },
        code: {
          '@id': 'http://publications.europa.eu/ontology/authority/legacy-code'
        }
      }
    }
  )
}

export const getLicenseList = () => {
  return executeSparqlConstruct<OcConcept>(
    `
    CONSTRUCT {
      ?s skos:prefLabel ?prefLabel.
      ?s skos:altLabel ?altLabel.
    }
    WHERE {
      ?s a skos:Concept;
         skos:inScheme <http://publications.europa.eu/resource/authority/licence>;
         skos:prefLabel ?prefLabel;
         skos:altLabel ?altLabel.
    }
    `,
    {
      context: {
        identifier: {
          '@id': 'http://purl.org/dc/terms/identifier',
          '@type': 'http://www.w3.org/2000/01/rdf-schema#Literal'
        },
        prefLabel: {
          '@id': 'http://www.w3.org/2004/02/skos/core#prefLabel',
          '@container': '@language'
        },
        altLabel: {
          '@id': 'http://www.w3.org/2004/02/skos/core#altLabel',
          '@container': '@language'
        }
      }
    }
  )
}
