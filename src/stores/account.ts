import type { Credentials, OcMemberInfos, OcMembership, OcPerson } from '@/declarations'
import { defineStore } from 'pinia'
import { computed, ref, watch, type ComputedRef, type Ref } from 'vue'

interface AccountState {
  auth?: Ref<Credentials | undefined>
  infos: Ref<OcMemberInfos | null>
  profile: Ref<OcPerson | null>
  memberships: Ref<OcMembership[]>
  isAuthenticated: ComputedRef<boolean>
}

export const useAccountStore = defineStore<string, AccountState>('account', () => {
  const auth = ref<Credentials>()
  const localAuth = localStorage.getItem('token')

  if (localAuth !== null) {
    const [email, password] = atob(localAuth).split(':')
    if (email && password) {
      auth.value = {
        email: email,
        password: password,
      }
    }
  }

  const infos = ref(null)
  const profile = ref(null)
  const memberships = ref<OcMembership[]>([])

  const isAuthenticated = computed(() => auth.value !== undefined)

  watch(auth, (auth: Credentials | undefined) => {
    if (auth) {
      localStorage.setItem("token", btoa(`${auth.email}:${auth.password}`));
    } else {
      localStorage.removeItem("token")
    }
  })

  return {
    auth,
    infos,
    profile,
    memberships,
    isAuthenticated
  }
})
