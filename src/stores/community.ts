import type { OcCommunity } from '@/declarations'
import { getCommunityList } from '@/sparql/communities'
import { defineStore } from 'pinia'
import { computed, ref, watch } from 'vue'
import { useAccountStore } from './account'
import { useAbility } from '@casl/vue'
import { defineAbilityFor } from '@/ability'

interface CommunityState {
  data: OcCommunity[]
  loading: boolean
  error: null | Error
}

export const useCommunityStore = defineStore('community', () => {
  const state = ref<CommunityState>({
    data: [],
    loading: false,
    error: null,
  })

  const ability = useAbility()
  const accountStore = useAccountStore()

  async function fetchIfEmpty(force: boolean = false) {
    if (force || state.value.data.length === 0) {
      state.value.loading = true
      try {
        const result = await getCommunityList(accountStore.auth);
        state.value.data = result
      } catch (e) {
        state.value.error = e as Error
      }
      state.value.loading = false
    }
  }

  const communityByName = computed(() => {
    const list: { [name: string]: OcCommunity } = {}
    for (const community of state.value.data) {
      list[community.name] = community
    }

    return list
  })

  const getByName = (name: string) => communityByName.value[name]

  // We update abilities when we fetch communties
  // Or when we get new memberships
  watch(
    [
      () => accountStore.memberships,
      () => state.value.data
    ],
    ([memberships, communities]) => {
      ability.update(defineAbilityFor(memberships, communities, accountStore.infos?.hasPrivateGraph))
    }
  )

  return {
    state,
    fetchIfEmpty,
    getByName,
    communityByName
  }
})
