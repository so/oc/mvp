import type { OcBreadcrumbItem, OcCommunity, OcTreeNode } from '@/declarations'
import { getTreeNodesFromParentNodeURI, getUrisBetweenHomeAndResource } from '@/sparql/tree'
import { defineStore, storeToRefs } from 'pinia'
import { computed, ref, watch } from 'vue'
import { useAccountStore } from './account'
import { useTranslateValue } from '@/composables/useTranslateValue'
import { type2ResourceType } from '@/helpers/resourceType'

interface TreeState {
  nodes: OcTreeNode[]
  selectedNodeKey: string | undefined
  expandedNodesKeys: string[]
  loading: boolean
  error: null | Error
}

export const useTreeStore = defineStore('tree', () => {
  const state = ref<TreeState>({
    nodes: [],
    selectedNodeKey: undefined,
    expandedNodesKeys: [],
    loading: false,
    error: null
  })

  const { translateValue } = useTranslateValue()

  const account = storeToRefs(useAccountStore())

  async function fetchIfEmpty(community: OcCommunity) {
    if (state.value.nodes.length == 0 || community['@id'] != state.value.nodes[0]['@id']) {
      state.value.loading = true
      resetState()

      const result = await getTreeNodesFromParentNodeURI(community['@id'], account.auth?.value)
      
      if (result.length && Object.keys(result[0]).length){
        state.value.nodes = result
      }

      state.value.loading = false
    }
  }

  async function fetchTreeNodesFromParentNodeURI(parentNodeUri: string, community: OcCommunity) {
    if (state.value.nodes.length == 0) {
      await fetchIfEmpty(community)
    }
    let parentNode = findNode(parentNodeUri, state.value.nodes[0])
    if (parentNode) {
      parentNode.loading = true
      state.value.loading = true
      const result = await getTreeNodesFromParentNodeURI(parentNodeUri, account.auth?.value)
      parentNode = Object.assign(parentNode, result[0])
      parentNode.loading = false
      state.value.loading = false
    }
  }

  function findNode(nodeUri: string, currentNode: OcTreeNode): OcTreeNode | false {
    let result: OcTreeNode | false

    if (nodeUri == currentNode['@id']) {
      return currentNode
    } else {
      if (currentNode.catalog) {
        for (let i = 0; i < currentNode.catalog.length; i += 1) {
          if (typeof currentNode.catalog[i] !== 'string') {
            result = findNode(nodeUri, currentNode.catalog[i] as OcTreeNode)

            if (result !== false) {
              return result
            }
          }
        }
      }

      if (currentNode.dataset) {
        for (let i = 0; i < currentNode.dataset.length; i += 1) {
          if (typeof currentNode.dataset[i] !== 'string') {
            result = findNode(nodeUri, currentNode.dataset[i] as OcTreeNode)

            if (result !== false) {
              return result
            }
          }
        }
      }

      if (currentNode.distribution) {
        for (let i = 0; i < currentNode.distribution.length; i += 1) {
          if (typeof currentNode.distribution[i] !== 'string') {
            result = findNode(nodeUri, currentNode.distribution[i] as OcTreeNode)

            if (result !== false) {
              return result
            }
          }
        }
      }
      return false
    }
  }

  async function fetchTreeForResource(resourceId: string, resourceType: string, community: OcCommunity) {
    state.value.loading = true
    const result = await getUrisBetweenHomeAndResource(
      community['@id'],
      resourceId,
      resourceType,
      account.auth?.value
    )

    if (result.length) {
      const ordered = reorderNodesFromHomeToResource(result, community['@id'], resourceId)

      await ordered.reduce(async (promiseChain, item) => {
        await promiseChain
        await fetchTreeNodesFromParentNodeURI(item['@id'], community)
        state.value.expandedNodesKeys.push(item.identifier)
      }, Promise.resolve());
      state.value.selectedNodeKey = resourceId
    } else {
      console.warn('No path between home and resource of id "' + resourceId + '" found')
      await fetchIfEmpty(community)
    }

    state.value.loading = false
  }

  function reorderNodesFromHomeToResource(
    resources: OcTreeNode[],
    homeUri: string,
    resourceId: string
  ): OcTreeNode[] {
    const reordered: OcTreeNode[] = []
    let currentResource = resources.filter((resource) => resource.identifier == resourceId)[0]
    let currentResourceUri = currentResource['@id']

    if (currentResource['@type'].includes('http://www.w3.org/ns/dcat#Distribution')) {
      reordered.unshift(currentResource)

      currentResource = resources.filter((resource) => resource.distribution?.includes(currentResourceUri))[0]
      currentResourceUri = currentResource['@id']
    }

    if (currentResource['@type'].includes('http://www.w3.org/ns/dcat#Dataset')) {
      reordered.unshift(currentResource)

      currentResource = resources.filter((resource) => resource.dataset?.includes(currentResourceUri))[0]
      currentResourceUri = currentResource['@id']
    }

    while (currentResourceUri != homeUri) {
      reordered.unshift(currentResource)

      currentResource = resources.filter((resource) => resource.catalog?.includes(currentResourceUri))[0]
      currentResourceUri = currentResource['@id']
    }

    reordered.unshift(currentResource)

    return reordered
  }

  watch(account.isAuthenticated, () => {
    state.value.nodes = []
    state.value.selectedNodeKey = undefined
    state.value.expandedNodesKeys = []
  })

  function getNodePath(path: OcTreeNode[], currentNode: OcTreeNode, nodeKey: string): OcTreeNode[] | undefined {
    if (currentNode.identifier === nodeKey) {
      return path
    }

    let childrenNodes: Array<OcTreeNode> = []
    if (currentNode.catalog) {
      childrenNodes = [...childrenNodes, ...(currentNode.catalog).filter((node) => typeof node !== 'string')]
    }
    if (currentNode.dataset) {
      childrenNodes = [...childrenNodes, ...(currentNode.dataset).filter((node) => typeof node !== 'string')]
    }
    if (currentNode.distribution) {
      childrenNodes = [...childrenNodes, ...(currentNode.distribution).filter((node) => typeof node !== 'string')]
    }

    for (let i = 0; i < childrenNodes.length; i += 1) {
      const res = getNodePath([...path, childrenNodes[i]], childrenNodes[i], nodeKey)
      if (res) {
        return res
      }
    }
  }

  const breadcrumb = computed<OcBreadcrumbItem[]>(() => {
    if (state.value.selectedNodeKey && state.value.nodes[0]) {
      const path = getNodePath([], state.value.nodes[0], state.value.selectedNodeKey)
      if (path === undefined) {
        return []
      }
      return path.map((node) => ocTreeNode2BreadcrumbItem(node))
    } else {// Si il n'y a pas de noeud selectionné, le breadcrumb est vide
      return []
    }
  })

  function ocTreeNode2BreadcrumbItem(node: OcTreeNode): OcBreadcrumbItem {
    const dcatType = type2ResourceType[node['@type'].filter((type) => type in type2ResourceType)[0]]

    return {
      key: node.identifier,
      label: translateValue(node.title),
      type: dcatType,
      to: { name: 'community.resource', params: { resource: dcatType, identifier: node.identifier } }
    }
  }

  function resetState() {
    state.value = {
      nodes: [],
      selectedNodeKey: undefined,
      expandedNodesKeys: [],
      loading: false,
      error: null
    }
  }

  return {
    state,
    fetchIfEmpty,
    fetchTreeNodesFromParentNodeURI,
    fetchTreeForResource,
    resetState,
    breadcrumb
  }
})
