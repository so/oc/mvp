/* eslint-disable */
/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors')

export default {
  content: ['./src/**/*.vue', './index.html'],
  theme: {
    extend: {
      boxShadow: {
        oc: 'rgba(0, 0, 0, 0.4) 0 5px 20px '
      }
    },
    fontFamily: {
      title: ['Antonio', 'sans-serif'],
      sans: ['Montserrat', 'sans-serif']
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      amber: colors.amber,
      red: colors.red,
      gray: colors.gray,
      slate: colors.slate,
      zinc: colors.zinc,
      'fair-f': '#056839',
      'fair-a': '#fcb041',
      'fair-i': '#7f3f98',
      'fair-r': '#1076bc',
      olivine: {
        50: '#f5f9ec',
        100: '#e7f0d7',
        200: '#d1e3b3',
        300: '#b0cf81',
        400: '#96bc5f',
        500: '#78a141',
        600: '#5d8030',
        700: '#476229',
        800: '#3c4f25',
        900: '#344423',
        950: '#19240f'
      },
      linen: {
        50: '#fcf6f0',
        100: '#f9ede0',
        200: '#f0d3b8',
        300: '#e7b58a',
        400: '#dc8e5b',
        500: '#d4723b',
        600: '#c65b30',
        700: '#a4472a',
        800: '#843a28',
        900: '#6b3123',
        950: '#391811'
      },
      bluesky: {
        50: '#e6f7fc',
        100: '#d4f0f9',
        200: '#aee0f3',
        300: '#77c8e9',
        400: '#38a7d8',
        500: '#1c8abe',
        600: '#1a6fa0',
        700: '#1c5a82',
        800: '#1f4c6b',
        900: '#1e3f5b',
        950: '#0e283e'
      }
    }
  },
  safelist: [
    ...['fair-f', 'fair-a', 'fair-i', 'fair-r'].map((c) => `bg-${c}`),
    ...['primary', 'olivine', 'linen', 'bluesky', 'slate'].map((c) => `bg-${c}-200`),
    ...['primary', 'olivine', 'linen', 'bluesky', 'slate'].map((c) => `hover:bg-${c}-300`),
    ...['primary', 'olivine', 'linen', 'bluesky', 'slate'].map((c) => `hover:bg-${c}-700`),
    ...['primary', 'olivine', 'linen', 'bluesky', 'slate'].map((c) => `bg-${c}-300`),
    ...['primary', 'olivine', 'linen', 'bluesky', 'slate'].map((c) => `bg-${c}-400`),
    ...['primary', 'olivine', 'linen', 'bluesky', 'slate'].map((c) => `bg-${c}-600/80`),
    ...['primary', 'olivine', 'linen', 'bluesky', 'slate'].map((c) => `hover:bg-${c}-400`),
    ...['primary', 'olivine', 'linen', 'bluesky', 'slate'].map((c) => `text-${c}-200/80`)
  ],
  plugins: [require('tailwindcss-primeui')]
}
