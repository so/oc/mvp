// import { fileURLToPath } from 'node:url'
import { mergeConfig, defineConfig /*, configDefaults */ } from 'vitest/config'
import { storybookTest } from '@storybook/experimental-addon-test/vitest-plugin';
import viteConfig from './vite.config'

export default mergeConfig(
  viteConfig,
  defineConfig({
    plugins: [
      storybookTest({
        // This should match your package.json script to run Storybook
        // The --ci flag will skip prompts and not open a browser
        storybookScript: 'npm run storybook --ci',
      }),
    ],
    test: {

      // environment: 'jsdom',
      // exclude: [...configDefaults.exclude, 'e2e/**'],
      // root: fileURLToPath(new URL('./', import.meta.url))

      // Storybook conf
      // Glob pattern to find story files
      include: ['src/**/*.stories.?(m)[jt]s?(x)'],
      // Enable browser mode
      browser: {
        enabled: true,
        name: 'chromium',
        // Make sure to install Playwright
        provider: 'playwright',
        headless: true,
      },
      // Speed up tests and better match how they run in Storybook itself
      // https://vitest.dev/config/#isolate
      // Consider removing this if you have flaky tests
      isolate: false,
      setupFiles: ['./.storybook/vitest.setup.ts'],
    }
  })
)
